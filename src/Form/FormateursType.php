<?php

namespace App\Form;


use App\Entity\Formateurs;
use App\Form\TarifFormateurType;
use App\Form\FormateurContactType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

class FormateursType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            //->add('denominationSociale')
            //->add('siret')
            // ->add('nom')
            // ->add('prenom')
            // ->add('email', EmailType::class)
            //->add('entreprise')
            // ->add('adresse')
            ->add('contacts',FormateurContactType::class, ['label'=> false])
            ->add('entreprise', EntrepriseType::class)
            //->add('sessions')
            //->add('contacts')
            //->add('adresses',ChoiceType::class)
            ->add('methode', ChoiceType::class, ['choices'=>['Distanciel'=>1, 'Présenciel'=>2, 'Distanciel & Présenciel'=>3]])
            ->add('formations')
            ->add('tarifs', CollectionType::class,[
                    'label' => false,
                    'entry_type' => TarifFormateurType::class,
                    'entry_options' => ['label' => false],
                    'allow_add' => true,
                    'prototype' => true,
                    'by_reference'=>false
            ])
            // ->add('Entreprise', ProspectsEntrepriseType::class)
            // ->add('password') une génération automatique de mdp
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Formateurs::class,
        ]);
    }
}
