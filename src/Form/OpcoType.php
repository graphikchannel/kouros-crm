<?php

namespace App\Form;

use App\Entity\Opco;
use App\Entity\Contacts;
use App\Form\AdresseType;
use App\Form\ContactsType;
use App\Form\EntrepriseType;
use Symfony\Component\Form\AbstractType;
use phpDocumentor\Reflection\Types\Callable_;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

class OpcoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        
        $builder
            //->add('devis')
            ->add('entreprise', EntrepriseType::class)

            ->add('contacts', CollectionType::class,[
                    "label_html"=> true,
                    'label'=>false,
                    'entry_type'=> ContactsType::class,
                    'entry_options' => ['label' => false],
                    'allow_add' => true,
                    'prototype' => true,
                    
            ])
            // ->add('contacts', ChoiceType::class, [
            //     'data_class' => Contacts::class,
            //     'choice_label' => 'nom',
            //     'by_reference'=>false,
            //     'multiple'=>true,
            //     "choice_filter"=> 'nom',
                

            // ])

            ->add('adresse', AdresseOpcoType::class)
           
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Opco::class,
        ]);
    }
}
