<?php

namespace App\Form;

use App\Entity\Contacts;
use App\Entity\Employes;
use App\Form\ContactsType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;

class EmployesType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            // ->add('nom')
            // ->add('prenom')
            // ->add('email')
            // ->add('identifiant')
            ->add('roles', ChoiceType::class,[
                "label"=>false,
                "choices"=>[
                    // "Employé"=>"ROLE_USER",
                    "Administrateur"=>"ROLE_ADMIN"
                ],
                "multiple"=>true,
                "expanded"=>true
            ])
            // ->add('contact', CollectionType::class, [
            //     'label' => false,
            //     'entry_type' => ContactsType::class,
            //     'entry_options' => ['label' => false],
            //     'allow_add' => true,
            //     'prototype' => true,
            // ])
            ->add('contact', ContactsType::class,[
                'label' => false,
            ])

            //pas obligatoire car soit on crée un mdp soit il est autogénéré et envoyé par email
            ->add('password', PasswordType::class,[
                'label' => 'Mot de passe*',
                "required"=> false,
                "help"=>'*Laisser vide pour génération automatique et envoie par email'
            ])
            //->add('image')
            //->add('fullname')
        ;
    }

    

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Employes::class,
        ]);
    }
}
