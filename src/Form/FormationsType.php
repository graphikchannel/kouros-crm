<?php

namespace App\Form;

use App\Entity\Formations;
use App\Form\TarifsFormationsType;
use Doctrine\DBAL\Types\StringType;
use Symfony\Component\Form\AbstractType;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;


class FormationsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('titre')
            ->remove('prixJour')
            ->add('programme', FileType::class,[
                'required'=>false,
                'data_class' => null
            ])
            ->add('niveau')
            ->add('secteur')
            ->add('tarifs', CollectionType::class,[
                    'label' => false,
                    'entry_type' => TarifsFormationsType::class,
                    'entry_options' => ['label' => false],
                    'allow_add' => true,
                    'prototype' => true,
                    'by_reference'=>false
            ])

            // secteur / niveau => des entitées avec des cruds 
            // ->add('niveau', ChoiceType::class, ['placeholder'=>'Quel est le niveau ', 'choices'=>
            // ['Initiation'=>1, 'Intermédiaire'=>2, 'Perfectionnement'=>3]])
            
            //créer une entité avec le crud pour ranger tout ces séteurs (pour un ajout ou suppression de secteur au futur)
            // ->add('rubrique', ChoiceType::class,
            // ['placeholder'=>'Quel est la rubrique : ', 'choices'=>
            // ['3D'=>1, 'Architecture 3D'=>2, 'Audiovisuel'=>4,'BIM'=>5, 'Bureautique'=>6, 'CAO-DAO'=>7,'Cartographie'=>8, 'Commercial'=>9, 'Communication'=>10,'Comptabilité'=>11, 'Développement'=>12, 'Droit'=>13, 'Immobilier'=>14, 'Imprimerie'=>15, 'Infographie 3D'=>16, 'Informatique'=>17, 'Ingénierie'=>18, 'Langues'=>19,'Maîtrise d\'oeuvre'=>20, 'PAO'=>21, 'Web'=>22]])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Formations::class,
        ]);
    }
}
