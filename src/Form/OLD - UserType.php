<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email')
            ->add('roles', ChoiceType::class,[
                "choices"=>[
                    "User"=>"ROLE_USER",
                    "Administrateur"=>"ROLE_ADMIN"
                ],
                "multiple"=>true,
                "expanded"=>true
            ])
            ->add('password')
            //->add('image')
            //->add('fullname')
            
            ->add('commentaires', CollectionType::class, [
                'label' => false,
                'entry_type' => CommentaireType::class,
                'entry_options' => ['label' => false],
                'allow_add' => true,
                'prototype' => true,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
