<?php

namespace App\Form;

use App\Entity\Prospects;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class ClientsType extends AbstractType
{//18-08-21 tde : ce formulaire n'est accessible qu'aux employés et admin kouros
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('statut', ChoiceType::class, ['placeholder'=>'Souhaitez vous enregistrer une entreprise ou un particulier ', 'choices'=>
            ['Entreprise'=>1, 'Particulier'=>2]])
            ->add('nom')
            ->add('prenom')
            ->add('email')
            ->add('tel')
            ->add('adresse')
            ->add('codePostal')
            ->add('ville')
            ->add('site')
            ->add('entreprise')
            ->add('commentaire')
            //18-08-21 tde : un password sera automatiquement envoyé au prospect lors du changement d'état vers client 
            //car un prospect n'a pas besoin d'accèder à un espace client
            //pour éviter prob de sécu le mdp doit être généré à la création du prospect
            ->remove('etat', ChoiceType::class, ['placeholder'=>'prospect ou client', 'choices'=>
            ['Prospect'=>0, 'Client'=>1]])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Prospects::class,
        ]);
    }
}
