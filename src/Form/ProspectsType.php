<?php

namespace App\Form;

use App\Form\OpcoType;
use App\Entity\Prospects;
use App\Form\AdresseType;
use App\Entity\Entreprise;
use App\Form\ContactsType;
use App\Form\CommentaireType;
use App\Form\ProspectsEntrepriseType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

class ProspectsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('statut', ChoiceType::class, 
            ['choices'=>
                ['Entreprise'=>1, 
                'Particulier'=>2],
                'data'=>1,//de base sur entreprise
                'expanded'=>true,
                'multiple'=>false, 
                'required'=>true]
                )

                ->add('contacts', CollectionType::class, [
                    'label' => false,
                    'entry_type' => ContactsType::class,
                    'entry_options' => ['label' => false],
                    'allow_add' => true,
                    'prototype' => true,
                ])

                ->add('adresses', CollectionType::class, [
                    'label' => false,
                    'entry_type' => AdresseType::class,
                    'entry_options' => ['label' => false],
                    'allow_add' => true,
                    'prototype' => true,
                ])

            //créer une entreprise
            ->add('Entreprise', ProspectsEntrepriseType::class, [
                'required' => false,
                'label' => false,
            ])

            ->add('Commentaire', CommentaireType::class, [
                'required' => false,
                'label' => false,
            ])
            // ->add('commentaire', CollectionType::class, [
            //     'label' => false,
            //     'entry_type' => CommentaireType::class,
            //     'entry_options' => ['label' => false],
            //     'allow_add' => true,
            //     'prototype' => true,
            // ])

            /*->remove('etat', ChoiceType::class, ['placeholder'=>'prospect ou client', 'choices'=>
            ['Prospect'=>0, 'Client'=>1]])*/

            //->add('opco')

/*            ->add('Opco', CollectionType::class, [
                'entry_type' => OpcoType::class,
                'entry_options' => ['label' => false],
                'allow_add' => true,
            ])

            ->add('adherent',null, [
                "label"=>'Adhérent'
            ])
*/


        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Prospects::class,
        ]);
    }
}
