<?php

namespace App\Form;

use App\Entity\Devis;
use App\Entity\Contacts;
use App\Form\EmployesType;
use App\Entity\Conventions;
use App\Entity\Participants;
use App\Form\ParticipantsType;
use App\Repository\DevisRepository;
use Symfony\Component\Form\AbstractType;
use App\Form\ConventionsParticipantsType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

class ConventionsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('lieuFormation')
            ->add('prospect')
            ->add('devis', EntityType::class, [
                'class' => Devis::class,
                'choice_label' => 'numeroDevis'
            ])
            ->add('commentaire')
            // ANCHOR
            ->add('participants', CollectionType::class, [
                'label'=>false,
                'entry_type' => ParticipantsType::class, 
                'entry_options' => ['label' => false],
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
                
            ])
            ->add('submit', SubmitType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Conventions::class,
        ]);
    }
}
