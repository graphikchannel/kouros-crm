<?php

namespace App\Form;

use App\Entity\Tarifs;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TarifsDevisType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->remove('tarifshoraire')
            ->remove('tarifsjournalier')
            ->remove('tarifsforfaitaire')
            ->add('horstaxe')
            ->add('ttc', null, [
                'required' => false,
                'attr' => ['class'=> "tarif"]
            ])
            ->remove('formateurs')
            ->remove('formations')
            ->remove('devis')
        ;
        
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Tarifs::class,
        ]);
    }
}
