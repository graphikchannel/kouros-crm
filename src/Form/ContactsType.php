<?php

namespace App\Form;

use App\Entity\Contacts;
use App\Form\CommentaireType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

class ContactsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('nom', null, [
            "label"=>'Nom'
        ])

        ->add('prenom', null, [
            "label"=>'Prénom'
        ])

        ->add('tel',null, [
            "label"=>'Téléphone'
        ])

        ->add('mobile', null, [
            "label"=>'Mobile'
        ])

        ->add('email', EmailType::class, [
            "label"=>'Email',
        ])
        
        ->add('principal', ChoiceType::class, [
            "label"=>false,
            "choices" => [
                "principal" => 1,
                "autre" => 0,
            ],
            "multiple" => false,
            "expanded" => true,
        ])
        
        ->add('Commentaire', CommentaireType::class, [
            'required' => false,
            'label' => false,
        ])

        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Contacts::class, 
            'Empty_data' => new Contacts([])
        ]);
    }
}
