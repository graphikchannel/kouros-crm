<?php

namespace App\Form;

use App\Form\OpcoType;
use App\Entity\Entreprise;
use App\Form\ContactsType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Intl\NumberFormatter\NumberFormatter;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;



class ProspectsEntrepriseType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('denominationSociale', null, [
                "label"=>'Dénomination sociale'
            ])

            ->add('siret', TextType::class, [
                "attr"=>["maxlength"=>"14", "minlength"=>"14"]
            ])

            ->add('ape')

            ->add('activite', null, [
                "label"=>'Activité'
            ])

            ->add('nbsalarie', null, [
                'label'=> 'Tranche salariale',
            ])

            ->add('numeroIntracommunautaireTva', null, [
                'label'=> "Numéro de TVA",
                'required'=>'false',
                ]
            )

            ->add('site', null, [
                'label'=> 'Site internet'
            ])

            
            ;



    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Entreprise::class]);
    }
}
