<?php

namespace App\Form;

use App\Entity\Devis;
use App\Entity\Niveau;
use App\Entity\Tarifs;
use App\Entity\Formations;
use App\Form\FraisDiversType;
use App\Form\TarifsDevisType;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormInterface;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\OptionsResolver\OptionsResolver;
use App\Form\DataTransformer\TarifsToNumbersTransformer;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

class DevisType extends AbstractType
{
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nbrParticipants')
            //tde 03-09-21 déjà dans entreprise
            // ->add('tva', NumberType::class, [
            //     'scale'=> 2,
            //     'attr'=> [
            //         'min'  => 0,
            //         'max'  => 99.99,
            //         'step' => 0.1,
            //     ],
            //     'required'=>'false'
            // ])

            //tde date création = date du jour
            //->add('dateCreation', DateType::class, ['widget'=> 'single_text'], ['label'=>'Création du devis'])

            ->add('dateDebut', DateType::class, [
                'widget' => 'single_text',
                'label' => 'Début de la formation',
                'required'=>false,
                'data'=> new \DateTime('now'),
                'input' => 'datetime', 
                'attr' => ['min' => date('d-m-Y')]
            ])

            ->add('dateFin', DateType::class, [
                'widget' => 'single_text',
                'required'=>false,
                'data'=> new \DateTime('now'),
                'label' => 'Fin de la formation',
                'input' => 'datetime',
                'attr' => ['min' => date('d-m-Y')]
            ])

            ->add('dureeH', NumberType::class, [
                "label" => "Nbr d'heures",
                'attr' => ['class' => "heures change"]
            ])

            //tde 03/09/21 ne sert à rien c'est l'id
            //->add('numeroDevis')

            ->add( 'methode', ChoiceType::class, [
                    'label'=>'Méthode',
                    'choices' => [
                        'Distantiel' => 1,
                        'Présentiel' => 2,
                        'Distantiel & Présentiel' => 3
                    ]
                ]
            )

            ->remove('fraisAnnexes', NumberType::class, [
                // 'scale'=> 2,
                'attr' => [
                    'min'  => 0,
                    //     'max'  => 99.99,
                    'step' => 1,
                ],
                'required' => 'false'
            ])

            ->add('formations', EntityType::class, [
                "class"=> Formations::class,
                "placeholder" => " Selectionnez votre formations ",
                "required" => false
            ])
            ->add('prix',ChoiceType::class, [
                'disabled'=>true,
                'mapped'=>false,
            ])

            ->add('niveau', null, [
                "label" => 'Niveau de la formation',
                "placeholder" => " Selectionnez le niveau "
            ])
            //->add('client')
            //->add('nomContact')
            
            ->add('nbJours',null,[
                'attr' => ['class' => "jours change"]
                //'disabled'=>true
            ])
            ->add('opco')
            ->add('tarifs', TarifsDevisType::class)


            ->add('statut', ChoiceType::class, [
                'choices' => [
                    'choices' => ['En cours' => 0, 'Accepté' => 1, 'Rejeté' => 2]
                ]
            ])
            ->add('fraisDivers', CollectionType::class,[
                    'label'=>false,
                    'entry_type' => FraisDiversType::class,
                    'entry_options' => ['label' => false],
                    'allow_add' => true,
                    'allow_delete' => true,
                    'by_reference' => false,
                    
            ]);
       
        $builder->get('formations')->addEventListener(
            FormEvents::POST_SUBMIT,
            function (FormEvent $event){
                $form = $event->getForm();
                $this->addPrixField($form->getParent(), $form->getData());
               
            }  
        ); 

        
    }
    /**
     * Rajoute le champs prix au formulaire
     *
     * @param FormInterface $form
     * @param Formations $formations
     */
    private function addPrixField(FormInterface $form, Formations $formations)
    {
        foreach ($formations->getTarifs() as $tarif) {

            $tarif->getTarifshoraire();
            $tarif->getTarifsjournalier();
            $tarif->getTarifsforfaitaire();
        }
            $builder = $form->getConfig()->getFormFactory()->createNamedBuilder(
            'prix',
            ChoiceType::class,
            null,
            [
                
                'placeholder' => 'Seletionnez le tarif',
                'required' => false,
                'mapped' => false,
                'auto_initialize' => false,
                'attr'=> ['class'=> 'change'],
                'choices' => [
                    'tarif horaire ' . $tarif->getTarifshoraire() . '€' => $tarif->getTarifshoraire(),
                    'tarif journalier ' . $tarif->getTarifsjournalier() . '€' => $tarif->getTarifsjournalier(),
                    'tarif forfaitaire ' . $tarif->getTarifsforfaitaire() . '€' => $tarif->getTarifsforfaitaire(),
                ]
            ]
        );

// Options selection type de temps a decommenter si besoin pour d'autre organisme
// permet de 'disabled le champs correspondant au type de tarif choisi "horaire ou journalier)
        // $builder->addEventlistener(
        //     FormEvents::POST_SUBMIT,
        //     function (FormEvent $event) {
        //         $form = $event->getForm();
        //         $this->addNbrField($form->getParent(), $form->getData());
        //     }
        // );
        $form->add($builder->getForm());
    }

    /**
     * ajoute les champs nombre heure et jours
     *
     * @param FormInterface $form
     */
    private function addNbrField(FormInterface $form)
    {dd('test');
        $form->add('dureeH', NumberType::class,["label" => "Nbr d'heures"])
             ->add('nbJours');   
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Devis::class,
        ]);
    }
}
