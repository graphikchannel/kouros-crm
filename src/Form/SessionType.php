<?php

namespace App\Form;

use App\Entity\Session;
use App\Entity\DaysOfWeek;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\Extension\Core\Type\ColorType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;

class SessionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('title', TextType::class)
        ->add('start', DateType::class, ['widget' => 'single_text', 'required' => false])
        ->add('end', DateType::class, ['widget' => 'single_text', 'required' => false])
        ->add('startTime', TimeType::class,['widget' => 'single_text', 'required' => false])  
        ->add('endTime', TimeType::class, ['widget' => 'single_text', 'required' => false])
        ->add('startRecur', DateType::class, ['widget' => 'single_text', 'required' => false])
        ->add('endRecur', DateType::class, ['widget' => 'single_text', 'required' => false])
        ->add('dureeJour', NumberType::class)
        ->add('allDay', ChoiceType::class, ['choices'=>['Oui'=>"1", 'Non'=>"0"], 'multiple' => false, 'expanded' => true])
        ->add('description', TextType::class)
        ->add('backgroundColor', ColorType::class)
        ->add('textColor', ColorType::class)
        ->add('borderColor', ColorType::class)
        ->add('created_at', DateType::class, ['widget' => 'single_text', 'required' => false])
        ->add('groupId', TextType::class)
        ->add('daysOfWeeks', EntityType::class, ['class' => DaysOfWeek::class, 'choice_label' => 'name', 'multiple' => true, 'expanded' => true])
        ->add('formation', null, [
            'placeholder' => 'Sélectionez une formation ',
            'required' => true
        ])
        ->add('formateur', null, ["placeholder"=>"Sélectionez un formateur"])
        ->add('niveau')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Session::class,
        ]);
    }
}
