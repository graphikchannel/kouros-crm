<?php

namespace App\Form;

use App\Entity\Adresse;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CountryType;

class AdresseType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('numero')
            ->add('voie')
            ->add('ville')
            ->add('cp')
            ->add('region')
            ->add('country', CountryType::class, array(
                "preferred_choices" => ['FR']
            ))
            ->add('complementaire')
            ->add('principale', ChoiceType::class, [
                "label"=>false,
                "choices" => [
                    "principale" => 1,
                    "autre" => 0,
                ],
                "multiple" => false,
                "expanded" => true,
            ])
            // ->add('formateurs')
            // ->add('prospects')
            // ->add('opco')
            // ->add('payeur')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Adresse::class,
        ]);
    }
}
