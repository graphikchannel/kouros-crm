<?php

namespace App\Controller;

use App\Entity\Employes;
use App\Form\EmployesType;
use App\Form\EmailsType;

use App\Repository\EmployesRepository;
use App\Repository\TemplateRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;

/**
* @Route("/employes")
*/
class EmployesController extends AbstractController
{
    #[Route('/', name: 'employes_index', methods: ['GET'])]
    public function index(EmployesRepository $employesRepository): Response
    {
        return $this->render('employes/index.html.twig', [
            'employes' => $employesRepository->findAll(),
        ]);
    }
    // #[Route('/new', name: 'employes_new', methods: ['GET', 'POST'])]

    /**
    * @Route("/new", name= "employes_new", methods={"GET", "POST"})
    */
    public function new(Request $request, UserPasswordHasherInterface $hasher,  MailerInterface $mailer , EmployesRepository $repo, TemplateRepository $repoTemplate): Response
    {
        $employe = new Employes();
        $form = $this->createForm(EmployesType::class, $employe);
        $form->handleRequest($request);
        $mdp = "";

        if ($form->isSubmitted() && $form->isValid()) {

            $entityManager = $this->getDoctrine()->getManager();

            // tde 10/09/21 vérifie si l'identifiant existe déjà
            if($repo->findOneBy(["identifiant"=>$employe->getIdentifiant()])){
                //recup l'id le plus grand en bdd et ajoute 1
                $number = $repo->findBy([], ['id' => 'desc'],1,0);
                $number = intval($number[0]->getId()) + 1 ;
                //il l'ajoute à l'identifiant généré
                $employe->setIdentifiant($employe->MakeIdentifiant() . $number);
            }else{
                $employe->setIdentifiant($employe->MakeIdentifiant());
            }

            //tde 13/09/21 : si pas de role ajout user
            if($employe->getRoles() == []) {
                $employe->setRoles(['ROLE_USER', 'ROLE_ADMIN']);
            }else{
                $employe->setRoles(['ROLE_USER']);
            }

            //tde 17/08/21 encodage mdp avec hasher ⚠encoder est déprécié
            //génère un mdp si rien de saisie dans form
            if($employe->getPassword() == ''){
                $mdp = $employe->makePassword();
                $employe->setPassword($hasher->hashPassword($employe, $mdp));
            }else{
                $employe->setPassword($hasher->hashPassword($employe, $employe->getPassword()));
                $mdp = $employe->getPassword();
            }

            $entityManager->persist($employe);
            $entityManager->flush();

            $identifiant = $employe->MakeIdentifiant();
            $user = $form->getData()->getContact()->getEmail();

            $message = (new Email())
            ->From('info@graphikchannel.com')
            ->To($user)
            ->text("bonjour vous trouvez dans cet email votre mot de passe et identifint pour se connecter sur Kouros formation " .$mdp . " " .$identifiant)
            ;

             //envoie de mail
             $mailer->send($message);
            return $this->redirectToRoute('employes_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('employes/new.html.twig', [
            'employe' => $employe,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'employes_show', methods: ['GET'])]
    public function show(Employes $employe): Response
    {
        return $this->render('employes/show.html.twig', [
            'employe' => $employe,
        ]);
    }

    #[Route('/{id}/edit', name: 'employes_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Employes $employe, UserPasswordHasherInterface $hasher): Response
    {
        $form = $this->createForm(EmployesType::class, $employe);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            //tde 17/08/21 encodage mdp avec hasher ⚠encoder est déprécié
            //génère un mdp si rien de saisie dans form
            if($employe->getPassword() == ' '){
                $mdp = $employe->makePassword();
                $employe->setPassword($hasher->hashPassword($employe, $mdp));
            }else{
                $employe->setPassword($hasher->hashPassword($employe, $employe->getPassword()));
            }
            // $entityManager->persist($employe);
            $entityManager->flush();
            //$this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('employes_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('employes/edit.html.twig', [
            'employe' => $employe,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'employes_delete', methods: ['POST'])]
    public function delete(Request $request, Employes $employe): Response
    {
        if ($this->isCsrfTokenValid('delete'.$employe->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($employe);
            $entityManager->flush();
        }

        return $this->redirectToRoute('employes_index', [], Response::HTTP_SEE_OTHER);
    }
}

