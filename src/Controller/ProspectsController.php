<?php

namespace App\Controller;

use DateTime;
use App\Entity\User;
use App\Entity\Contacts;
use App\Entity\Prospects;
use App\Form\ClientsType;
use App\Entity\Commentaire;
use App\Form\ProspectsType;
use App\Form\SearchWordType;
use App\Repository\AdresseRepository;
use App\Repository\ContactsRepository;
use App\Repository\ProspectsRepository;
use App\Repository\EntrepriseRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

/**
 * @Route("/prospects")
 */
class ProspectsController extends AbstractController
{
    /**
     * @Route("/", name="prospects_index", methods= {"GET","POST"})
     */
    public function index(ProspectsRepository $prospectsRepository, Request $request): Response
    {
        // recherche 
        $prospects = $prospectsRepository->findDernierProspect();
        $form = $this->createForm(SearchWordType::class);
        $search = $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // rechercher les formations correspondants aux mots-clés
            $prospects = $prospectsRepository->search(
                $search->get("mots")->getData(),
            );
        }
    
        return $this->render('prospects/index.html.twig', [
            // 'prospects' => $prospectsRepository->findAll(),
            'prospects' => $prospects,
            'form' => $form->createView()
        ]);
    }

    // Affichage client
    /**
     * @Route("/client", name="clients_index", methods= {"GET","POST"})
     */
    public function indexClient(ProspectsRepository $prospectsRepository, Request $request, ContactsRepository $contactsRepository): Response
    {
        // recherche 
        $prospects = $prospectsRepository->findDernierProspect();
        $form = $this->createForm(SearchWordType::class);
        $search = $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // rechercher les formations correspondants aux mots-clés
            $prospects = $prospectsRepository->search(
                $search->get('mots')->getData(),
            );
        }

        return $this->render('clients/indexClient.html.twig', [
            //'prospect' => $prospectsRepository->findAll(),
            'prospects' => $prospects,
            'form' => $form->createView(),
            'contact' => $contactsRepository->findAll()
        ]);
    }

    // New prospect
    /**
     * @Route("/new", name="prospects_new", methods= {"GET", "POST"})
     */
    public function new(Request $request, UserPasswordHasherInterface $hasher, EntrepriseRepository $entreprise, ProspectsRepository $repo, ContactsRepository $contactsRepository): Response
    {
        $prospect = new Prospects();

        $form = $this->createForm(ProspectsType::class, $prospect);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            //tde 27/08/21 : suppression de entreprise si vide
            if ($prospect->getStatut() == '2') {
                //tde 30-08-21 : je change le statut 1 en entreprise et 2 en particulier
                $prospect->setStatut('Particulier');
                $prospect->setEntreprise(null);
            } else {
                $prospect->setStatut('Entreprise');

                //je stocke les infos de contact et je supp le contact 
                //pour l'insérer après le persist afin d'avoir un id entreprise
                $contactSupp = $prospect->getEntreprise()->getContacts();
                $prospect->getEntreprise()->getContacts(null);
            }
            // dd($prospect->getEntreprise()->getContacts());
            // dd($prospect);

            //tde 20/08/21 : récup user connecté et date du jour pour s
                $user = $this->getUser();
            //   if($form->get('commentaire')["__name__"]->getData()->getTexte() != null){
            //     $commentaire = new Commentaire();
            //     $commentaire->setTexte($form->get('commentaire')['__name__']->getData()->getTexte());
            //     $commentaire->setUser($prospect);
            //     $prospect->setCommentaire($commentaire);
            // }
  
            //20-09-21 tde : ajoute un commentaire de contact
            // if($prospect->getContacts()["__name__"]->getCommentaire()[0]->getTexte() == null){
            //     $prospect->getContacts()["__name__"]->setCommentaire(null);
            // } else{
            //     //contact concerné par le commentaire
            //     //$prospect->getContacts()["__name__"]->getCommentaires()[0]->setContact()
            //     //auteur du commentaire
            //     $prospect->getContacts()["__name__"]->getCommentaire()[0]->setAuteur($user);
            // }


            //06-10-21 ajout adresses
            foreach ($prospect->getAdresses() as $key => $adresses) {
                if ($prospect->getAdresses()[$key]->getVille() == null) {
                    $prospect->removeAdress($prospect->getAdresses()[$key]);
                } else {
                    $prospect->getAdresses()[$key]->setProspects($prospect);
                }
            }

            //27-09-21 ajoute le prospect en question à tous les contacts
            foreach ($prospect->getContacts() as $key => $contacts) {
                if ($prospect->getContacts()[$key]->getNom() == null) {
                    $prospect->removeContact($prospect->getContacts()[$key]);
                } else {
                    $prospect->getContacts()[$key]->setProspects($prospect);
                }
            }

            // tde 10/09/21 vérifie si l'identifiant existe déjà
            if ($repo->findOneBy(["identifiant" => $prospect->MakeIdentifiant()])) {
                //recup l'id le plus grand en bdd et ajoute 1
                $number = $repo->findBy([], ['id' => 'desc'], 1, 0);
                $number = intval($number[0]->getId()) + 1;
                //il l'ajoute à l'identifiant généré
                $prospect->setIdentifiant($prospect->MakeIdentifiant() . $number);
            } else {
                $prospect->setIdentifiant($prospect->MakeIdentifiant());
            }

            $entityManager = $this->getDoctrine()->getManager();

            $prospect->setEtat('0'); //0 = prospect / 1=client
            //17-08-21 tde maj des droits d'accès au portail
            $prospect->setRoles(['ROLE_PROSPECT']);

            //tde 18-08-21 : insère un mdp généré automatiquement par mdp()
            if ($prospect->getPassword() == '') {
                //tde 17/08/21 encodage mdp avec hasher ⚠encoder est déprécié
                $mdp = $prospect->makePassword();
                $prospect->setPassword($hasher->hashPassword($prospect, $mdp));
            }

            //tde 18-08-21 : remplacé par génération auto de mdp
            //$prospect->setPassword($hasher->hashPassword($prospect, $prospect->getPassword()));
            $entityManager->persist($prospect);
            $entityManager->flush();

            return $this->redirectToRoute('prospects_index');
        }
        //dd($prospect);

        return $this->render('prospects/new.html.twig', [
            'prospect' => $prospect,
            'form' => $form->createView(),
            'contact' => $contactsRepository
        ]);
    }

    // Changement de statut prospect->client
    /**
     * @Route("/{id}/migration", name="prospects_migration", methods= {"GET", "POST"})
     */
    public function migration(Request $request, Prospects $prospect): Response
    {
        if ($this->isCsrfTokenValid('prospects_migration' . $prospect->getId(), $request->request->get('_token'))) {
            $prospect->setEtat('1');
            //tde 17-08-21 : maj des droit d'accès au portail
            $prospect->setRoles(['ROLE_CLIENT']);
            //tde 18-08-21 : voir envoie mail mdp au client
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($prospect);
            $entityManager->flush();
            return $this->redirectToRoute('clients_index');
        }
    }

    //tde 18-08-21 : a priori redondant et inutile car on ne crée pas de client sans passer par prospect
    //et le changement de statut est fait par la route prospects_migration
    // // new client
    // /**
    //  * @Route("/newClients", name="prospects_newClients", methods= {"GET", "POST"})
    //  */
    // public function newClients(Request $request): Response
    // {
    //     $prospect = new Prospects();
    //     $form = $this->createForm(ClientsType::class, $prospect);
    //     $form->handleRequest($request);

    //     if ($form->isSubmitted() && $form->isValid()) {
    //         $prospect->setEtat('1');
    //         //17-08-21 tde maj des droit d'accès au portail
    //         $prospect->setRoles(['ROLE_CLIENT']);
    //         $entityManager = $this->getDoctrine()->getManager();
    //         $entityManager->persist($prospect);
    //         $entityManager->flush();

    //         return $this->redirectToRoute('prospects_indexClients');
    //     }

    //     return $this->render('prospects/newClients.html.twig', [
    //         'prospect' => $prospect,
    //         'form' => $form->createView(),
    //     ]);
    // }


    // show prospects
    /**
     * @Route("/{id}", name="prospects_show", methods= {"GET"})
     */
    public function show(Prospects $prospect, ContactsRepository $contactsRepository, AdresseRepository $adresseRepository, EntrepriseRepository $entrepriseRepository): Response
    {
        return $this->render('prospects/show.html.twig', [
            'prospect' => $prospect,
            'contact' => $contactsRepository,
            'adresse' => $adresseRepository,
            'entreprise' => $entrepriseRepository
        ]);
    }
    //  show clients
    /**
     * @Route("clients/{id}", name="prospects_showClients", methods= {"GET"})
     */
    public function showClients(Prospects $prospect, ContactsRepository $contactsRepository): Response
    {
        return $this->render('clients/showClients.html.twig', [
            'prospect' => $prospect,
            'contact' => $contactsRepository->findAll()
        ]);
    }

    // edit prospects
    /**
     * @Route("/{id}/edit", name="prospects_edit", methods= {"GET", "POST"})
     */
    public function edit(Request $request, Prospects $prospect): Response
    {
        $form = $this->createForm(ProspectsType::class, $prospect);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // dd($prospect->getContacts());
            //tde 27/08/21 : suppression de entreprise si vide
            if ($prospect->getStatut() == '2') {
                //tde 30-08-21 : je change le statut 1 en entreprise et 2 en particulier
                $prospect->setStatut('Particulier');
                $prospect->setEntreprise(null);
            } else {
                $prospect->setStatut('Entreprise');
            }

            //27-09-21 ajoute le prospect en question à tous les contacts
            foreach ($prospect->getContacts() as $key => $contact) {
                if ($prospect->getContacts()[$key]->getNom() == null) {
                    $prospect->removeContact($prospect->getContacts()[$key]);
                } else {
                    $prospect->getContacts()[$key]->setProspects($prospect);
                }
            }

            //06-10-21 ajout adresses
            foreach ($prospect->getAdresses() as $key => $adresse) {
                if ($prospect->getAdresses()[$key]->getVille() == null) {
                    $prospect->removeAdress($prospect->getAdresses()[$key]);
                } else {
                    $prospect->getAdresses()[$key]->setProspects($prospect);
                }
            }

            $entityManager = $this->getDoctrine()->getManager();

            $entityManager->persist($prospect);
            $entityManager->flush();
            return $this->redirectToRoute('prospects_index');
        }
        return $this->render('prospects/edit.html.twig', [
            'prospect' => $prospect,
            'form' => $form->createView(),
        ]);
    }

    // edit clients
    /**
     * @Route("clients/{id}/edit", name="prospects_editClients", methods= {"GET", "POST"})
     */
    public function editClients(Request $request, Prospects $prospect): Response
    {
        $form = $this->createForm(ProspectsType::class, $prospect);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('clients_index');
        }

        return $this->render('clients/edit.html.twig', [
            'prospect' => $prospect,
            'form' => $form->createView(),
        ]);
    }

    //tde 18-08-21 : est ce qu'il est utile de pouvoir supprimer un client ?
    /**
     * @Route("/{id}", name="prospects_delete", methods= {"POST"})
     */
    public function delete(Request $request, Prospects $prospect): Response
    {
        if ($this->isCsrfTokenValid('delete' . $prospect->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($prospect);
            $entityManager->flush();
        }

        return $this->redirectToRoute('prospects_index');
    }
}