<?php

namespace App\Controller;

use App\Repository\FormationsRepository;
use App\Repository\SessionRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class FormationController extends AbstractController
{

    //afficher les 10 dérnieres Formations
    /**
    * @Route("/formation", name="formation")
    */
    
    public function index(FormationsRepository $formationsRepository): Response
    {
        $formations = $formationsRepository->findDerniereFormations();
        return $this->render('formation/index.html.twig', [
            'formations'=>$formations
        ]);
    }

    // TODO ajouter le repository session et récupérer les données    =>c'est fait😉
    /**
    * @Route("formation/sessionCours/", name="formation_sessionCours")
    */
    public function sessionCours(SessionRepository $sessionRepository):Response
    {
        $sessionEnCours = $sessionRepository->findSessionEnCours();
        return $this->render('formation/sessionCours.html.twig', [
            'SessionCours'=>$sessionEnCours

        ]);
    }
    // TODO ajouter le repository session et récupérer les données   =>c'est fait😉
    /**
    * @Route("formation/sessionVenir/", name="formation_sessionVenir")
    */
    public function sessionVenir(SessionRepository $sessionRepository):Response
    {
        $sessionVenir = $sessionRepository->findSessionAVenir();
        return $this->render('formation/sessionVenir.html.twig', [
            'sessionVenir'=> $sessionVenir

        ]);
    }
    // TODO ajouter le repository session et récupérer les données  =>c'est fait😉
    /**
    * @Route("formation/sessionTermine/", name="formation_sessionTermine")
    */
    public function sessionTermine(SessionRepository $sessionRepository):Response
    {
        $sessionTermine = $sessionRepository->findSessionPassee();
        return $this->render('formation/sessionTermine.html.twig', [
            'sessionPasse'=>$sessionTermine
        ]);
    }
}