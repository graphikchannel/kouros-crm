<?php

namespace App\Controller;

use App\Repository\DevisRepository;
use App\Repository\SessionRepository;
use App\Repository\ContactsRepository;
use App\Repository\ConventionsRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class DashboardController extends AbstractController
{
    /**
     * @route("/dashboard", name= "dashboard")]
     */
    // TODO ajout des données des entités: Devis + Factures + Sessions 
    public function index(ContactsRepository $contactsRepository ,DevisRepository $devisRepository, SessionRepository $sessionRepository,ConventionsRepository $convRepository ): Response
    {

        $eventsSessions = $sessionRepository->findAll();
    
        $rdvsSession = [];
    
            foreach($eventsSessions as $eventSession){
                $daysOfWeeks = [];
                foreach($eventSession->getDaysOfWeeks() as $d){
                    array_push($daysOfWeeks, $d->getNumber().'');
                }
                //
                $rdvsSession[] = [
                    'id' => $eventSession->getId(),
                    'title' => $eventSession->getTitle(),
                    'start' => (!is_null($eventSession->getStart()))? $eventSession->getStart()->format('Y-m-d H:i:s') : null,
                    'end' => (!is_null($eventSession->getEnd()))? $eventSession->getEnd()->format('Y-m-d H:i:s') : null,
                    'description' => $eventSession->getDescription(),
                    'backgroundColor' => $eventSession->getBackgroundColor(),
                    'borderColor' => $eventSession->getBorderColor(),
                    'textColor' => $eventSession->getTextColor(),
                    'allDay' => $eventSession->getAllDay(),
                    'startTime' =>(!is_null($eventSession->getStartTime()))? $eventSession->getStartTime()->format('H:i:s') : null,
                    'endTime' => (!is_null($eventSession->getEndTime()))? $eventSession->getEndTime()->format(' H:i:s') : null,
                    'startRecur' => (!is_null($eventSession->getStartRecur()))? $eventSession->getStartRecur()->format('Y-m-d') : null,
                    'endRecur' => (!is_null($eventSession->getEndRecur()))? $eventSession->getEndRecur()->format('Y-m-d') : null,
                    'daysOfWeek' => $daysOfWeeks,
                    'groupId' => $eventSession->getGroupId(),
                    ];
                }

            $dataSession = json_encode($rdvsSession);

            //
    $toto  = $devisRepository->findBy(['statut'=>'1']);
        return $this->render('dashboard/index.html.twig', [
            // 'devi'=>$devisRepository->findAll(),
            'dataSession' => $dataSession, 
            'eventsSessions' => $eventsSessions,
            'devisValide'=>$devisRepository->findBy(['statut'=>'1']),
            'devisCours'=>$devisRepository->findBy(['statut'=>'0']),
            'devisRejete'=>$devisRepository->findBy(['statut'=>'2']),
            'conventions'=>$convRepository->findAll(),
            'contacts'=>$contactsRepository->findAll(),

        ]);
        
    }
}
