<?php

namespace App\Controller;

use App\Entity\User;
use App\Entity\Tarifs;
use App\Entity\Formateurs;
use App\Entity\Formations;
use App\Form\FormateursType;
use App\Form\SearchWordType;
use Symfony\Component\Mime\Email;
use App\Repository\UserRepository;
use App\Repository\FormateursRepository;
use App\Repository\FormationsRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

/**
* @Route("/formateurs")
*/
class FormateursController extends AbstractController
{
    /**
    * @Route("/", name= "formateurs_index", methods={"GET", "POST"})
    */
    public function index(FormateursRepository $formateursRepository, Request $request): Response
    {   
        // recherche 
        $formateurs = $formateursRepository->findDernierFormateur();
        $form= $this->createForm(SearchWordType::class);
        $search = $form->handleRequest($request);
        
        
        if($form->isSubmitted() && $form->isValid()){
            
            $formateurs= $formateursRepository->search(
            $search->get("mots")->getData(),
            );
        }
        return $this->render('formateurs/index.html.twig', [
            'formateurs' => $formateurs,
            'form'=> $form->createView(),
        ]);
    }

    /**
    * @Route("/new", name= "formateurs_new", methods={"GET", "POST"})
    */
    public function new(Request $request, UserPasswordHasherInterface $hasher, FormationsRepository $formationsRepository, MailerInterface $mailer, FormateursRepository $repo): Response
    {
        $formateur = new Formateurs();
        $form = $this->createForm(FormateursType::class, $formateur);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            //tde 16/08/21 attribut le role ROLE_FORMATEUR
            $formateur->setRoles(['ROLE_FORMATEUR']);
            
            //tde 17/08/21 encodage mdp avec hasher ⚠encoder est déprécié
            $mdp = $formateur->makePassword();
            $formateur->setPassword($hasher->hashPassword($formateur, $mdp));

            // tde 10/09/21 vérifie si l'identifiant existe déjà
            if($repo->findOneBy(["identifiant"=>$formateur->getIdentifiant()])){
                //recup l'id le plus grand en bdd et ajoute 1
                $number = $repo->findBy([], ['id' => 'desc'],1,0);
                $number = intval($number[0]->getId()) + 1 ;
                //il l'ajoute à l'identifiant généré
                $formateur->setIdentifiant($formateur->MakeIdentifiant() . $number);
            }else{
                $formateur->setIdentifiant($formateur->MakeIdentifiant());
            }
            //dd($formateur);
            $entityManager->persist($formateur);
            $entityManager->flush();

            //envoie de mdp généré par l'email
           // $donnees = $form->getData();// récup les données du formulaire
            $user = $form->getData()->getContacts()->getEmail();
            $message = (new Email())
            ->From('info@graphikchannel.com')
            ->To($user)
            ->text('Bonjour voici votre mot de passe'.$mdp);

            //envoie de mail
            $mailer->send($message);

            return $this->redirectToRoute('formateurs_index');
        }

        return $this->render('formateurs/new.html.twig', [
            'formateur' => $formateur,
            'form' => $form->createView(),
            'formation' => $formationsRepository
        ]);
    }

    /**
    * @Route("/{id}", name= "formateurs_show", methods={"GET"})
    */
    public function show(Formateurs $formateur): Response
    {
        $formateurs = $formateur->getContacts();
        return $this->render('formateurs/show.html.twig', [
            'formateur' => $formateur,
            'contacts'=> $formateurs
        ]);
    }

    /**
    * @Route("/{id}/edit", name= "formateurs_edit", methods={"GET", "POST"})
    */
    public function edit(Request $request, Formateurs $formateur, UserPasswordHasherInterface $hasher): Response
    {
        $form = $this->createForm(FormateursType::class, $formateur);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            //tde 16/08/21 attribut le role ROLE_FORMATEUR
            $formateur->setRoles(['ROLE_FORMATEUR']);

            //tde 16/08/21 encodage mdp avec hasher ⚠encoder est déprécié
            $formateur->setPassword($hasher->hashPassword($formateur, $formateur->getPassword()));
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('formateurs_index');
        }

        return $this->render('formateurs/edit.html.twig', [
            'formateur' => $formateur,
            'form' => $form->createView(),
        ]);
    }

    /**
    * @Route("/{id}", name= "formateurs_delete", methods={"POST"})
    */
    public function delete(Request $request, Formateurs $formateur): Response
    {
        if ($this->isCsrfTokenValid('delete'.$formateur->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($formateur);
            $entityManager->flush();
        }

        return $this->redirectToRoute('formateurs_index');
    }
}
