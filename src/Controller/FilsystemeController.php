<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;
use Symfony\Component\Filesystem\Filesystem;
use App\Form\FilsystemeType;
use Symfony\Component\Finder\Finder;


class FilsystemeController extends AbstractController
{
    /**
     * @Route("/creer_email", name="email")
     */
    public function index(Request $request): Response
    {
        $form = $this->createForm(FilsystemeType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
 
            $data= $form->getData();
          
           $titre =  $form->get('titre')->getData(); // recup les données du champs titre
           $contenu =  $form->get('message')->getData();// recup les données du champs message // il faut un filtre (raw)!!!!!!

            // dd($contenu);
            $email = new Filesystem();
             
            $projectPath = $this->getParameter('kernel.project_dir');  // recup la racine du projet
            // $email = $projectPath.'//public//emails//'.$titre;
            $newEmail = $projectPath.'//templates//emails//'.$titre. '.html.twig';  // le chemin absolut vers templates/emails => sinon le répertoire et les fichiers sont créés par défaut dans public
            // dd($email);
           
            try {

                // $email->mkdir('emails/', 0700);// créer le repertoire   

                $email->dumpFile($newEmail , $contenu); // enregistre le contenu dans un fichier qui a le nom de $titre avec une concaténation pour avoir l'extension pour ce fichier. il aura aussi $contenu qui est le contenu du fichier (il faut mettre un filtre)

                $email->appendToFile($newEmail , $contenu); // ajoute du contenu a a fin du fichier (sur le formaulre il faut mettre le non du fichier qui existe déjà et qu'on veut apporter des modif)

                // $email->remove(['emails/' ]); // supprimer un fichier ou un contenu 

            } catch (IOExceptionInterface $exception) {
                echo "An error occurred while creating your directory at ".$exception->getPath();
            } 

        }

        return $this->render('filsysteme/formEmail.html.twig', [
            'emails'=>$form->createView()
        ]);
    }

    /**
     * @Route("/emails", name="findFiles")
     */
    public function findFiles(){


        $finder = new Finder();
        //dd($finder);
        $dir = $this->getParameter('kernel.project_dir');
        $finder->in($dir. '//templates//emails');
        $finder->files()->name('*.html.twig'); // pourquoi ça ne marche pas!!!!!!!!
        ;
        
              
        return $this->render('filsysteme/index.html.twig', [
            'finders'=>$finder
        ]);
    }

}
