<?php

namespace App\Controller;

use App\Entity\Contacts;
use App\Entity\Entreprise;
use App\Form\EntrepriseType;
use App\Form\SearchWordType;
use App\Repository\EntrepriseRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
    * @Route("/entreprise")
    */
class EntrepriseController extends AbstractController
{
    /**
    * @Route("/", name= "entreprise_index", methods= {"GET", "POST"})
    */
    public function index(EntrepriseRepository $entrepriseRepository, Request $request): Response
    {
        // recherche 
        $entreprise = $entrepriseRepository->findAll();
        $form= $this->createForm(SearchWordType::class);
        $search = $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $entreprise= $entrepriseRepository->search(
            $search->get('mots')->getData(),
            );
        }
        return $this->render('entreprise/index.html.twig', [
            'entreprise' => $entreprise,
            'form'=> $form->createView(),
        ]);
    }

    /**
    * @Route("/new", name= "entreprise_new", methods= {"GET", "POST"})
    */
    public function new(Request $request, EntrepriseRepository $repo): Response
    {
        $entreprise = new Entreprise();

        $form = $this->createForm(EntrepriseType::class, $entreprise);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();

            // $contact2 = new Contacts();
            // $contact2->setNom($entreprise->getContacts->getNom());
            // $contact2->setPrenom($entreprise->getContacts->getPrenom());
            // $contact2->setTel($entreprise->getContacts->getTel());
            // $contact2->setMobile($entreprise->getContacts->getMobile());
            // $contact2->setEmail($entreprise->getContacts->getEmail());
            // $contact2->setEntreprise($repo->findOneBy(['denominationSociale'=>$repo->getDenominationSociale()]));
            // $contact2->setPrincipal('2');
            // $entreprise->getContacts()->add($contact2);


            $entityManager->persist($entreprise);
            $entityManager->flush();

            return $this->redirectToRoute('entreprise_index');
        }

        return $this->render('entreprise/new.html.twig', [
            'entreprise' => $entreprise,
            'form' => $form->createView(),
        ]);
    }

    /**
    * @Route("/{id}", name= "entreprise_show", methods= {"GET"})
    */
    public function show(Entreprise $entreprise): Response
    {
        return $this->render('entreprise/show.html.twig', [
            'entreprise' => $entreprise,
        ]);
    }

    /**
    * @Route("/{id}/edit", name= "entreprise_edit", methods= {"GET", "POST"})
    */
    public function edit(Request $request, Entreprise $entreprise): Response
    {
        $form = $this->createForm(EntrepriseType::class, $entreprise);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('entreprise_index');
        }

        return $this->render('entreprise/edit.html.twig', [
            'entreprise' => $entreprise,
            'form' => $form->createView(),
        ]);
    }

    /**
    * @Route("/{id}", name= "entreprise_delete", methods= {"POST"})
    */
    public function delete(Request $request, Entreprise $entreprise): Response
    {
        if ($this->isCsrfTokenValid('delete'.$entreprise->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($entreprise);
            $entityManager->flush();
        }

        return $this->redirectToRoute('entreprise_index');
    }
}
