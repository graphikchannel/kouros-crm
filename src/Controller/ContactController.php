<?php

namespace App\Controller;

use App\Repository\ContactsRepository;
use App\Repository\ProspectsRepository;
use App\Repository\EntrepriseRepository;
use App\Repository\OpcoRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ContactController extends AbstractController
{

    //afficher les 10 dérniers Prospects
    
    /**
     * @Route("/contact", name="contact")
     */
    public function index(ProspectsRepository $ProspectsRepository): Response
    {
        return $this->render('contact/index.html.twig', [
            'prospects' => $ProspectsRepository->findDernierProspect(),
            
        ]);
    }


    //afficher les 10 dérniers Clients => à faire
    
    /**
     * @Route("/contact/client/", name="contact_client")
     */
    public function client( ContactsRepository $contactsRepository): Response
    {
        return $this->render('contact/client.html.twig', [
            //17-08-21 tde à tester - voir tous les clients
            'contacts' => $contactsRepository->findAll()
        ]);
    }

    //afficher les 10 dérniers Entreprises
    
    /**
     * @Route("/contact/entreprise/", name="contact_entreprise")
     */
    public function entreprise(EntrepriseRepository $EntrepriseRepository): Response
    {
        $entreprise = $EntrepriseRepository->findDerniereEntreprises();
        return $this->render('contact/entreprise.html.twig', [
            'entreprise' => $entreprise
        ]);
    }
    
    // TODO ajouter le repository OPCO (10 derniers) et récupérer les données  =>c'est fait😉
    
    /**
     * @Route("/contact/opco/", name="contact_opco")
     */
    public function opco(OpcoRepository $opcoRepository, ContactsRepository $contactsRepository): Response
    {
        return $this->render('contact/opco.html.twig', [
            'opco' => $opcoRepository,
            'contact' => $contactsRepository->findAll()
        ]);
    }
}