<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\ResetPassType;
use App\Form\EditPasswordType;

use App\Repository\UserRepository;
use App\Repository\ContactsRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Csrf\TokenGenerator\TokenGeneratorInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;

class SecurityController extends AbstractController
{
    /**
     * @Route("/login", name="app_login")
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        // if ($this->getUser()) {
        //     return $this->redirectToRoute('target_path');
        // }

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/login.html.twig', ['last_username' => $lastUsername, 'error' => $error]);
    }

    /**
     * @Route("/logout", name="app_logout")
     */
    public function logout()
    {
        throw new \LogicException('This method can be blank - it will be intercepted by the logout key on your firewall.');
    }


    

//   mot de passe oublié

    /**
     * @Route("/oubli_pass", name="oublie_mdp")
     */
    public function oublie_mdp(Request $request, UserRepository $userRepo, MailerInterface $mailer, TokenGeneratorInterface $tokenGenerator){
        //création du form poue demander l'email 
        $form = $this->createForm(ResetPassType::class);

        //traiter le form
        $form->handleRequest($request);

        //si le form est valide
        if($form->isSubmitted() && $form->isValid()){
            // récupération de données
            $donnees = $form->getData();
            dump($donnees);

            //chercher si un user à cet identifiant
            $user = $userRepo->findOneByIdentifiant($donnees['identifiant']);


            //si user n'existe pas , on envoie un message flash , redirection vers connexion
            if(!$user){
                $this->addFlash('danger', 'Cet identifiant n\'existe pas');
                return $this->redirectToRoute('app_login');
            }

            //générer le token une fois on a les conditions
            $token = $tokenGenerator->generateToken();

            //flush de token en bdd
            try{
                $user->setResetToken($token);
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($user);
                $entityManager->flush();
             
            }
            catch(\Exception $e){
                $this->addFlash('warning', 'Une erreur est survenue : ' .$e->getMessage());
                return $this->redirectToRoute('app_login');
            }

            //génération de l'email de réinitialisation de mdp
            $url = $this->generateUrl('app_reset_password', ['token' => $token],
            UrlGeneratorInterface::ABSOLUTE_URL);

            //envoie le message
            $message = (new TemplatedEmail())
                ->From('info@graphikchannel.com')
                ->To($user->getContact()->getEmail())
                ->htmlTemplate('emails/oublieMdp.html.twig')
                ->context([
                    "url" => $url
                ]);

                //envoie de mail
                $mailer->send($message);
                

                $this->addFlash('message', ' Un e-mail de réinitialisation de mot de passe vous a été envoyé ');

                // return $this->redirectToRoute('app_login');
        }

        //
        return $this->render('security/oublieMdp.html.twig',[ 'FormIdentifiant'=>$form->createView()]);
    }

    /**
     * @Route("reset_pass/{token}" , name="app_reset_password")
     */
    public function resetPassword($token, Request $request, UserPasswordHasherInterface $hasher){

        //chercher si un user a ce token
        $user = $this->getDoctrine()->getRepository(User::class)->findOneBy(['reset_token'=> $token]);

        if(!$user){
            $this->addFlash('danger', 'Token inconnu');
            return $this->redirectToRoute('app_login');
        }

        if($request->isMethod('POST')){

            $user->setResetToken(null);
            $user->setPassword($hasher->hashPassword($user, $request->get('password')));
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            $this->addFlash('message', 'Mot de passe modifié avec succés');

            return $this->redirectToRoute('app_login');

        }else{
            return $this->render('security/reset_password.html.twig', ['token'=> $token ]);
        }
    }





// pour modifier le mot de passe
    /**
     * @Route("edit_password", name="edit_password")
     */
    public function editPassword(Request $request,UserRepository $userRepo, UserPasswordHasherInterface $hasher){

        if($request->isMethod('POST')){

            $user= $this->getUser();
            $mdp =$request->request->get('pass');
            dd($mdp, $user); 
            dd($request->request->get('pass') == $request->request->get('pass2'));

            // vérifier que les deux mots de pase sont identique
            if($request->request->get('pass') == $request->request->get('pass2')){
                $em = $this->getDoctrine()->getManager();
                $user->setPassword($hasher->hashPassword($user, $request->request->get('pass')));
                $em->flush('message', 'Mot de passe mis à jour avec succès');

                
            }else{
                $this->addFlash('danger', 'Les deux mot de passe ne sont pas identiques');
            }
        }




        // $form = $this->createForm(EditPasswordType::class);
        // $form->handleRequest($request);

        // if($form->isSubmitted() && $form->isValid()){
        //     $data = $form->getData();

        //     $user = $this->getUser();// recup l'user conneceté
        //     $password = $user->getPassword();
        //     $oldpassword = $data['oldPassword']; // recup le password saisie 
        //     $new_password = $data['newpassword'];
            
        //      dd($oldpassword == $password);


            //comparaison de mdp enregistrer en bdd et celui qui est saisie
            //la condition est false mais il rentre dans la condition?????????????????
            // if( $hasher->isPasswordValid($user, $password)){

            //     $user->setPassword($hasher->hashPassword($old_password, $new_password));
            //     $entityManager = $this->getDoctrine()->getManager();
            //     $entityManager->persist($user);
            //     $entityManager->flush();

            // }else{
            //     $this->addFlash('message', 'le mot de passe n\'est pas valide');
            // }  
            // return $this->redirectToRoute('app_logout');

            
        
        return $this->render('security/editPassword.html.twig');
    }
}
