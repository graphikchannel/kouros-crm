<?php

namespace App\Controller;

use App\Repository\AdresseRepository;
use App\Repository\ConventionsRepository;
use App\Repository\DevisRepository;
use App\Repository\FormationsRepository;
use App\Repository\ProspectsRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class GestionController extends AbstractController
{
    /**
     * @Route("/gestion", name= "gestion")
     * */

    // TODO ajouter le repository devis (10 derniers) et récupérer les données  =>c'est fait 😉
    public function index(DevisRepository $devisRepository, FormationsRepository $formationsRepository): Response
    {
        $formations = $formationsRepository->findAll();
        return $this->render('gestion/index.html.twig', [
            'devis' => $devisRepository->findDernierDevis(),
            'formations' => $formations,
        ]);
    }
    // TODO ajouter le repository facture et récupérer les données 
    /**
     * @Route("gestion/facture/", name="gestion_facture")
     */
    public function facture(): Response
    {
        return $this->render('gestion/facture.html.twig', [

        ]);
    }

    // TODO ajouter le repository Conventions (10 derniers) et récupérer les données  =>c'est fait 😉
    /**
     * @Route("gestion/convention/", name="gestion_convention")
     */
    public function convention(ConventionsRepository $conventionsRepository, ProspectsRepository $prospectsRepository, AdresseRepository $adresseRepository): Response
    {
        return $this->render('gestion/convention.html.twig', [

            'conventions' => $conventionsRepository->findDerniereConventions(),
            'prospects' => $prospectsRepository->findAll(),
            'adresse' =>$adresseRepository->findAll()
        ]);
    }
    // TODO ajouter le repository reglement et récupérer les données 
    /**
     * @Route("gestion/reglement/", name="gestion_reglement")
     */
    public function reglement(): Response
    {
        return $this->render('gestion/reglement.html.twig', [

        ]);
    }
}
