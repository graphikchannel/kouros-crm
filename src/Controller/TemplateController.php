<?php

namespace App\Controller;

use App\Entity\Template;
use App\Form\TemplateType;
use App\Repository\TemplateRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;
use Symfony\Component\Filesystem\Filesystem;

/**
 * @Route("/template")
 */
class TemplateController extends AbstractController
{
    /**
     * @Route("/", name="template_index", methods={"GET"})
     */
    public function index(TemplateRepository $templateRepository): Response
    {
        return $this->render('template/index.html.twig', [
            'templates' => $templateRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="template_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $template = new Template();
        $form = $this->createForm(TemplateType::class, $template);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $data= $form->getData();

            $titre =  $form->get('filename')->getData(); // recup les données du champs titre
            $contenu =  $form->get('texte')->getData();// recup les données du champs message // il faut un filtre (raw)!!!!!!

            $email = new Filesystem();
             $projectPath = $this->getParameter('kernel.project_dir');  // recup la racine du projet
             $newEmail = $projectPath.'//public//templates_email//'.$titre. '.html.twig';  // le chemin absolut vers templates/emails => sinon le répertoire et les fichiers sont créés par défaut dans public
            
            try {
                 $email->dumpFile($newEmail , $contenu); // enregistre le contenu dans un fichier qui a le nom de $titre avec une concaténation pour avoir l'extension pour ce fichier. il aura aussi $contenu qui est le contenu du fichier (il faut mettre un filtre)
            } catch (IOExceptionInterface $exception) {
                echo "An error occurred while creating your directory at ".$exception->getPath();
            } 

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($template);
            $entityManager->flush();

            return $this->redirectToRoute('template_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('template/new.html.twig', [
            'template' => $template,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="template_show", methods={"GET"})
     */
    public function show(Template $template): Response
    {
        return $this->render('template/show.html.twig', [
            'template' => $template,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="template_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Template $template): Response
    {
        $form = $this->createForm(TemplateType::class, $template);
        $form->handleRequest($request);

        $titre =  $form->get('filename')->getData(); // recup les données du champs titre
        $contenu =  $form->get('texte')->getData(); // recup les données du champs message // il faut un filtre (raw)!!!!!!

        // dd($contenu);
        $email = new Filesystem();

        if ($form->isSubmitted() && $form->isValid()) {
            $projectPath = $this->getParameter('kernel.project_dir');  // recup la racine du projet
            // $email = $projectPath.'//public//emails//'.$titre;
            $editEmail = $projectPath . '//public//templates_email//' . $titre . '.html.twig'; 
        
            $email->dumpFile($editEmail, $contenu);
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('template_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('template/edit.html.twig', [
            'template' => $template,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="template_delete", methods={"POST"})
     */
    public function delete(Request $request, Template $template): Response
    {
        

        if ($this->isCsrfTokenValid('delete'.$template->getId(), $request->request->get('_token'))) {
            $titre= $template->getFilename();
            $projectPath = $this->getParameter('kernel.project_dir');  // recup la racine du projet
            $template_twig = $projectPath . '//public//templates_email//' . $titre . '.html.twig';

            $filesystem = new Filesystem();
            $filesystem->remove($template_twig);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($template);
            $entityManager->flush();
        }

        return $this->redirectToRoute('template_index', [], Response::HTTP_SEE_OTHER);
    }
}
