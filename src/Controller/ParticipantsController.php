<?php

namespace App\Controller;

use App\Entity\Participants;
use App\Form\ParticipantsType;
use App\Repository\ParticipantsRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;


/**
 * @Route("/participants")
 */
class ParticipantsController extends AbstractController
{
    /**
     * @Route("/", name="participants_index", methods={"GET"})
     */
    public function index(ParticipantsRepository $participantsRepository): Response
    {
        return $this->render('participants/index.html.twig', [
            'participants' => $participantsRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="participants_new", methods={"GET","POST"})
     */
    public function new(Request $request,UserPasswordHasherInterface $hasher, MailerInterface $mailer, ParticipantsRepository $repo): Response
    {
        $participant = new Participants();
        $form = $this->createForm(ParticipantsType::class, $participant);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();

            // tde 10/09/21 vérifie si l'identifiant existe déjà
            if($repo->findOneBy(["identifiant"=>$participant->getIdentifiant()])){
                //recup l'id le plus grand en bdd et ajoute 1
                $number = $repo->findBy([], ['id' => 'desc'],1,0);
                $number = intval($number[0]->getId()) + 1 ;
                //il l'ajoute à l'identifiant généré
                $participant->setIdentifiant($participant->MakeIdentifiant() . $number);
            }else{
                $participant->setIdentifiant($participant->MakeIdentifiant());
            }
dd($participant);

            //tde 17/08/21 encodage mdp avec hasher ⚠encoder est déprécié
            $mdp = $participant->makePassword();
            $participant->setPassword($hasher->hashPassword($participant, $mdp));
            
            $participant->setRoles(['ROLE_PARTICIPANT']);

            $entityManager->persist($participant);
            $entityManager->flush();

            //récuperer l'email qui est saisie dans le formulaire 
            $user = $form->getData()->getEmail();
            
            //envoie de mdp généré par l'email
            $message = (new Email())
            ->From('info@graphikchannel.com')
            ->To($user)
            ->text('Bonjour voici votre mot de passe  : '.$mdp);
            

            //envoie de mail
            $mailer->send($message);


            // j'intercepte la redirection pour vérifier ll'envoie de mail mais cette ligne sera décommenter plustard
            // return $this->redirectToRoute('participants_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('participants/new.html.twig', [
            'participant' => $participant,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="participants_show", methods={"GET"})
     */
    public function show(Participants $participant): Response
    {
        return $this->render('participants/show.html.twig', [
            'participant' => $participant,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="participants_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Participants $participant, ParticipantsRepository $repo , UserPasswordHasherInterface $hasher): Response
    {
        $form = $this->createForm(ParticipantsType::class, $participant);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if ($repo->findOneBy(["identifiant" => $participant->getIdentifiant()])) {
                //recup l'id le plus grand en bdd et ajoute 1
                $number = $repo->findBy([], ['id' => 'desc'], 1, 0);
                $number = intval($number[0]->getId()) + 1;
                //il l'ajoute à l'identifiant généré
                $participant->setIdentifiant($participant->MakeIdentifiant() . $number);
            } else {
                $participant->setIdentifiant($participant->MakeIdentifiant());
            }
            $participant->setPassword($hasher->hashPassword($participant, $participant->getPassword()));
            $participant->setRoles(['ROLE_PARTICIPANT']);
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('participants_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('participants/edit.html.twig', [
            'participant' => $participant,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="participants_delete", methods={"POST"})
     */
    public function delete(Request $request, Participants $participant): Response
    {
        if ($this->isCsrfTokenValid('delete'.$participant->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($participant);
            $entityManager->flush();
        }

        return $this->redirectToRoute('participants_index', [], Response::HTTP_SEE_OTHER);
    }
}
