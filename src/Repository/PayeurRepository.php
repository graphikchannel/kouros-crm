<?php

namespace App\Repository;

use App\Entity\Payeur;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Payeur|null find($id, $lockMode = null, $lockVersion = null)
 * @method Payeur|null findOneBy(array $criteria, array $orderBy = null)
 * @method Payeur[]    findAll()
 * @method Payeur[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PayeurRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Payeur::class);
    }

    // /**
    //  * @return Payeur[] Returns an array of Payeur objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Payeur
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
