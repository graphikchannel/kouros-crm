<?php

namespace App\Repository;

use App\Entity\FraisDivers;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method FraisDivers|null find($id, $lockMode = null, $lockVersion = null)
 * @method FraisDivers|null findOneBy(array $criteria, array $orderBy = null)
 * @method FraisDivers[]    findAll()
 * @method FraisDivers[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FraisDiversRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, FraisDivers::class);
    }

    // /**
    //  * @return FraisDivers[] Returns an array of FraisDivers objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?FraisDivers
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
