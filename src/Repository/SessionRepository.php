<?php

namespace App\Repository;

use App\Entity\Session;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Session|null find($id, $lockMode = null, $lockVersion = null)
 * @method Session|null findOneBy(array $criteria, array $orderBy = null)
 * @method Session[]    findAll()
 * @method Session[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SessionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Session::class);
    }

    //repo pour session à venir
    public function findSessionAVenir()
    {
        $qb = $this->createQueryBuilder('s')
            ->orderBy('s.id', 'DESC')
            ->andWhere("s.start > :date")
            ->setParameter("date", new \Datetime)
            ->setMaxResults(10);
           return $qb->getQuery()->getResult();
    }

    //repo pour session passée
    public function findSessionPassee()
    {
        $qb = $this->createQueryBuilder('s')
            ->orderBy('s.id', 'DESC')
            ->andWhere("s.end < :date")
            ->setParameter("date", new \Datetime)
            ->setMaxResults(10);
           return $qb->getQuery()->getResult();
    }

    
    //repo pour session en cours
    public function findSessionEnCours()
    {
        $qb = $this->createQueryBuilder('s')
            ->orderBy('s.id', 'DESC')
            ->andWhere("s.start < :date")
            ->andWhere("s.end > :date")
            ->setParameter("date", new \Datetime)
            ->setMaxResults(10);
           return $qb->getQuery()->getResult();
    }

    public function findDerniereSession()
    {
        $qb = $this->createQueryBuilder('o')
            ->orderBy('o.id', 'DESC');
        return $qb->getQuery()->getResult();
    }
    

    // /**
    //  * @return Session[] Returns an array of Session objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Session
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
