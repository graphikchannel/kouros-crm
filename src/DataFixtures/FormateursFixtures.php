<?php

namespace App\DataFixtures;

use App\Entity\Formateurs;
use App\DataFixtures\ContactsFixtures;
use Doctrine\Persistence\ObjectManager;
use App\DataFixtures\EntrepriseFixtures;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;


class FormateursFixtures extends Fixture implements DependentFixtureInterface
{
    private $hasher; 

    public function __construct(UserPasswordHasherInterface $hasher){
        $this->hasher = $hasher;
    }
    public function load(ObjectManager $manager)
    {
        $entreprise = $this->getReference('entreprise');
        $contact = $this->getReference('contact');
        $formateurs = [
            1 => [
                'methode'=>'',
                'password'=>'mdp',
                'identifiant'=>'TGaudfroy',
                'entreprise'=>'51'
            ],
            
        ];

        foreach($formateurs as $key=>$value){
            $formateur = new Formateurs();
            $formateur -> setEntreprise($entreprise);
            $formateur -> setMethode($value['methode']);
            $formateur -> setContacts($contact);
            $formateur -> setRoles(['ROLE_FORMATEUR']);
            $formateur -> setIdentifiant($value['identifiant']);
            $password= $this->hasher->hashPassword($formateur, $value['password']);
            $formateur->setPassword($password);

            $manager->persist($formateur);

            $this->setReference('formateur',$formateur );

        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            EntrepriseFixtures::class,
            ContactsFixtures::class,
            
        ];
    }
}
