<?php

namespace App\DataFixtures;

use App\Entity\Prospects;
use App\DataFixtures\DevisFixtures;
use Doctrine\Persistence\ObjectManager;
// use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use App\DataFixtures\EntrepriseFixtures;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;



class ProspectsFixtures extends Fixture
{
     public function __construct(UserPasswordHasherInterface $hasher)
        {
            $this->hasher = $hasher;
        }

    public function load(ObjectManager $manager)
    {
        
        
        $prospects = [
            1=>[
                'statut'=>'Entreprise',
                'etat'=>'0',
                'identifiant'=>'BGates'
            ],
            2=>[
                'statut'=>'Entreprise',
                'etat'=>'0',
                'identifiant'=>'SJobs'
            ],
            3=>[
                'statut'=>'Particulier',
                'etat'=>'1',
                'identifiant'=>'MZuckerberg'
            ],
        ];

        foreach($prospects  as $key=>$value){
            
            $prospect = new Prospects();
            $prospect->setStatut($value['statut']);
            $prospect->setEtat($value['etat']);
            $prospect->setIdentifiant($value['identifiant']);
            $prospect->setRoles(['ROLE_PROSPECT']);
            $password = $this->hasher->hashPassword($prospect,'mdp');
            $prospect->setPassword($password);
            
            $manager->persist($prospect);
            $this->setReference('prospect', $prospect);
        }

        $manager->flush();
    }

    

}
