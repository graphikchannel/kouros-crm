<?php

namespace App\DataFixtures;

use App\Entity\Niveau;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class NiveauFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $niveaux = [
            1=>[
                'nom'=>'Initiation',
            ],
            2=>[
                'nom'=>'Intermédiaire',
            ],
            3=>[
                'nom'=>'Perfectionnement',
            ],
        ];

        foreach($niveaux as $key=>$value){
            $niveau = new Niveau();
            $niveau->setNom($value['nom']);
            $manager->persist($niveau);
            $this->setReference('niveau',$niveau );

        }
        $manager->flush();
    }
}
