<?php

namespace App\DataFixtures;

use App\Entity\Contacts;
use Doctrine\Persistence\ObjectManager;
use App\DataFixtures\EntrepriseFixtures;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class ContactsFixtures extends Fixture //implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        
        $contacts = [

            1=> [
                'nom'=>'Michel',
                'prenom'=>'Sébastien',
                'email'=>'sebastien.michel@cetim.fr',
                'tel'=>'0272741020',
                'mobile'=>'0665432987',
                'principal' => '1'
            ],
            2 => [
                'nom' => 'Blavette',
                'prenom' => 'Gaelle',
                'email' => 'gaelleblavette@socgen.com',
                'tel' => '0123456789',
                'mobile' => '0621407909',
                'principal' => '0'
            ],
            3 => [
                'nom'=>'Mamah-Djiman',
                'prenom'=>'Khamal',
                'email'=>'khamal3@yahoo.ca',
                'tel'=>'0650433160',
                'mobile'=>'0698765432',
                'principal' =>'1'
            ],
            4 => [
                'nom' => 'Baup',
                'prenom' => 'Philippe',
                'email' => 'philippe.baup@agir.pro',
                'tel' => '0654698732',
                'mobile' => '0698765432',
                'principal' =>'0'
            ]
        ];
        foreach($contacts as $key => $value){
            
            $contact = new Contacts();
            $contact->setNom($value['nom']);
            $contact->setPrenom($value['prenom']);
            $contact->setEmail($value['email']);
            $contact->setTel($value['tel']);
            $contact->setPrincipal($value['principal']);
            $manager->persist($contact);

            $this->setReference('contact',$contact );

        }
        $manager->flush();
    }

    
}
