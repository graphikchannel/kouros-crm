<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use App\Entity\Formations;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use App\DataFixtures\SecteurFixtures;
use App\DataFixtures\NiveauFixtures;




class FormationsFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
       $formations = [
           1=>[
               'titre'=>'Symfony',
               'prixJour'=>'590',
               'programme'=>'  Présentation
                                Les objets littéraux
                                Ajouter & accéder aux propriétés
                                Les property shorthand',
               'niveau'=>[],
               'secteur'=>[],    
           ],
           2=>[
            'titre'=>'Javascript',
            'prixJour'=>'590',
            'programme'=>'  Présentation
                             Les objets littéraux
                             Ajouter & accéder aux propriétés
                             Les property shorthand',
            'niveau'=>[],
            'secteur'=>[],
        
        ],
        3=>[
            'titre'=>'Jquery',
            'prixJour'=>'590',
            'programme'=>'Généralités et définitions

            Théorie sur la programmation web (langages,rôles,applications)
            Particularités et forces de jQuery et JavaScript
            Règles de nommage et de syntaxe
        Structure du code jQuery
        
            Insertion de la bibliothèque (librairie) externe jQuery
            Inclusion dans du code HTML (balises et instructions)
            Constantes, variables et types de données en jQuery
            Présentation de jQuery UI et de ses apports
        Grandes règles du code jQuery
        
            Opérateurs et utilisation
            Structure de contrôle (boucles, instructions, conditions)
            Fonctions et appels
            Fonctions anonymes
            Traitement de chaînes et conversions
            Gestion des tableaux (fusion, traitement, séparation, clés...)
            Fonctions courantes et usuelles
            Fonctions mathématiques avec jQuery
            Traitement des dates et des chaînes de caractères
            Expressions régulières 
            Gestion des formulaires HTML
            Interactions et effets avec jQuery
            Gestion des événements et animations
            Sélecteurs jQuery et avantages
            Gestion des événements de souris, clavier, stylet...
            Boîtes de dialogue et interactions avec le client
            Création d’animations développées avec jQuery',
        
            'niveau'=>'Initiation',
            'secteur'=>[],
        
        ],
        4=>[
            'titre'=>'Node.js',
            'prixJour'=>'590',
            'programme'=>'   Introduction

            Présentation des versions de node 
            Principe d’un serveur Web
            Comparaison entre serveur
            Rappels de Javascript
            Atelier pratique : Télécharger et installer NodeJS

        Présentation de Node.js
        
            Gérer les versions de node (nvm vs n)
            L’interet de javascript côté serveur
            Tester des commandes avec REPL 
            La single-threaded event loop 
            L’API non bloquante
            Le moteur V8 de Google
            Atelier pratique : Exécuter les premiers scripts avec REPL

        Node.js et Ecmascript
        
            Historique d’Ecmascript
            Présentation du mode strict
            Support d’Ecmascript dans Node.js (node.green)
        
        Gestionnaire de paquets
        
            Présentation et description npm: node package manager
            Présentation de la bibliothèque de module
            Inclure des modules dans un projet: require 
            Installation locale ou globale
            Le fichier package.json
            Gérer les dépendances (production et developpement)
            npm et npx
            Ajouter des scripts
            Créer et publier un module
            Atelier pratique : Tester npm.
  
        Les variables globales
        
            La documentation de l’API de Node
            La classes Process: les events, les fluxs, etc
            Utiliser les sorties standards (stdin, stdout, stderr)
            Les classes Global, Console (log, errors, trace), Buffer, Streams
            Les variables __filenames et __dirnames
            Capter et émettre des évènements
            Les Timers 
            Manipuler les fichiers
            Lecture et écriture de fichier
            Gérer les chemins et les urls
            Création, suppression, renommer des fichiers et dossiers
            Lister le contenu d’un fichier
            Atelier pratique : manipuler les fichiers
        ',
            'niveau'=>[],
            'secteur'=>[],
        
        ],
        5=>[
            'titre'=>'ReactJs',
            'prixJour'=>'590',
            'programme'=>'  Présentation
                             Les objets littéraux
                             Ajouter & accéder aux propriétés
                             Les property shorthand',
            'niveau'=>[],
            'secteur'=>[],
        
        ],
        6=>[
            'titre'=>'CSS',
            'prixJour'=>'590',
            'programme'=>'  Présentation
                             Les objets littéraux
                             Ajouter & accéder aux propriétés
                             Les property shorthand',
            'niveau'=>[],
            'secteur'=>[],
        
        ],
        7=>[
            'titre'=>'HTML',
            'prixJour'=>'590',
            'programme'=>'  Présentation
                             Les objets littéraux
                             Ajouter & accéder aux propriétés
                             Les property shorthand',
            'niveau'=>[],
            'secteur'=>[],
        
        ],
        8=>[
            'titre'=>'Wireframing',
            'prixJour'=>'590',
            'programme'=>'  Présentation
                             Les objets littéraux
                             Ajouter & accéder aux propriétés
                             Les property shorthand',
            'niveau'=>[],
            'secteur'=>[],
        
        ],
        9=>[
            'titre'=>'ReactNative',
            'prixJour'=>'590',
            'programme'=>'  Présentation
                             Les objets littéraux
                             Ajouter & accéder aux propriétés
                             Les property shorthand',
            'niveau'=>[],
            'secteur'=>[],
        
        ],
        10=>[
            'titre'=>'Excel',
            'prixJour'=>'590',
            'programme'=>'  Présentation
                             Les objets littéraux
                             Ajouter & accéder aux propriétés
                             Les property shorthand',
            'niveau'=>[],
            'secteur'=>[],
        
        ],
        11=>[
            'titre'=>'Word',
            'prixJour'=>'590',
            'programme'=>'  Présentation
                             Les objets littéraux
                             Ajouter & accéder aux propriétés
                             Les property shorthand',
            'niveau'=>[],
            'secteur'=>[],
        
        ],
        12=>[
            'titre'=>'Toto',
            'prixJour'=>'590',
            'programme'=>'  Présentation
                             Les objets littéraux
                             Ajouter & accéder aux propriétés
                             Les property shorthand',
            'niveau'=>[],
            'secteur'=>[],
        
        ],
    ];

        foreach($formations as $key=>$value){
            $secteur = $this->getReference('secteur');
            $niveau = $this->getReference('niveau');


            $formation = new Formations();
            $formation -> setTitre($value['titre']);
            $formation -> setPrixJour($value['prixJour']);
            $formation -> setProgramme($value['programme']);
            $formation -> setNiveau($niveau);
            $formation -> setSecteur($secteur);
            
            $manager ->persist($formation);

            $this->setReference('formation',$formation );

        }

        $manager->flush();
    }
    public function getDependencies()
    {
        return [
            SecteurFixtures::class,
            NiveauFixtures::class,
        ];
    }
}
