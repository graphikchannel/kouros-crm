<?php

namespace App\DataFixtures;

use App\Entity\DaysOfWeek;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class DaysOfWeeksFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $days = [
            1 => [
                'name' => 'Dimanche',
                'number' => '0',
                
            ],
            2 => [
                'name' => 'Lundi',
                'number' =>'1' ,
            ],
            3 => [
                'name' => 'Mardi',
                'number' =>'2' ,
            ],
            4 => [
                'name' => 'Mercredi',
                'number' =>'3' ,
            ],
            5 => [
                'name' => 'Jeudi',
                'number' =>'4' ,
            ],
            6 => [
                'name' => 'Vendredi',
                'number' =>'5' ,
            ],
            7 => [
                'name' => 'Samedi',
                'number' =>'6' ,
            ]
        ];
        foreach($days as $key => $value){
            $day = new DaysOfWeek();
            $day->setName($value['name']);
            $day->setNumber($value['number']);

            $manager->persist($day);

            $this->setReference('day',$day );

        }
        $manager->flush();
    }
}
