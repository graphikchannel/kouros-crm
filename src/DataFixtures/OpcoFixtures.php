<?php

namespace App\DataFixtures;

use App\Entity\Opco;
use Doctrine\Persistence\ObjectManager;
use App\DataFixtures\EntrepriseFixtures;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class OpcoFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        
        $entreprise = $this->getReference('entreprise');

        
            $opco = new Opco();
            $opco->setEntreprise($entreprise);
            

            $manager->persist($opco);

            $this->setReference('opco',$opco);

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            EntrepriseFixtures::class,

        ];
    }
}
