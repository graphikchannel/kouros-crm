<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use App\Entity\Conventions;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use App\DataFixtures\ProspectsFixtures;
use App\DataFixtures\DevisFixtures;


class ConventionsFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {

        $conventions = [
            1 => [

                'lieuFormation' => 'Intra-toulon',
                'commentaire' => '',
                //'prospect' => $this->getReference('contact'),
                //'devis' => []

            ],
            




        ];

        foreach ($conventions as $key => $value) {
            $devis = $this->getReference('devis');
            $prospects = $this->getReference('prospect');

            $convention = new Conventions();
            $convention->setLieuFormation($value['lieuFormation']);
            $convention->setCommentaire($value['commentaire']);
            $convention->setProspect($prospects);
            $convention->setDevis($devis);



            //  $convention -> setProspect($this->getReference('prospet_'. $prospects));
            //  $convention -> setDevis($this->getReference('devis_'. $devis));

            // $convention->setProspect($this->getReference(ProspectsFixtures::PROSPECT));
            // $convention->setDevis($this->getReference(DevisFixtures::DEVIS));
            $manager->persist($convention);


            $this->setReference('conventions', $convention);
        }

        $manager->flush();
    }
    public function getDependencies()
    {
        return [
            ProspectsFixtures::class,
            DevisFixtures::class,
        ];
    }
}
