<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use App\Entity\Secteur;


class SecteurFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $secteurs = [
            1=>[
                'nom'=>'Architecture 3D',
            ],
            2=>[
                'nom'=>'Audiovisuel'
            ],
            3=>[
                'nom'=>'3D',
            ],
            4=>[
                'nom'=>'BIM',
            ],
            5=>[
                'nom'=>'Bureautique',
            ],
            6=>[
                'nom'=>'CAO-DAO',
            ],
            7=>[
                'nom'=>'Cartographie',
            ],
            8=>[
                'nom'=>'Commercial',
            ],
            9=>[
                'nom'=>'Communication',
            ],
            10=>[
                'nom'=>'Comptabilité',
            ],
            12=>[
                'nom'=>'Développement',
            ],
            14=>[
                'nom'=>'Infographie 3D',
            ],
            15=>[
                'nom'=>'Imprimerie',
            ],
            16=>[
                'nom'=>'Immobilier',
            ],
            17=>[
                'nom'=>'Langues',
            ],
            18=>[
                'nom'=>'Maîtrise d\'oeuvre',
            ],
            19=>[
                'nom'=>'Infomatique',
            ],
            20=>[
                'nom'=>'Web',
            ],
           
        ];

        foreach($secteurs as $key=>$value){
            $secteur = new Secteur();
            $secteur->setTitre($value['nom']);
            $manager->persist($secteur);
            $this->setReference('secteur',$secteur );

        }

        $manager->flush();
    }
}
