<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use App\Entity\Template;
// use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class TemplateFixtures extends Fixture //implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $templates = [
            1=>[
                'texte'=>'toto',
                'tag'=>'toto',
                'filename'=>'toto',

            ],
        ];

        foreach($templates as $key=>$value){
            $template = new Template();
            $template->setTexte($value['texte']);
            $template->setTag($value['tag']);
            $template->setFilename($value['filename']);

            $manager->persist($template);
            $this->addReference('template',$template );

        }

        $manager->flush();
    }
}
