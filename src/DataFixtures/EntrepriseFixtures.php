<?php

namespace App\DataFixtures;

use App\Entity\Entreprise;
use App\DataFixtures\OpcoFixtures;
use App\DataFixtures\ProspectsFixtures;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class EntrepriseFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $entreprises = [
            1 => [
                'denominationSociale' => 'Societe generale',
                'siret' => '12345678904567',
                'activite'=> 'prestations bancaires',
                'ape'=>'15687Y',
                'nbSalarie'=>'50'
                // 'commentaire'=> '',
            ],
            2 => [
                'denominationSociale' => 'ISRA',
                'siret' => '98765432104567',
                'activite'=> 'activité culturelle',
                'ape'=>'95624N',
                'nbSalarie'=>'25'
                // 'commentaire'=> '',
            ]
        ];
        foreach($entreprises as $key => $value){
            
            $prospect = $this->getReference('prospect');
            $opco = $this->getReference('opco');

            $entreprise = new Entreprise();
            $entreprise->setDenominationSociale($value['denominationSociale']);
            $entreprise->setApe($value['ape']);
            $entreprise->setSiret($value['siret']);
            $entreprise->setActivite($value['activite']);
            $entreprise->setNbsalarie($value['nbSalarie']);
            $entreprise->setYes($prospect);
            $entreprise->setOpco($opco);

            // $entreprise->setCommentaire($value['commentaire']);

            $manager->persist($entreprise);

            $this->setReference('entreprise',$entreprise );

        }
        $manager->flush();
    }
    public function getDependencies()
    {
        return [
            ProspectsFixtures::class,
            OpcoFixtures::class,
            
        ];
    }
}
