<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use App\Entity\Devis;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use App\DataFixtures\OpcoFixtures;
use App\DataFixtures\FormationsFixtures;


class DevisFixtures extends Fixture implements DependentFixtureInterface
{
    // public const DEVIS = 'devis_toto';

    public function load(ObjectManager $manager)
    {
        $devis = [
            1 => [
                'formations'=>'symfony',
                'nbrParticipants' =>'2',
                'tva' =>'10',
                'dateCreation'=>new \DateTime('04/05/2021'),
                'client'=>[],
                'dateDebut'=>new \DateTime('05/12/2021'),
                'dateFin'=>new \DateTime('10/01/2021'),
                'dureeH'=>'140',
                'methode'=>'',
                'fraisAnnexes'=>'200',
                'opco'=>[],
                'statut'=>'En cours',
               
            ],
            2 => [
                'formations'=>'symfony',
                'nbrParticipants' =>'2',
                'tva' =>'10',
                'dateCreation'=>new \DateTime(),
                'client'=>[],
                'dateDebut'=>new \DateTime('06/09/2021'),
                'dateFin'=>new \DateTime('12/09/2021'),
                'dureeH'=>'42',
                'methode'=>'',
                'fraisAnnexes'=>'200',
                'opco'=>[],
                'statut'=>'En cours',
               
            ],
            3 => [
                'formations'=>'symfony',
                'nbrParticipants' =>'2',
                'tva' =>'10',
                'dateCreation'=>new \DateTime('04/03/2021'),
                'client'=>[],
                'dateDebut'=>new \DateTime('04/10/2022'),
                'dateFin'=>new \DateTime('04/11/2022'),
                'dureeH'=>'125',
                'methode'=>'',
                'fraisAnnexes'=>'200',
                'opco'=>[],
                'statut'=>'En cours',
               
            ],
            4 => [
                'formations'=>'symfony',
                'nbrParticipants' =>'2',
                'tva' =>'10',
                'dateCreation'=>new \DateTime('04/03/2021'),
                'client'=>[],
                'dateDebut'=>new \DateTime('04/03/2021'),
                'dateFin'=>new \DateTime('04/03/2021'),
                'dureeH'=>'750',
                'methode'=>'',
                'fraisAnnexes'=>'200',
                'opco'=>[],
                'statut'=>'En cours',
               
            ],
            5 => [
                'formations'=>'symfony',
                'nbrParticipants' =>'2',
                'tva' =>'10',
                'dateCreation'=>new \DateTime('04/03/2021'),
                'client'=>[],
                'dateDebut'=>new \DateTime('04/03/2021'),
                'dateFin'=>new \DateTime('04/03/2021'),
                'dureeH'=>'750',
                'methode'=>'',
                'fraisAnnexes'=>'200',
                'opco'=>[],
                'statut'=>'En cours',
               
            ],
            6 => [
                'formations'=>'symfony',
                'nbrParticipants' =>'2',
                'tva' =>'10',
                'dateCreation'=>new \DateTime('04/03/2021'),
                'client'=>[],
                'dateDebut'=>new \DateTime('04/03/2021'),
                'dateFin'=>new \DateTime('04/03/2021'),
                'dureeH'=>'750',
                'methode'=>'',
                'fraisAnnexes'=>'200',
                'opco'=>[],
                'statut'=>'En cours',
               
            ],
            7 => [
                'formations'=>'symfony',
                'nbrParticipants' =>'2',
                'tva' =>'10',
                'dateCreation'=>new \DateTime('04/03/2021'),
                'client'=>[],
                'dateDebut'=>new \DateTime('04/03/2021'),
                'dateFin'=>new \DateTime('04/03/2021'),
                'dureeH'=>'750',
                'methode'=>'',
                'fraisAnnexes'=>'200',
                'opco'=>[],
                'statut'=>'En cours',
               
            ],
            8 => [
                'formations'=>'symfony',
                'nbrParticipants' =>'2',
                'tva' =>'10',
                'dateCreation'=>new \DateTime('04/03/2021'),
                'client'=>[],
                'dateDebut'=>new \DateTime('04/03/2021'),
                'dateFin'=>new \DateTime('04/03/2021'),
                'dureeH'=>'750',
                'methode'=>'',
                'fraisAnnexes'=>'200',
                'opco'=>[],
                'statut'=>'En cours',
               
            ],
            9 => [
                'formations'=>'symfony',
                'nbrParticipants' =>'2',
                'tva' =>'10',
                'dateCreation'=>new \DateTime('04/03/2021'),
                'client'=>[],
                'dateDebut'=>new \DateTime('04/03/2021'),
                'dateFin'=>new \DateTime('04/03/2021'),
                'dureeH'=>'750',
                'methode'=>'',
                'fraisAnnexes'=>'200',
                'opco'=>[],
                'statut'=>'En cours',
               
            ],
            10 => [
                'formations'=>'symfony',
                'nbrParticipants' =>'2',
                'tva' =>'10',
                'dateCreation'=>new \DateTime('04/03/2021'),
                'client'=>[],
                'dateDebut'=>new \DateTime('04/03/2021'),
                'dateFin'=>new \DateTime('04/03/2021'),
                'dureeH'=>'750',
                'methode'=>'',
                'fraisAnnexes'=>'200',
                'opco'=>[],
                'statut'=>'En cours',
               
            ],
            11 => [
                'formations'=>'symfony',
                'nbrParticipants' =>'2',
                'tva' =>'10',
                'dateCreation'=>new \DateTime('04/03/2021'),
                'client'=>[],
                'dateDebut'=>new \DateTime('04/03/2021'),
                'dateFin'=>new \DateTime('04/03/2021'),
                'dureeH'=>'750',
                'methode'=>'',
                'fraisAnnexes'=>'200',
                'opco'=>[],
                'statut'=>'En cours',
               
            ],
            12 => [
                'formations'=>'symfony',
                'nbrParticipants' =>'2',
                'tva' =>'10',
                'dateCreation'=>new \DateTime('04/03/2021'),
                'client'=>[],
                'dateDebut'=>new \DateTime('04/03/2021'),
                'dateFin'=>new \DateTime('04/03/2021'),
                'dureeH'=>'750',
                'methode'=>'',
                'fraisAnnexes'=>'200',
                'opco'=>[],
                'statut'=>'En cours',
               
            ],
            13 => [
                'formations'=>'symfony',
                'nbrParticipants' =>'2',
                'tva' =>'10',
                'dateCreation'=>new \DateTime('04/03/2021'),
                'client'=>[],
                'dateDebut'=>new \DateTime('04/03/2021'),
                'dateFin'=>new \DateTime('04/03/2021'),
                'dureeH'=>'750',
                'methode'=>'',
                'fraisAnnexes'=>'200',
                'opco'=>[],
                'statut'=>'En cours',
               
            ],
          
        ];
// dans les get et set onn trouve tarif mais il exsite pas comme propriété?????
        foreach($devis as $key=>$value){
            $formation = $this->getReference('formation');
            $opco = $this->getReference('opco');


            $devi = new Devis();
            // $devi -> setFormation($formation);
            $devi -> setNbrParticipants($value['nbrParticipants']);
            $devi -> setTva($value['tva']);
            $devi -> setDateCreation($value['dateCreation']);
            $devi -> setDateDebut($value['dateDebut']);
            $devi -> setDateFin($value['dateFin']);
            $devi -> setDureeH($value['dureeH']);
            $devi -> setMethode($value['methode']);
            $devi -> setFraisAnnexes($value['fraisAnnexes']);
            $devi -> setOpco($opco);
            $devi -> setFormations($formation);
            $devi -> setStatut($value['statut']);

            $manager->persist($devi);

            //Enregitrer le devis dans une référence
            // $this->addReference('devis_'. $key, $devi);
            // $this->addReference(self::DEVIS, $devi);
            
            $this->setReference('devis', $devi);
        }

        $manager->flush();
    }
    public function getDependencies()
    {
        return [
            FormationsFixtures::class,
            OpcoFixtures::class,
        ];
    }
}
