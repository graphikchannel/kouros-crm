<?php

namespace App\DataFixtures;

use App\Entity\Employes;
use App\DataFixtures\ContactsFixtures;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;



class EmployesFixtures extends Fixture implements DependentFixtureInterface
{
    // private $encoder;
    private $hasher; 

    


    // public function __construct(UserPasswordEncoderInterface $encoder){
    //     $this->encoder = $encoder;
    // }
    public function __construct(UserPasswordHasherInterface $hasher){
        $this->hasher = $hasher;
    }


    public function load(ObjectManager $manager )
    {
        $contact = $this->getReference('contact');

        $admin3=new Employes(); 
        // $password = $this->encoder->encodePassword($admin3,"mdp"); 
         //tde 12/08 encodage mdp avec hasher ⚠️encoder est déprécié
         $password= $this->hasher->hashPassword($admin3, 'mdp');

         $admin3->setContact($contact)
            ->setIdentifiant('FBastaraud')
            ->setRoles(["ROLE_ADMIN"]) 
            ->setPassword($password)
             ;

        $manager->persist($admin3);

        // $user=new Employes(); 
        // $password= $this->hasher->hashPassword($user, 'pass');
        // $user->setContact($contact)
        //      ->setIdentifiant('user')
        //      ->setRoles(["ROLE_USER"]) 
        //      ->setPassword($password)
        //      ;

        // $manager->persist($user);
        
        $manager->flush();
    }
    
    public function getDependencies()
    {
        return [
            ContactsFixtures::class,
        ];
    }
}
