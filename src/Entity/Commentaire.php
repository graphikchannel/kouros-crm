<?php

namespace App\Entity;

use App\Entity\User;
use App\Entity\Contacts;
use App\Entity\Prospects;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\CommentaireRepository;

/**
 * @ORM\Entity(repositoryClass=CommentaireRepository::class)
 */
class Commentaire
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text")
     */
    private $texte;

    /**
     * @ORM\ManyToOne(targetEntity=Devis::class, inversedBy="commentaires")
     * @ORM\JoinColumn(nullable=true)
     */
    private $dossier;

    /**
     * @ORM\OneToOne(targetEntity=User::class, mappedBy="commentaire", cascade={"persist", "remove"})
     */
    private $user;

    public function __toString() {
        return $this->texte;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTexte(): ?string
    {
        return $this->texte;
    }

    public function setTexte(string $texte): self
    {
        $this->texte = $texte;

        return $this;
    }

    public function getDossier(): ?Devis
    {
        return $this->dossier;
    }

    public function setDossier(?Devis $dossier): self
    {
        $this->dossier = $dossier;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        // unset the owning side of the relation if necessary
        if ($user === null && $this->user !== null) {
            $this->user->setCommentaire(null);
        }

        // set the owning side of the relation if necessary
        if ($user !== null && $user->getCommentaire() !== $this) {
            $user->setCommentaire($this);
        }
        $this->user = $user;

        return $this;
    }

}
