<?php

namespace App\Entity;

use App\Entity\Commentaire;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\ContactsRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass=ContactsRepository::class)
 */
class Contacts
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=80)
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=80)
     */
    private $prenom;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $tel;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $mobile;

    /**
     * @ORM\Column(type="string", length=180)
     */
    private $email;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $principal;

    /**
     * @ORM\OneToOne(targetEntity=Employes::class, mappedBy="contact", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $employes;

    /**
     * @ORM\OneToOne(targetEntity=Participants::class, mappedBy="contact", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $participants;

    

    /**
     * @ORM\ManyToOne(targetEntity=Prospects::class, inversedBy="contacts")
     * @ORM\JoinColumn(nullable=true)
     */
    private $prospects;

    /**
     * @ORM\ManyToOne(targetEntity=Opco::class, inversedBy="contacts")
     * @ORM\JoinColumn(nullable=true)
     */
    private $opco;

    /**
     * @ORM\ManyToOne(targetEntity=Payeur::class, inversedBy="contacts")
     * @ORM\JoinColumn(nullable=true)
     */
    private $payeur;

    /**
     * @ORM\OneToOne(targetEntity=Commentaire::class, cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $commentaire;

    /**
     * @ORM\OneToOne(targetEntity=Formateurs::class, mappedBy="contacts", cascade={"persist", "remove"})
     */
    private $formateurs;

    public function __construct()
    {
        $this->commentaires = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function setPrenom(string $prenom): self
    {
        $this->prenom = $prenom;

        return $this;
    }

    public function getTel(): ?string
    {
        return $this->tel;
    }

    public function setTel(string $tel): self
    {
        $this->tel = $tel;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPrincipal(): ?bool
    {
        return $this->principal;
    }

    public function setPrincipal(?bool $principal): self
    {
        $this->principal = $principal;

        return $this;
    }

    public function getMobile(): ?string
    {
        return $this->mobile;
    }

    public function setMobile(?string $mobile): self
    {
        $this->mobile = $mobile;

        return $this;
    }

    public function getEmployes(): ?Employes
    {
        return $this->employes;
    }

    public function setEmployes(Employes $employes): self
    {
        // set the owning side of the relation if necessary
        if ($employes->getContact() !== $this) {
            $employes->setContact($this);
        }

        $this->employes = $employes;

        return $this;
    }

    public function getParticipants(): ?Participants
    {
        return $this->participants;
    }

    public function setParticipants(Participants $participants): self
    {
        // set the owning side of the relation if necessary
        if ($participants->getContact() !== $this) {
            $participants->setContact($this);
        }

        $this->participants = $participants;

        return $this;
    }

    

    public function getProspects(): ?Prospects
    {
        return $this->prospects;
    }

    public function setProspects(?Prospects $prospects): self
    {
        $this->prospects = $prospects;

        return $this;
    }

    public function getOpco(): ?Opco
    {
        return $this->opco;
    }

    public function setOpco(?Opco $opco): self
    {
        $this->opco = $opco;

        return $this;
    }

    public function getPayeur(): ?Payeur
    {
        return $this->payeur;
    }

    public function setPayeur(?Payeur $payeur): self
    {
        $this->payeur = $payeur;

        return $this;
    }

    public function getCommentaire(): ?Commentaire
    {
        return $this->commentaire;
    }

    public function setCommentaire(?Commentaire $commentaire): self
    {
        $this->commentaire = $commentaire;

        return $this;
    }

    public function __toString()
    {
        return (string) $this->getNom();
    }

    public function getFormateurs(): ?Formateurs
    {
        return $this->formateurs;
    }

    public function setFormateurs(?Formateurs $formateurs): self
    {
        // unset the owning side of the relation if necessary
        if ($formateurs === null && $this->formateurs !== null) {
            $this->formateurs->setContacts(null);
        }

        // set the owning side of the relation if necessary
        if ($formateurs !== null && $formateurs->getContacts() !== $this) {
            $formateurs->setContacts($this);
        }

        $this->formateurs = $formateurs;

        return $this;
    }



}
