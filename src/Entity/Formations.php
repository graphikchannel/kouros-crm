<?php

namespace App\Entity;

use App\Entity\Niveau;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\FormationsRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass=FormationsRepository::class)
 */
class Formations
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $titre;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $programme;

    /**
     * @ORM\ManyToMany(targetEntity=Formateurs::class, mappedBy="formations")
     */
    private $formateurs;

    // /**
    //  * @ORM\ManyToMany(targetEntity=Devis::class, mappedBy="formations")
    //  */
    // private $devis;


    /**
     * @ORM\ManyToMany(targetEntity=Template::class, mappedBy="formations")
     */
    private $templates;

    /**
     * @ORM\ManyToOne(targetEntity=Secteur::class, inversedBy="formation")
     */
    private $secteur;

    /**
     * @ORM\OneToMany(targetEntity=Devis::class, mappedBy="formations")
     */
    private $devis;

    /**
     * @ORM\ManyToOne(targetEntity=Niveau::class, inversedBy="formations")
     */
    private $niveau;

    /**
     * @ORM\OneToMany(targetEntity=Tarifs::class, mappedBy="formations",cascade={"persist", "remove"})
     */
    private $tarifs;

    public function __construct()
    {
        $this->formateurs = new ArrayCollection();
        //$this->devis = new ArrayCollection();
        
        $this->templates = new ArrayCollection();
        $this->sessions = new ArrayCollection();
        $this->tarifs = new ArrayCollection();
        

    }

    public function __toString(){
        return $this->titre;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitre(): ?string
    {
        return $this->titre;
    }

    public function setTitre(string $titre): self
    {
        $this->titre = $titre;

        return $this;
    }

   

    public function getProgramme(): ?string
    {
        return $this->programme;
    }

    public function setProgramme($programme): self
    {
        $this->programme = $programme;

        return $this;
    }

    public function getNiveau(): ?Niveau
    {
        return $this->niveau;
    }

    public function setNiveau(?Niveau $niveau): self
    {
        $this->niveau = $niveau;

        return $this;
    }


    /**
     * @return Collection|Formateurs[]
     */
    public function getFormateurs(): Collection
    {
        return $this->formateurs;
    }

    public function addFormateur(Formateurs $formateur): self
    {
        if (!$this->formateurs->contains($formateur)) {
            $this->formateurs[] = $formateur;
            $formateur->addFormation($this);
        }

        return $this;
    }

    public function removeFormateur(Formateurs $formateur): self
    {
        if ($this->formateurs->removeElement($formateur)) {
            $formateur->removeFormation($this);
        }

        return $this;
    }

    // /**
    //  * @return Collection|Devis[]
    //  */
    // public function getDevis(): Collection
    // {
    //     return $this->devis;
    // }

    // public function addDevi(Devis $devi): self
    // {
    //     if (!$this->devis->contains($devi)) {
    //         $this->devis[] = $devi;
    //         $devi->addFormation($this);
    //     }

    //     return $this;
    // }

    // public function removeDevi(Devis $devi): self
    // {
    //     if ($this->devis->removeElement($devi)) {
    //         $devi->removeFormation($this);
    //     }

    //     return $this;
    // }

    

    /**
     * @return Collection|Template[]
     */
    public function getTemplates(): Collection
    {
        return $this->templates;
    }

    public function addTemplate(Template $template): self
    {
        if (!$this->templates->contains($template)) {
            $this->templates[] = $template;
            $template->addFormation($this);
        }

        return $this;
    }

    public function removeTemplate(Template $template): self
    {
        if ($this->templates->removeElement($template)) {
            $template->removeFormation($this);
        }

        return $this;
    }

    public function getSecteur(): ?Secteur
    {
        return $this->secteur;
    }

    public function setSecteur(?Secteur $secteur): self
    {
        $this->secteur = $secteur;

        return $this;
    }

    /**
     * @return Collection|Devis[]
     */
    public function getDevis(): Collection
    {
        return $this->devis;
    }

    public function addDevi(Devis $devi): self
    {
        if (!$this->devis->contains($devi)) {
            $this->devis[] = $devi;
            $devi->setFormations($this);
        }

        return $this;
    }

    public function removeDevi(Devis $devi): self
    {
        if ($this->devis->removeElement($devi)) {
            // set the owning side to null (unless already changed)
            if ($devi->getFormations() === $this) {
                $devi->setFormations(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Tarifs[]
     */
    public function getTarifs(): Collection
    {
        return $this->tarifs;
    }

    public function addTarif(Tarifs $tarif): self
    {
        if (!$this->tarifs->contains($tarif)) {
            $this->tarifs[] = $tarif;
            $tarif->setFormations($this);
        }

        return $this;
    }

    public function removeTarif(Tarifs $tarif): self
    {
        if ($this->tarifs->removeElement($tarif)) {
            // set the owning side to null (unless already changed)
            if ($tarif->getFormations() === $this) {
                $tarif->setFormations(null);
            }
        }

        return $this;
    }

}
