<?php

namespace App\Entity;

use App\Repository\EntrepriseRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=EntrepriseRepository::class)
 */
class Entreprise
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $denominationSociale;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $siret;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $ape;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $activite;

    /**
     * @ORM\OneToMany(targetEntity=Participants::class, mappedBy="entreprise")
     */
    private $participants;

    /**
     * @ORM\OneToOne(targetEntity=Prospects::class, mappedBy="entreprise", cascade={"persist", "remove"})
     */
    private $yes;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $tva;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $nbsalarie;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $site;

    /**
     * @ORM\OneToOne(targetEntity=Formateurs::class, mappedBy="entreprise", cascade={"persist", "remove"})
     */
    private $formateurs;

    /**
     * @ORM\OneToOne(targetEntity=Opco::class, mappedBy="entreprise", cascade={"persist", "remove"})
     */
    private $opco;

    /**
     * @ORM\OneToOne(targetEntity=Payeur::class, mappedBy="entreprise", cascade={"persist", "remove"})
     */
    private $payeur;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $numeroIntracommunautaireTva;

    //tde 01-09-21 test pour prob collection
    public function __construct()
    {
        $this->contacts = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDenominationSociale(): ?string
    {
        return $this->denominationSociale;
    }

    public function setDenominationSociale(string $denominationSociale): self
    {
        $this->denominationSociale = $denominationSociale;

        return $this;
    }

    public function getSiret(): ?int
    {
        return $this->siret;
    }

    public function setSiret(?int $siret): self
    {
        $this->siret = $siret;

        return $this;
    }

    public function getActivite(): ?string
    {
        return $this->activite;
    }

    public function setActivite(?string $activite): self
    {
        $this->activite = $activite;

        return $this;
    }

    public function __toString(){
        return $this->denominationSociale;
    }

    /**
     * @return Collection|Participants[]
     */
    public function getParticipants(): Collection
    {
        return $this->participants;
    }
    //tde 16-08-21 commenté car pas de relation coté participant avec entreprise
    // public function addParticipant(Participants $participant): self
    // {
    //     if (!$this->participants->contains($participant)) {
    //         $this->participants[] = $participant;
    //         $participant->setEntreprise($this);
    //     }

    //     return $this;
    // }

    // public function removeParticipant(Participants $participant): self
    // {
    //     if ($this->participants->removeElement($participant)) {
    //         // set the owning side to null (unless already changed)
    //         if ($participant->getEntreprise() === $this) {
    //             $participant->setEntreprise(null);
    //         }
    //     }

    //     return $this;
    // }

    /**
     * @return Collection|Contacts[]
     */
    public function getContacts(): Collection
    {
        return $this->contacts;
    }

    public function getYes(): ?Prospects
    {
        return $this->yes;
    }

    public function setYes(?Prospects $yes): self
    {
        // unset the owning side of the relation if necessary
        if ($yes === null && $this->yes !== null) {
            $this->yes->setEntreprise(null);
        }

        // set the owning side of the relation if necessary
        if ($yes !== null && $yes->getEntreprise() !== $this) {
            $yes->setEntreprise($this);
        }

        $this->yes = $yes;

        return $this;
    }

    public function getTva(): ?string
    {
        return $this->tva;
    }

    public function setTva(?string $tva): self
    {
        $this->tva = $tva;

        return $this;
    }

    public function getApe(): ?string
    {
        return $this->ape;
    }

    public function setApe(?string $ape): self
    {
        $this->ape = $ape;

        return $this;
    }

    public function getNbsalarie(): ?string
    {
        return $this->nbsalarie;
    }

    public function setNbsalarie(?string $nbsalarie): self
    {
        $this->nbsalarie = $nbsalarie;

        return $this;
    }

    public function getSite(): ?string
    {
        return $this->site;
    }

    public function setSite(?string $site): self
    {
        $this->site = $site;

        return $this;
    }

    public function getFormateurs(): ?Formateurs
    {
        return $this->formateurs;
    }

    public function setFormateurs(Formateurs $formateurs): self
    {
        // set the owning side of the relation if necessary
        if ($formateurs->getEntreprise() !== $this) {
            $formateurs->setEntreprise($this);
        }

        $this->formateurs = $formateurs;

        return $this;
    }

    public function getOpco(): ?Opco
    {
        return $this->opco;
    }

    public function setOpco(Opco $opco): self
    {
        // set the owning side of the relation if necessary
        if ($opco->getEntreprise() !== $this) {
            $opco->setEntreprise($this);
        }

        $this->opco = $opco;

        return $this;
    }

    public function getPayeur(): ?Payeur
    {
        return $this->payeur;
    }

    public function setPayeur(?Payeur $payeur): self
    {
        // unset the owning side of the relation if necessary
        if ($payeur === null && $this->payeur !== null) {
            $this->payeur->setEntreprise(null);
        }

        // set the owning side of the relation if necessary
        if ($payeur !== null && $payeur->getEntreprise() !== $this) {
            $payeur->setEntreprise($this);
        }

        $this->payeur = $payeur;

        return $this;
    }

    public function getNumeroIntracommunautaireTva(): ?string
    {
        return $this->numeroIntracommunautaireTva;
    }

    public function setNumeroIntracommunautaireTva(?string $numeroIntracommunautaireTva): self
    {
        $this->numeroIntracommunautaireTva = $numeroIntracommunautaireTva;

        return $this;
    }




}
