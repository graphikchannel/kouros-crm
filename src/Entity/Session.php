<?php

namespace App\Entity;

use App\Repository\SessionRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=SessionRepository::class)
 */
class Session
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $start;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $end;

    /**
     * @ORM\Column(type="time", nullable=true)
     */
    private $startTime;

    /**
     * @ORM\Column(type="time", nullable=true)
     */
    private $endTime;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $startRecur;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $endRecur;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $dureeJour;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $allDay;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $backgroundColor;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $textColor;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $borderColor;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $groupId;

    /**
     * @ORM\ManyToMany(targetEntity=DaysOfWeek::class, inversedBy="sessions", cascade={"persist", "remove"})
     */
    private $daysOfWeeks;

    /**
     * @ORM\ManyToOne(targetEntity=Formations::class, inversedBy="sessions")
     * 
     */
    private $formation;

    /**
     * @ORM\ManyToOne(targetEntity=Formateurs::class, inversedBy="sessions")
     */
    private $formateur;

    /**
     * @ORM\ManyToMany(targetEntity=Niveau::class, inversedBy="sessions")
     */
    private $niveau;

    public function __construct()
    {
        $this->daysOfWeeks = new ArrayCollection();
        $this->niveau = new ArrayCollection();
    }

     public function __toString()
    {
        return $this->title;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getStart(): ?\DateTimeInterface
    {
        return $this->start;
    }

    public function setStart(?\DateTimeInterface $start): self
    {
        $this->start = $start;

        return $this;
    }

    public function getEnd(): ?\DateTimeInterface
    {
        return $this->end;
    }

    public function setEnd(?\DateTimeInterface $end): self
    {
        $this->end = $end;

        return $this;
    }

    public function getStartTime(): ?\DateTimeInterface
    {
        return $this->startTime;
    }

    public function setStartTime(?\DateTimeInterface $startTime): self
    {
        $this->startTime = $startTime;

        return $this;
    }

    public function getEndTime(): ?\DateTimeInterface
    {
        return $this->endTime;
    }

    public function setEndTime(?\DateTimeInterface $endTime): self
    {
        $this->endTime = $endTime;

        return $this;
    }

    public function getStartRecur(): ?\DateTimeInterface
    {
        return $this->startRecur;
    }

    public function setStartRecur(?\DateTimeInterface $startRecur): self
    {
        $this->startRecur = $startRecur;

        return $this;
    }

    public function getEndRecur(): ?\DateTimeInterface
    {
        return $this->endRecur;
    }

    public function setEndRecur(?\DateTimeInterface $endRecur): self
    {
        $this->endRecur = $endRecur;

        return $this;
    }

    public function getDureeJour(): ?int
    {
        return $this->dureeJour;
    }

    public function setDureeJour(?int $dureeJour): self
    {
        $this->dureeJour = $dureeJour;

        return $this;
    }

    public function getAllDay(): ?bool
    {
        return $this->allDay;
    }



    public function setAllDay(?bool $allDay): self
    {
        $this->allDay = $allDay;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getBackgroundColor(): ?string
    {
        return $this->backgroundColor;
    }

    public function setBackgroundColor(?string $backgroundColor): self
    {
        $this->backgroundColor = $backgroundColor;

        return $this;
    }

    public function getTextColor(): ?string
    {
        return $this->textColor;
    }

    public function setTextColor(?string $textColor): self
    {
        $this->textColor = $textColor;

        return $this;
    }

    public function getBorderColor(): ?string
    {
        return $this->borderColor;
    }

    public function setBorderColor(?string $borderColor): self
    {
        $this->borderColor = $borderColor;

        return $this;
    }

    public function getCreatedAt(): ?\DateTime
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTime $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getGroupId(): ?string
    {
        return $this->groupId;
    }

    public function setGroupId(?string $groupId): self
    {
        $this->groupId = $groupId;

        return $this;
    }

    /**
     * @return Collection|DaysOfWeek[]
     */
    public function getDaysOfWeeks(): Collection
    {
        return $this->daysOfWeeks;
    }

    // ANCHOR - potentiel probleme par rapport au POC due au sens de la relation
    public function addDaysOfWeek(DaysOfWeek $daysOfWeek): self
    {
        if (!$this->daysOfWeeks->contains($daysOfWeek)) {
            $this->daysOfWeek[] = $daysOfWeek;
        }

        return $this;
    }

    public function removeDaysOfWeek(DaysOfWeek $daysOfWeek): self
    {
        $this->daysOfWeek->removeElement($daysOfWeek);

        return $this;
    }

    public function getFormation(): ?Formations
    {
        return $this->formation;
    }

    public function setFormation(?Formations $formation): self
    {
        $this->formation = $formation;

        return $this;
    }

    public function getFormateur(): ?Formateurs
    {
        return $this->formateur;
    }

    public function setFormateur(?Formateurs $formateur): self
    {
        $this->formateur = $formateur;

        return $this;
    }

    /**
     * @return Collection|Niveau[]
     */
    public function getNiveau(): Collection
    {
        return $this->niveau;
    }

    public function addNiveau(Niveau $niveau): self
    {
        if (!$this->niveau->contains($niveau)) {
            $this->niveau[] = $niveau;
        }

        return $this;
    }

    public function removeNiveau(Niveau $niveau): self
    {
        $this->niveau->removeElement($niveau);

        return $this;
    }
}
