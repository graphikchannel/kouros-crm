<?php

namespace App\Entity;

use App\Entity\Entreprise;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\OpcoRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass=OpcoRepository::class)
 */
class Opco
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity=Devis::class, mappedBy="opco")
     */
    private $devis;

    /**
     * @ORM\OneToOne(targetEntity=Entreprise::class, inversedBy="opco", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $entreprise;

    /**
     * @var ArrayCollection contacts
     * @ORM\OneToMany(targetEntity=Contacts::class, mappedBy="opco", cascade={"persist", "remove"})
     */
    private $contacts;
    

    /**
     * @ORM\OneToOne(targetEntity=Adresse::class, inversedBy="opco", cascade={"persist", "remove"})
     */
    private $adresse;

    public function __construct()
    {
        $this->devis = new ArrayCollection();
        $this->contacts = new ArrayCollection();
        
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection|Devis[]
     */
    public function getDevis(): Collection
    {
        return $this->devis;
    }

    public function addDevi(Devis $devi): self
    {
        if (!$this->devis->contains($devi)) {
            $this->devis[] = $devi;
            $devi->setOpco($this);
        }

        return $this;
    }

    public function removeDevi(Devis $devi): self
    {
        if ($this->devis->removeElement($devi)) {
            // set the owning side to null (unless already changed)
            if ($devi->getOpco() === $this) {
                $devi->setOpco(null);
            }
        }

        return $this;
    }

    

    public function __toString(){
       return $this->entreprise;
    }

    public function getEntreprise(): ?Entreprise
    {
        return $this->entreprise;
    }

    public function setEntreprise(Entreprise $entreprise): self
    {
        $this->entreprise = $entreprise;

        return $this;
    }

    /**
     * @return Collection|Contacts[]
     * 
     */
    public function getContacts(): Collection
    {
        
        return $this->contacts;
    }

    public function addContact(Contacts $contact): self
    {
        if (!$this->contacts->contains($contact)) {
            $this->contacts[] = $contact;
            $contact->setOpco($this);
        }

        return $this;
    }

    public function removeContact(Contacts $contact): self
    {
        if ($this->contacts->removeElement($contact)) {
            // set the owning side to null (unless already changed)
            if ($contact->getOpco() === $this) {
                $contact->setOpco(null);
            }
        }

        return $this;
    }

    public function getAdresse(): ?Adresse
    {
        return $this->adresse;
    }

    public function setAdresse(?Adresse $adresse): self
    {
        $this->adresse = $adresse;

        return $this;
    }
}
