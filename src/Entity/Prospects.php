<?php

namespace App\Entity;

use App\Repository\ProspectsRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ProspectsRepository::class)
 */
class Prospects extends User
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $statut;

    /**
     * @ORM\OneToOne(targetEntity=Entreprise::class, inversedBy="yes", cascade={"persist", "remove"})
     */
    private $entreprise;

    /**
     * @ORM\Column(type="boolean")
     */
    private $etat; //tde 17-08-21 prospect ou client

    /**
     * @ORM\OneToMany(targetEntity=Devis::class, mappedBy="client",cascade={"persist"})
     */
    private $devis;

    /**
     * @ORM\OneToMany(targetEntity=Conventions::class, mappedBy="prospect",cascade={"persist"})
     */
    private $conventions;

    // /**
    //  * @ORM\ManyToMany(targetEntity=Devis::class, mappedBy="nomContact")
    //  */
    // private $devisNom;

    /**
     * @ORM\ManyToOne(targetEntity=Opco::class)
     */
    private $opco;
    
    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $adherent;

    /**
     * @ORM\OneToMany(targetEntity=Contacts::class, mappedBy="prospects",cascade={"persist"})
     */
    private $contacts;

    /**
     * @ORM\OneToMany(targetEntity=Adresse::class, mappedBy="prospects",cascade={"persist"})
     */
    private $adresses;

    /**
     * @ORM\ManyToOne(targetEntity=Payeur::class, inversedBy="prescripteur")
     */
    private $payeur;



    public function __construct()
    {
        $this->devis = new ArrayCollection();
        $this->conventions = new ArrayCollection();
        $this->devisNom = new ArrayCollection();
        $this->contacts = new ArrayCollection();
        $this->adresses = new ArrayCollection();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    //tde 10/09/21 : génère automatiquement un identifiant
    public function MakeIdentifiant($contact = null){
        if(is_null ($this->getEntreprise()) ){
            //crée un identifant 1ere lettre prénom + nom
            $identifiant = substr($this->getContacts()[0]->getPrenom(),0,1) . $this->getContacts()[0]->getNom();
        }else{
            //crée un identifant = dénomination sociale
            $identifiant = $this->getEntreprise()->getDenominationSociale();
        }
        return $identifiant;
    }

    public function getStatut(): ?string
    {
        return $this->statut;
    }

    public function setStatut(string $statut): self
    {
        $this->statut = $statut;

        return $this;
    }
    
    public function __toString(){
        if($this->getEntreprise() === null ){
            //crée un identifant 1ere lettre prénom + nom
            $nom ='';
            //$nom = $this->getContacts()[0]->getPrenom() . " " . $this->getContacts()[0]->getNom();
        }else{
            //crée un identifant = dénomination sociale
            $nom = $this->getEntreprise()->getDenominationSociale();
        }
        return $nom;
    }

    public function getEtat(): ?bool
    {
        return $this->etat;
    }

    public function setEtat(bool $etat): self
    {
        $this->etat = $etat;

        return $this;
    }

    /**
     * @return Collection|Devis[]
     */
    public function getDevis(): Collection
    {
        return $this->devis;
    }

    public function addDevi(Devis $devi): self
    {
        if (!$this->devis->contains($devi)) {
            $this->devis[] = $devi;
            $devi->setClient($this);
        }
        return $this;
    }

    public function removeDevi(Devis $devi): self
    {
        if ($this->devis->removeElement($devi)) {
            // set the owning side to null (unless already changed)
            if ($devi->getClient() === $this) {
                $devi->setClient(null);
            }
        }
        return $this;
    }

    /**
     * @return Collection|Conventions[]
     */
    public function getConventions(): Collection
    {
        return $this->conventions;
    }

    public function addConvention(Conventions $convention): self
    {
        if (!$this->conventions->contains($convention)) {
            $this->conventions[] = $convention;
            $convention->setProspect($this);
        }
        return $this;
    }

    public function removeConvention(Conventions $convention): self
    {
        if ($this->conventions->removeElement($convention)) {
            // set the owning side to null (unless already changed)
            if ($convention->getProspect() === $this) {
                $convention->setProspect(null);
            }
        }
        return $this;
    }

    /**
     * @return Collection|Devis[]
     */
    public function getDevisNom(): Collection
    {
        return $this->devisNom;
    }

    public function addDevisNom(Devis $devisNom): self
    {
        if (!$this->devisNom->contains($devisNom)) {
            $this->devisNom[] = $devisNom;
            $devisNom->setNomContact($this);
        }

        return $this;
    }

    // public function removeDevisNom(Devis $devisNom): self
    // {
    //     if ($this->devisNom->removeElement($devisNom)) {
    //         $devisNom->removeNomContact($this);
    //     }

    //     return $this;
    // }

    public function getEntreprise(): ?Entreprise
    {
        return $this->entreprise;
    }

    public function setEntreprise(?Entreprise $entreprise): self
    {
        $this->entreprise = $entreprise;

        return $this;
    }

    public function getAdherent(): ?string
    {
        return $this->adherent;
    }

    public function setAdherent(?string $adherent): self
    {
        $this->adherent = $adherent;

        return $this;
    }

    public function getOpco(): ?Opco
    {
        return $this->opco;
    }

    public function setOpco(?Opco $opco): self
    {
        $this->opco = $opco;

        return $this;
    }

    /**
     * @return Collection|Contacts[]
     */
    public function getContacts(): Collection
    {
        return $this->contacts;
    }

    public function addContact(Contacts $contact): self
    {
        if (!$this->contacts->contains($contact)) {
            $this->contacts[] = $contact;
            $contact->setProspects($this);
        }

        return $this;
    }

    public function removeContact(Contacts $contact): self
    {
        if ($this->contacts->removeElement($contact)) {
            // set the owning side to null (unless already changed)
            if ($contact->getProspects() === $this) {
                $contact->setProspects(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Adresse[]
     */
    public function getAdresses(): Collection
    {
        return $this->adresses;
    }

    public function addAdress(Adresse $adress): self
    {
        if (!$this->adresses->contains($adress)) {
            $this->adresses[] = $adress;
            $adress->setProspects($this);
        }

        return $this;
    }

    public function removeAdress(Adresse $adress): self
    {
        if ($this->adresses->removeElement($adress)) {
            // set the owning side to null (unless already changed)
            if ($adress->getProspects() === $this) {
                $adress->setProspects(null);
            }
        }

        return $this;
    }

    public function getPayeur(): ?Payeur
    {
        return $this->payeur;
    }

    public function setPayeur(?Payeur $payeur): self
    {
        $this->payeur = $payeur;

        return $this;
    }
}
