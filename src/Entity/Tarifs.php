<?php

namespace App\Entity;

use App\Repository\TarifsRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=TarifsRepository::class)
 */
class Tarifs
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * 
     */
    private $tarifshoraire;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $tarifsjournalier;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $tarifsforfaitaire;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $horstaxe;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Assert\NotBlank()
     */
    private $ttc;

    /**
     * @ORM\ManyToOne(targetEntity=Formateurs::class, inversedBy="tarifs")
     */
    private $formateurs;

    /**
     * @ORM\ManyToOne(targetEntity=Formations::class, inversedBy="tarifs")
     */
    private $formations;

    /**
     * @ORM\OneToOne(targetEntity=Devis::class, mappedBy="tarifs", cascade={"persist", "remove"})
     */
    private $devis;



    public function __toString()
    {
        return $this->tarifshoraire;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTarifshoraire(): ?int
    {
        return $this->tarifshoraire;
    }

    public function setTarifshoraire(?int $tarifshoraire): self
    {
        $this->tarifshoraire = $tarifshoraire;

        return $this;
    }

    public function getTarifsjournalier(): ?int
    {
        return $this->tarifsjournalier;
    }

    public function setTarifsjournalier(?int $tarifsjournalier): self
    {
        $this->tarifsjournalier = $tarifsjournalier;

        return $this;
    }

    public function getTarifsforfaitaire(): ?int
    {
        return $this->tarifsforfaitaire;
    }

    public function setTarifsforfaitaire(?int $tarifsforfaitaire): self
    {
        $this->tarifsforfaitaire = $tarifsforfaitaire;

        return $this;
    }

    public function getHorstaxe(): ?int
    {
        return $this->horstaxe;
    }

    public function setHorstaxe(?int $horstaxe): self
    {
        $this->horstaxe = $horstaxe;

        return $this;
    }

    public function getTtc(): ?int
    {
        return $this->ttc;
    }

    public function setTtc(?int $ttc): self
    {
        $this->ttc = $ttc;

        return $this;
    }

    public function getFormateurs(): ?Formateurs
    {
        return $this->formateurs;
    }

    public function setFormateurs(?Formateurs $formateurs): self
    {
        $this->formateurs = $formateurs;

        return $this;
    }

    public function getFormations(): ?Formations
    {
        return $this->formations;
    }

    public function setFormations(?Formations $formations): self
    {
        $this->formations = $formations;

        return $this;
    }

    public function getDevis(): ?Devis
    {
        return $this->devis;
    }

    public function setDevis(?Devis $devis): self
    {
        // unset the owning side of the relation if necessary
        if ($devis === null && $this->devis !== null) {
            $this->devis->setTarifs(null);
        }

        // set the owning side of the relation if necessary
        if ($devis !== null && $devis->getTarifs() !== $this) {
            $devis->setTarifs($this);
        }

        $this->devis = $devis;

        return $this;
    }
    
    


}
