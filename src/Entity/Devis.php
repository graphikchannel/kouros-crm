<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\DevisRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints\DateTime;

/**
 * @ORM\Entity(repositoryClass=DevisRepository::class)
 */
class Devis
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    // /**
    //  * @ORM\ManyToMany(targetEntity=Formations::class, inversedBy="devis")
    //  */
    // private $formations;

    //tde 08-09-21 : le numéro de devis est créer par setDevis()
    //la nomenclature est la suivante :
    //  1ere lettre dénomination pour entreprise ou nom pour particulier
    //  4 chiffres années
    //  5 chiffres dont l'id (on ajoute des 0 devant l'id jusqu'à avoir 5 chiffres)
    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $numeroDevis;

    /**
     * @ORM\Column(type="integer")
     */
    private $nbrParticipants;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $tva;

    /**
     * @ORM\Column(type="datetime")
     */
    private $dateCreation;

    /**
     * @ORM\ManyToOne(targetEntity=Prospects::class, inversedBy="devis")
     */
    private $client;

    /**
     * @ORM\Column(type="datetime")
     */
    private $dateDebut;

    /**
     * @ORM\Column(type="datetime")
     */
    private $dateFin;

    /**
     * @ORM\Column(type="integer")
     */
    private $dureeH;

    // /**
    //  * @ORM\Column(type="integer")
    //  */
    // private $numeroDevis;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $methode;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $fraisAnnexes;

    /**
     * @ORM\ManyToOne(targetEntity=Opco::class, inversedBy="devis")
     */
    private $opco;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $statut;

    /**
     * @ORM\OneToOne(targetEntity=Conventions::class, mappedBy="devis", cascade={"persist", "remove"})
     */
    private $conventions;

    // ORM\ManyToMany(targetEntity=Prospects::class, inversedBy="devisNom")

    /**
     * @ORM\JoinColumn(nullable=false)
     */
    private $nomContact;

    /**
     * @ORM\ManyToOne(targetEntity=Formations::class, inversedBy="devis")
     */
    private $formations;

    /**
     * @ORM\ManyToOne(targetEntity=Niveau::class, inversedBy="devis")
     */
    private $niveau;

    /**
     * @ORM\ManyToOne(targetEntity=Payeur::class, inversedBy="devis")
     */
    private $payeur;

    /**
     * @ORM\OneToMany(targetEntity=Commentaire::class, mappedBy="dossier")
     */
    private $commentaires;

    /**
     * @ORM\OneToMany(targetEntity=FraisDivers::class, mappedBy="devis", cascade={"persist", "remove"})
     */
    private $fraisDivers;

    /**
     * @ORM\OneToOne(targetEntity=Tarifs::class, inversedBy="devis", cascade={"persist", "remove"})
     */
    private $tarifs;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $nbJours;

    public function __construct()
    {
        // $this->formations = new ArrayCollection();

        $this->nomContact = new ArrayCollection();
        $this->commentaires = new ArrayCollection();
        $this->fraisDivers = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    //tde 08-09-21 : le numéro de devis est créer par la méthode setDevis()
    //la nomenclature est la suivante :
    //  1ere lettre dénomination pour entreprise ou nom pour particulier
    //  4 chiffres années
    //  6 chiffres dont l'id (on ajoute des 0 devant l'id jusqu'à avoir 5 chiffres)
    //si c'est un particulier 1er lettre nom
    public function setNumeroDevis(): self
    {
            //1. 1ere lettre dénomination pour entreprise ou nom pour particulier en majuscule
            if($this->client->getStatut() == 'Particulier'){
                $entete = strtoupper(substr($this->client->getContacts()[0]->getNom(),0,1));
            }else{
                //si c'est une entreprise 1ere lettre denomination
                $entete = strtoupper(substr($this->client->getEntreprise()->getDenominationSociale(),0,1));
            }

            //2. 4 chiffres années
            $annee = date('Y');

            //3. crée nombre de 0 suivant taille chiffre id
            $nbZero = 6 - strlen($this->getId());
            $zero = '0';
            for($i = 1; $i < $nbZero; $i++ ){
                $zero .= '0';
            }

            $tiret = '-';

            //5. concatene l'ensemble
            $numeroDevis = $entete . $tiret . $annee . $tiret . $zero . $this->getId();

            $this->numeroDevis = $numeroDevis;
            return $this;
    }

    public function getNumeroDevis(): ?string
    {
        return $this->numeroDevis;
    }

    // /**
    //  * @return Collection|Formations[]
    //  */
    // public function getFormations(): Collection
    // {
    //     return $this->formations;
    // }

    // public function addFormation(Formations $formation): self
    // {
    //     if (!$this->formations->contains($formation)) {
    //         $this->formations[] = $formation;
    //     }

    //     return $this;
    // }

    // public function removeFormation(Formations $formation): self
    // {
    //     $this->formations->removeElement($formation);

    //     return $this;
    // }

    public function getNbrParticipants(): ?int
    {
        return $this->nbrParticipants;
    }

    public function setNbrParticipants(?int $nbrParticipants): self
    {
        $this->nbrParticipants = $nbrParticipants;

        return $this;
    }

    // la propriété tarif n'existe pas ????  Lydia=>12/08/2021

    // public function getTarif(): ?float
    // {
    //     return $this->tarif;
    // }

    // public function setTarif(float $tarif): self
    // {
    //     $this->tarif = $tarif;

    //     return $this;
    // }

    public function getTva(): ?float
    {
        return $this->tva;
    }

    public function setTva(?float $tva): self
    {
        $this->tva = $tva;

        return $this;
    }

    public function getDateCreation(): ?\DateTimeInterface
    {
        return $this->dateCreation;
    }

    public function setDateCreation(?\DateTimeInterface $dateCreation): self
    {
        $this->dateCreation = $dateCreation;

        return $this;
    }

    public function getClient(): ?Prospects
    {
        return $this->client;
    }

    public function setClient(?Prospects $client): self
    {
        $this->client = $client;

        return $this;
    }

    public function getDateDebut(): ?\DateTimeInterface
    {
        return $this->dateDebut;
    }

    public function setDateDebut(?\DateTimeInterface $dateDebut): self
    {
        $this->dateDebut = $dateDebut;

        return $this;
    }

    public function getDateFin(): ?\DateTimeInterface
    {
        return $this->dateFin;
    }

    public function setDateFin(?\DateTimeInterface $dateFin): self
    {
        $this->dateFin = $dateFin;

        return $this;
    }

    public function getDureeH(): ?int
    {
        return $this->dureeH;
    }

    public function setDureeH(?int $dureeH): self
    {
        $this->dureeH = $dureeH;

        return $this;
    }

    public function getMethode(): ?string
    {
        return $this->methode;
    }

    public function setMethode(?string $methode): self
    {
        $this->methode = $methode;

        return $this;
    }

    public function getFraisAnnexes(): ?float
    {
        return $this->fraisAnnexes;
    }

    public function setFraisAnnexes(?float $fraisAnnexes): self
    {
        $this->fraisAnnexes = $fraisAnnexes;

        return $this;
    }

    public function getOpco(): ?Opco
    {
        return $this->opco;
    }

    public function setOpco(?Opco $opco): self
    {
        $this->opco = $opco;

        return $this;
    }

    public function getStatut(): ?string
    {
        return $this->statut;
    }

    public function setStatut(?string $statut): self
    {
        $this->statut = $statut;

        return $this;
    }
    public function __toString(){
        return $this->id;
    }

    public function getConventions(): ?Conventions
    {
        return $this->conventions;
    }

    public function setConventions(Conventions $conventions): self
    {
        // set the owning side of the relation if necessary
        if ($conventions->getDevis() !== $this) {
            $conventions->setDevis($this);
        }

        $this->conventions = $conventions;

        return $this;
    }


    // /**
    //  * @return Collection|Formations[]
    //  */
    // public function getTitreFormation(): Collection
    // {
    //     return $this->formations;
    // }

    /**
     * @return Collection|Prospects[]
     */
    public function getNomContact(): ?Collection
    {
        return $this->nomContact;
    }
    public function setNomContact (?Prospects $nomContact): self
    {
        $this->nomContact = $nomContact;

        return $this;
    }
    // public function addNomContact(Prospects $nomContact): self
    // {
    //     if (!$this->nomContact->contains($nomContact)) {
    //         $this->nomContact[] = $nomContact;
    //     }

    //     return $this;
    // }

    // public function removeNomContact(Prospects $nomContact): self
    // {
    //     $this->nomContact->removeElement($nomContact);

    //     return $this;
    // }

    // public function getFormation(): ?Formations
    // {
    //     return $this->formation;
    // }

    // public function setFormation(?Formations $formation): self
    // {
    //     $this->formation = $formation;

    //     return $this;
    // }


    /**
     * Get the value of formations
     */ 
    public function getFormations()
    {
        return $this->formations;
    }

    /**
     * Set the value of formation
     *
     * @return  self
     */ 
    public function setFormations($formations)
    {
        $this->formations = $formations;

        return $this;
    }

    public function getNiveau(): ?Niveau
    {
        return $this->niveau;
    }

    public function setNiveau(?Niveau $niveau): self
    {
        $this->niveau = $niveau;

        return $this;
    }

    public function getPayeur(): ?Payeur
    {
        return $this->payeur;
    }

    public function setPayeur(?Payeur $payeur): self
    {
        $this->payeur = $payeur;

        return $this;
    }

    /**
     * @return Collection|Commentaire[]
     */
    public function getCommentaires(): Collection
    {
        return $this->commentaires;
    }

    public function addCommentaire(Commentaire $commentaire): self
    {
        if (!$this->commentaires->contains($commentaire)) {
            $this->commentaires[] = $commentaire;
            $commentaire->setDossier($this);
        }

        return $this;
    }

    public function removeCommentaire(Commentaire $commentaire): self
    {
        if ($this->commentaires->removeElement($commentaire)) {
            // set the owning side to null (unless already changed)
            if ($commentaire->getDossier() === $this) {
                $commentaire->setDossier(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|FraisDivers[]
     */
    public function getFraisDivers(): Collection
    {
        return $this->fraisDivers;
    }

    public function addFraisDiver(FraisDivers $fraisDiver): self
    {
        if (!$this->fraisDivers->contains($fraisDiver)) {
            $this->fraisDivers[] = $fraisDiver;
            $fraisDiver->setDevis($this);
        }

        return $this;
    }

    public function removeFraisDiver(FraisDivers $fraisDiver): self
    {
        if ($this->fraisDivers->removeElement($fraisDiver)) {
            // set the owning side to null (unless already changed)
            if ($fraisDiver->getDevis() === $this) {
                $fraisDiver->setDevis(null);
            }
        }

        return $this;
    }

    public function getTarifs(): ?Tarifs
    {
        return $this->tarifs;
    }

    public function setTarifs(?Tarifs $tarifs): self
    {
        $this->tarifs = $tarifs;

        return $this;
    }

    public function getNbJours(): ?int
    {
        return $this->nbJours;
    }

    public function setNbJours(?int $nbJours): self
    {
        $this->nbJours = $nbJours;

        return $this;
    }
}
