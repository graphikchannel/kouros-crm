<?php

namespace App\Entity;

use App\Repository\ParticipantsRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ParticipantsRepository::class)
 */
class Participants extends User
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\OneToOne(targetEntity=Contacts::class, inversedBy="participants", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $contact;

    /**
     * @ORM\ManyToMany(targetEntity=Devis::class, mappedBy="participants")
     */
    private $devis;

    /**
     * @ORM\ManyToMany(targetEntity=Conventions::class, mappedBy="participants", cascade={"persist", "remove"})
     */
    private $conventions;

    
    public function __construct()
    {
        $this->devis = new ArrayCollection();
        $this->conventions = new ArrayCollection();
        
    }
    
    public function getId(): ?int
    {
        return $this->id;
    }

    public function MakeIdentifiant($contact = null){
        //dd($this->getContact()->getPrenom());
        //crée un identifant 1ere lettre prénom + nom
        $identifiant = substr($this->getContact()->getPrenom(),0,1) . $this->getContact()->getNom();
        return $identifiant;

    }

    /**
     * @return Collection|Devis[]
     */
    public function getDevis(): Collection
    {
        return $this->devis;
    }


    // public function addDevi(Devis $devi): self
    // {
    //     if (!$this->devis->contains($devi)) {
    //         $this->devis[] = $devi;
    //         $devi->addParticipant($this);
    //     }

    //     return $this;
    // }

    // public function removeDevi(Devis $devi): self
    // {
    //     if ($this->devis->removeElement($devi)) {
    //         $devi->removeParticipant($this);
    //     }

    //     return $this;
    // }

    /**
     * @return Collection|Conventions[]
     */
    public function getConventions(): Collection
    {
        return $this->conventions;
    }

    public function addConvention(Conventions $convention): self
    {
        if (!$this->conventions->contains($convention)) {
            $this->conventions[] = $convention;
            $convention->addParticipant($this);
        }

        return $this;
    }

    public function removeConvention(Conventions $convention): self
    {
        if ($this->conventions->removeElement($convention)) {
            $convention->removeParticipant($this);
        }

        return $this;
    }

    public function getContact(): ?Contacts
    {
        return $this->contact;
    }

    public function setContact(Contacts $contact): self
    {
        $this->contact = $contact;

        return $this;
    }
    
}
