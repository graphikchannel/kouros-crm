<?php

namespace App\Entity;

use App\Repository\FormateursRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;



/**
 * @ORM\Entity(repositoryClass=FormateursRepository::class)
 */
class Formateurs extends User
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $methode;

    /**
     * @ORM\ManyToMany(targetEntity=Formations::class, inversedBy="formateurs")
     */
    private $formations;

    /**
     * @ORM\OneToMany(targetEntity=Session::class, mappedBy="formateur")
     */
    private $sessions;

    
    /**
     * @ORM\OneToOne(targetEntity=Entreprise::class, inversedBy="formateurs", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $entreprise;

    /**
     * @ORM\OneToMany(targetEntity=Adresse::class, mappedBy="formateurs")
     */
    private $adresses;

    /**
     * @ORM\OneToOne(targetEntity=Contacts::class, inversedBy="formateurs", cascade={"persist", "remove"})
     */
    private $contacts;

    /**
     * @ORM\OneToMany(targetEntity=Tarifs::class, mappedBy="formateurs", cascade={"persist", "remove"})
     */
    private $tarifs;

    

    


    public function __construct()
    {
        $this->formations = new ArrayCollection();
        $this->sessions = new ArrayCollection();
        $this->adresses = new ArrayCollection();
        $this->tarifs = new ArrayCollection();
        
        
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function MakeIdentifiant(){
        if($this->getEntreprise() == null){
            //crée un identifant 1ere lettre prénom + nom
            $identifiant = substr($this->getContact->getPrenom(),0,1) . $this->getContacts()[0]->getNom();
        }else{
            //crée un identifant = dénomination sociale
            $identifiant = $this->getEntreprise()->getDenominationSociale();
        }
        return $identifiant;
    }

    public function getMethode(): ?string
    {
        return $this->methode;
    }

    public function setMethode(?string $methode): self
    {
        $this->methode = $methode;

        return $this;
    }

    /**
     * @return Collection|Formations[]
     */
    public function getFormations(): Collection
    {
        return $this->formations;
    }

    public function addFormation(Formations $formation): self
    {
        if (!$this->formations->contains($formation)) {
            $this->formations[] = $formation;
        }

        return $this;
    }

    public function removeFormation(Formations $formation): self
    {
        $this->formations->removeElement($formation);

        return $this;
    }
    public function __toString(){
        return $this->entreprise;
    }

    /**
     * @return Collection|Session[]
     */
    public function getSessions(): Collection
    {
        return $this->sessions;
    }

    public function addSession(Session $session): self
    {
        if (!$this->sessions->contains($session)) {
            $this->sessions[] = $session;
            $session->setFormateur($this);
        }

        return $this;
    }

    public function removeSession(Session $session): self
    {
        if ($this->sessions->removeElement($session)) {
            // set the owning side to null (unless already changed)
            if ($session->getFormateur() === $this) {
                $session->setFormateur(null);
            }
        }

        return $this;
    }

    
    public function getEntreprise(): ?Entreprise
    {
        return $this->entreprise;
    }

    public function setEntreprise(Entreprise $entreprise): self
    {
        $this->entreprise = $entreprise;

        return $this;
    }

    /**
     * @return Collection|Adresse[]
     */
    public function getAdresses(): Collection
    {
        return $this->adresses;
    }

    public function addAdress(Adresse $adress): self
    {
        if (!$this->adresses->contains($adress)) {
            $this->adresses[] = $adress;
            $adress->setFormateurs($this);
        }

        return $this;
    }

    public function removeAdress(Adresse $adress): self
    {
        if ($this->adresses->removeElement($adress)) {
            // set the owning side to null (unless already changed)
            if ($adress->getFormateurs() === $this) {
                $adress->setFormateurs(null);
            }
        }

        return $this;
    }

    public function getContacts(): ?Contacts
    {
        return $this->contacts;
    }

    public function setContacts(?Contacts $contacts): self
    {
        $this->contacts = $contacts;

        return $this;
    }

    /**
     * @return Collection|Tarifs[]
     */
    public function getTarifs(): Collection
    {
        return $this->tarifs;
    }

    public function addTarif(Tarifs $tarif): self
    {
        if (!$this->tarifs->contains($tarif)) {
            $this->tarifs[] = $tarif;
            $tarif->setFormateurs($this);
        }

        return $this;
    }

    public function removeTarif(Tarifs $tarif): self
    {
        if ($this->tarifs->removeElement($tarif)) {
            // set the owning side to null (unless already changed)
            if ($tarif->getFormateurs() === $this) {
                $tarif->setFormateurs(null);
            }
        }

        return $this;
    }

    

    

}
