<?php

namespace App\Entity;

use App\Entity\Contacts;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\EmployesRepository;

/**
 * @ORM\Entity(repositoryClass=EmployesRepository::class)
 */
class Employes extends User
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\OneToOne(targetEntity=Contacts::class, inversedBy="employes", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $contact;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function MakeIdentifiant(){
        //crée un identifant 1ere lettre prénom + nom
        $identifiant = substr($this->getContact()->getPrenom(),0,1) . $this->getContact()->getNom();
        return $identifiant;;
    }

    public function getContact(): ?Contacts
    {
        return $this->contact;
    }

    public function setContact(Contacts $contact): self
    {
        $this->contact = $contact;

        return $this;
    }

}
