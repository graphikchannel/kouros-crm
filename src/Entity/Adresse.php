<?php

namespace App\Entity;

use App\Repository\AdresseRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=AdresseRepository::class)
 */
class Adresse
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $numero;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $voie;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $ville;

    /**
     * @ORM\Column(type="string", length=5, nullable=true)
     */
    private $cp;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $region;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $complementaire;

    /**
     * @ORM\ManyToOne(targetEntity=Formateurs::class, inversedBy="adresses")
     */
    private $formateurs;

    /**
     * @ORM\ManyToOne(targetEntity=Prospects::class, inversedBy="adresses")
     */
    private $prospects;

    /**
     * @ORM\OneToOne(targetEntity=Opco::class, mappedBy="adresse", cascade={"persist", "remove"})
     */
    private $opco;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $principale;

    /**
     * @ORM\OneToOne(targetEntity=Payeur::class, mappedBy="adresse", cascade={"persist", "remove"})
     */
    private $payeur;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $country;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNumero(): ?string
    {
        return $this->numero;
    }

    public function setNumero(?string $numero): self
    {
        $this->numero = $numero;

        return $this;
    }

    public function getVoie(): ?string
    {
        return $this->voie;
    }

    public function setVoie(?string $voie): self
    {
        $this->voie = $voie;

        return $this;
    }

    public function getVille(): ?string
    {
        return $this->ville;
    }

    public function setVille(?string $ville): self
    {
        $this->ville = $ville;

        return $this;
    }

    public function getCp(): ?string
    {
        return $this->cp;
    }

    public function setCp(?string $cp): self
    {
        $this->cp = $cp;

        return $this;
    }

    public function getRegion(): ?string
    {
        return $this->region;
    }

    public function setRegion(?string $region): self
    {
        $this->region = $region;

        return $this;
    }

    public function getComplementaire(): ?string
    {
        return $this->complementaire;
    }

    public function setComplementaire(?string $complementaire): self
    {
        $this->complementaire = $complementaire;

        return $this;
    }

    public function getFormateurs(): ?Formateurs
    {
        return $this->formateurs;
    }

    public function setFormateurs(?Formateurs $formateurs): self
    {
        $this->formateurs = $formateurs;

        return $this;
    }

    public function getProspects(): ?Prospects
    {
        return $this->prospects;
    }

    public function setProspects(?Prospects $prospects): self
    {
        $this->prospects = $prospects;

        return $this;
    }

    public function getOpco(): ?Opco
    {
        return $this->opco;
    }

    public function setOpco(?Opco $opco): self
    {
        // unset the owning side of the relation if necessary
        if ($opco === null && $this->opco !== null) {
            $this->opco->setAdresse(null);
        }

        // set the owning side of the relation if necessary
        if ($opco !== null && $opco->getAdresse() !== $this) {
            $opco->setAdresse($this);
        }

        $this->opco = $opco;

        return $this;
    }

    public function getPrincipale(): ?bool
    {
        return $this->principale;
    }

    public function setPrincipale(?bool $principale): self
    {
        $this->principale = $principale;

        return $this;
    }

    public function getPayeur(): ?Payeur
    {
        return $this->payeur;
    }

    public function setPayeur(?Payeur $payeur): self
    {
        // unset the owning side of the relation if necessary
        if ($payeur === null && $this->payeur !== null) {
            $this->payeur->setAdresse(null);
        }

        // set the owning side of the relation if necessary
        if ($payeur !== null && $payeur->getAdresse() !== $this) {
            $payeur->setAdresse($this);
        }

        $this->payeur = $payeur;

        return $this;
    }

     public function __toString()
    {
        return 
        
                (string) $this->getVille();
             ;
    }

     public function getCountry(): ?string
     {
         return $this->country;
     }

     public function setCountry(?string $country): self
     {
         $this->country = $country;

         return $this;
     }
}
