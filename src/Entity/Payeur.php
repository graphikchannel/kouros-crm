<?php

namespace App\Entity;

use App\Repository\PayeurRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PayeurRepository::class)
 */
class Payeur
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity=Entreprise::class, inversedBy="payeur", cascade={"persist", "remove"})
     */
    private $entreprise;

    /**
     * @ORM\OneToMany(targetEntity=Contacts::class, mappedBy="payeur")
     */
    private $contacts;

    /**
     * @ORM\OneToOne(targetEntity=Adresse::class, inversedBy="payeur", cascade={"persist", "remove"})
     */
    private $adresse;

    /**
     * @ORM\OneToMany(targetEntity=Prospects::class, mappedBy="payeur")
     */
    private $prescripteur;

    /**
     * @ORM\OneToMany(targetEntity=Devis::class, mappedBy="payeur")
     */
    private $devis;

    public function __construct()
    {
        $this->contacts = new ArrayCollection();
        $this->prescripteur = new ArrayCollection();
        $this->devis = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEntreprise(): ?Entreprise
    {
        return $this->entreprise;
    }

    public function setEntreprise(?Entreprise $entreprise): self
    {
        $this->entreprise = $entreprise;

        return $this;
    }

    /**
     * @return Collection|Contacts[]
     */
    public function getContacts(): Collection
    {
        return $this->contacts;
    }

    public function addContact(Contacts $contact): self
    {
        if (!$this->contacts->contains($contact)) {
            $this->contacts[] = $contact;
            $contact->setPayeur($this);
        }

        return $this;
    }

    public function removeContact(Contacts $contact): self
    {
        if ($this->contacts->removeElement($contact)) {
            // set the owning side to null (unless already changed)
            if ($contact->getPayeur() === $this) {
                $contact->setPayeur(null);
            }
        }

        return $this;
    }

    public function getAdresse(): ?Adresse
    {
        return $this->adresse;
    }

    public function setAdresse(?Adresse $adresse): self
    {
        $this->adresse = $adresse;

        return $this;
    }

    /**
     * @return Collection|Prospects[]
     */
    public function getPrescripteur(): Collection
    {
        return $this->prescripteur;
    }

    public function addPrescripteur(Prospects $prescripteur): self
    {
        if (!$this->prescripteur->contains($prescripteur)) {
            $this->prescripteur[] = $prescripteur;
            $prescripteur->setPayeur($this);
        }

        return $this;
    }

    public function removePrescripteur(Prospects $prescripteur): self
    {
        if ($this->prescripteur->removeElement($prescripteur)) {
            // set the owning side to null (unless already changed)
            if ($prescripteur->getPayeur() === $this) {
                $prescripteur->setPayeur(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Devis[]
     */
    public function getDevis(): Collection
    {
        return $this->devis;
    }

    public function addDevi(Devis $devi): self
    {
        if (!$this->devis->contains($devi)) {
            $this->devis[] = $devi;
            $devi->setPayeur($this);
        }

        return $this;
    }

    public function removeDevi(Devis $devi): self
    {
        if ($this->devis->removeElement($devi)) {
            // set the owning side to null (unless already changed)
            if ($devi->getPayeur() === $this) {
                $devi->setPayeur(null);
            }
        }

        return $this;
    }
}
