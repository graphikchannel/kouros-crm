<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211229134859 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE adresse (id INT AUTO_INCREMENT NOT NULL, formateurs_id INT DEFAULT NULL, prospects_id INT DEFAULT NULL, numero VARCHAR(10) DEFAULT NULL, voie VARCHAR(255) DEFAULT NULL, ville VARCHAR(100) DEFAULT NULL, cp VARCHAR(5) DEFAULT NULL, region VARCHAR(100) DEFAULT NULL, complementaire VARCHAR(255) DEFAULT NULL, principale TINYINT(1) DEFAULT NULL, country VARCHAR(255) DEFAULT NULL, INDEX IDX_C35F0816FB0881C8 (formateurs_id), INDEX IDX_C35F0816775D63D (prospects_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE commentaire (id INT AUTO_INCREMENT NOT NULL, dossier_id INT DEFAULT NULL, texte LONGTEXT NOT NULL, INDEX IDX_67F068BC611C0C56 (dossier_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE contacts (id INT AUTO_INCREMENT NOT NULL, employes_id INT DEFAULT NULL, participants_id INT DEFAULT NULL, prospects_id INT DEFAULT NULL, opco_id INT DEFAULT NULL, payeur_id INT DEFAULT NULL, commentaire_id INT DEFAULT NULL, nom VARCHAR(80) NOT NULL, prenom VARCHAR(80) NOT NULL, tel VARCHAR(10) DEFAULT NULL, mobile VARCHAR(20) DEFAULT NULL, email VARCHAR(180) NOT NULL, principal TINYINT(1) DEFAULT NULL, UNIQUE INDEX UNIQ_33401573F971F91F (employes_id), UNIQUE INDEX UNIQ_33401573838709D5 (participants_id), INDEX IDX_33401573775D63D (prospects_id), INDEX IDX_33401573CF671859 (opco_id), INDEX IDX_33401573422667C5 (payeur_id), UNIQUE INDEX UNIQ_33401573BA9CD190 (commentaire_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE conventions (id INT AUTO_INCREMENT NOT NULL, prospect_id INT NOT NULL, devis_id INT NOT NULL, lieu_formation VARCHAR(100) DEFAULT NULL, commentaire LONGTEXT DEFAULT NULL, INDEX IDX_AC36C555D182060A (prospect_id), UNIQUE INDEX UNIQ_AC36C55541DEFADA (devis_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE conventions_participants (conventions_id INT NOT NULL, participants_id INT NOT NULL, INDEX IDX_842B525975019F55 (conventions_id), INDEX IDX_842B5259838709D5 (participants_id), PRIMARY KEY(conventions_id, participants_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE days_of_week (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, number INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE devis (id INT AUTO_INCREMENT NOT NULL, client_id INT DEFAULT NULL, opco_id INT DEFAULT NULL, formations_id INT DEFAULT NULL, niveau_id INT DEFAULT NULL, payeur_id INT DEFAULT NULL, tarifs_id INT DEFAULT NULL, numero_devis VARCHAR(20) DEFAULT NULL, nbr_participants INT NOT NULL, tva DOUBLE PRECISION DEFAULT NULL, date_creation DATETIME NOT NULL, date_debut DATETIME NOT NULL, date_fin DATETIME NOT NULL, duree_h INT NOT NULL, methode VARCHAR(255) NOT NULL, frais_annexes DOUBLE PRECISION DEFAULT NULL, statut VARCHAR(255) NOT NULL, nb_jours INT DEFAULT NULL, INDEX IDX_8B27C52B19EB6921 (client_id), INDEX IDX_8B27C52BCF671859 (opco_id), INDEX IDX_8B27C52B3BF5B0C2 (formations_id), INDEX IDX_8B27C52BB3E9C81 (niveau_id), INDEX IDX_8B27C52B422667C5 (payeur_id), UNIQUE INDEX UNIQ_8B27C52BF5F3287F (tarifs_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE employes (id INT NOT NULL, contact_id INT NOT NULL, UNIQUE INDEX UNIQ_A94BC0F0E7A1254A (contact_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE entreprise (id INT AUTO_INCREMENT NOT NULL, denomination_sociale VARCHAR(100) NOT NULL, siret BIGINT DEFAULT NULL, ape VARCHAR(50) DEFAULT NULL, activite VARCHAR(255) DEFAULT NULL, tva VARCHAR(255) DEFAULT NULL, nbsalarie VARCHAR(255) DEFAULT NULL, site VARCHAR(100) DEFAULT NULL, numero_intracommunautaire_tva VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE formateurs (id INT NOT NULL, entreprise_id INT NOT NULL, contacts_id INT DEFAULT NULL, methode VARCHAR(255) DEFAULT NULL, UNIQUE INDEX UNIQ_FD80E574A4AEAFEA (entreprise_id), UNIQUE INDEX UNIQ_FD80E574719FB48E (contacts_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE formateurs_formations (formateurs_id INT NOT NULL, formations_id INT NOT NULL, INDEX IDX_BED037DFFB0881C8 (formateurs_id), INDEX IDX_BED037DF3BF5B0C2 (formations_id), PRIMARY KEY(formateurs_id, formations_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE formations (id INT AUTO_INCREMENT NOT NULL, secteur_id INT DEFAULT NULL, niveau_id INT DEFAULT NULL, titre VARCHAR(50) NOT NULL, programme LONGTEXT DEFAULT NULL, INDEX IDX_409021379F7E4405 (secteur_id), INDEX IDX_40902137B3E9C81 (niveau_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE frais_divers (id INT AUTO_INCREMENT NOT NULL, devis_id INT DEFAULT NULL, nom VARCHAR(255) DEFAULT NULL, montant INT DEFAULT NULL, INDEX IDX_F059262041DEFADA (devis_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE niveau (id INT AUTO_INCREMENT NOT NULL, nom VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE opco (id INT AUTO_INCREMENT NOT NULL, entreprise_id INT NOT NULL, adresse_id INT DEFAULT NULL, UNIQUE INDEX UNIQ_852D14B4A4AEAFEA (entreprise_id), UNIQUE INDEX UNIQ_852D14B44DE7DC5C (adresse_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE participants (id INT NOT NULL, contact_id INT NOT NULL, UNIQUE INDEX UNIQ_71697092E7A1254A (contact_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE payeur (id INT AUTO_INCREMENT NOT NULL, entreprise_id INT DEFAULT NULL, adresse_id INT DEFAULT NULL, UNIQUE INDEX UNIQ_AADA89E1A4AEAFEA (entreprise_id), UNIQUE INDEX UNIQ_AADA89E14DE7DC5C (adresse_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE prospects (id INT NOT NULL, entreprise_id INT DEFAULT NULL, opco_id INT DEFAULT NULL, payeur_id INT DEFAULT NULL, statut VARCHAR(50) NOT NULL, etat TINYINT(1) NOT NULL, adherent VARCHAR(50) DEFAULT NULL, UNIQUE INDEX UNIQ_35730C06A4AEAFEA (entreprise_id), INDEX IDX_35730C06CF671859 (opco_id), INDEX IDX_35730C06422667C5 (payeur_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE secteur (id INT AUTO_INCREMENT NOT NULL, titre VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE session (id INT AUTO_INCREMENT NOT NULL, formation_id INT DEFAULT NULL, formateur_id INT DEFAULT NULL, title VARCHAR(255) NOT NULL, start DATETIME DEFAULT NULL, end DATETIME DEFAULT NULL, start_time TIME DEFAULT NULL, end_time TIME DEFAULT NULL, start_recur DATE DEFAULT NULL, end_recur DATETIME DEFAULT NULL, duree_jour INT DEFAULT NULL, all_day TINYINT(1) DEFAULT NULL, description VARCHAR(255) DEFAULT NULL, background_color VARCHAR(255) DEFAULT NULL, text_color VARCHAR(255) DEFAULT NULL, border_color VARCHAR(255) DEFAULT NULL, created_at DATETIME DEFAULT NULL, group_id VARCHAR(255) DEFAULT NULL, INDEX IDX_D044D5D45200282E (formation_id), INDEX IDX_D044D5D4155D8F51 (formateur_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE session_days_of_week (session_id INT NOT NULL, days_of_week_id INT NOT NULL, INDEX IDX_233CEBC4613FECDF (session_id), INDEX IDX_233CEBC45D20E35A (days_of_week_id), PRIMARY KEY(session_id, days_of_week_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE session_niveau (session_id INT NOT NULL, niveau_id INT NOT NULL, INDEX IDX_24B4F932613FECDF (session_id), INDEX IDX_24B4F932B3E9C81 (niveau_id), PRIMARY KEY(session_id, niveau_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tarifs (id INT AUTO_INCREMENT NOT NULL, formateurs_id INT DEFAULT NULL, formations_id INT DEFAULT NULL, tarifshoraire INT DEFAULT NULL, tarifsjournalier INT DEFAULT NULL, tarifsforfaitaire INT DEFAULT NULL, horstaxe INT DEFAULT NULL, ttc INT DEFAULT NULL, INDEX IDX_F9B8C496FB0881C8 (formateurs_id), INDEX IDX_F9B8C4963BF5B0C2 (formations_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE template (id INT AUTO_INCREMENT NOT NULL, texte LONGTEXT DEFAULT NULL, tag VARCHAR(255) DEFAULT NULL, filename VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE template_formations (template_id INT NOT NULL, formations_id INT NOT NULL, INDEX IDX_625B64F85DA0FB8 (template_id), INDEX IDX_625B64F83BF5B0C2 (formations_id), PRIMARY KEY(template_id, formations_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, commentaire_id INT DEFAULT NULL, identifiant VARCHAR(80) NOT NULL, roles LONGTEXT NOT NULL COMMENT \'(DC2Type:json)\', password VARCHAR(255) DEFAULT NULL, image VARCHAR(255) DEFAULT NULL, fullname VARCHAR(255) DEFAULT NULL, reset_token VARCHAR(50) DEFAULT NULL, type VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_8D93D649C90409EC (identifiant), UNIQUE INDEX UNIQ_8D93D649BA9CD190 (commentaire_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE adresse ADD CONSTRAINT FK_C35F0816FB0881C8 FOREIGN KEY (formateurs_id) REFERENCES formateurs (id)');
        $this->addSql('ALTER TABLE adresse ADD CONSTRAINT FK_C35F0816775D63D FOREIGN KEY (prospects_id) REFERENCES prospects (id)');
        $this->addSql('ALTER TABLE commentaire ADD CONSTRAINT FK_67F068BC611C0C56 FOREIGN KEY (dossier_id) REFERENCES devis (id)');
        $this->addSql('ALTER TABLE contacts ADD CONSTRAINT FK_33401573F971F91F FOREIGN KEY (employes_id) REFERENCES employes (id)');
        $this->addSql('ALTER TABLE contacts ADD CONSTRAINT FK_33401573838709D5 FOREIGN KEY (participants_id) REFERENCES participants (id)');
        $this->addSql('ALTER TABLE contacts ADD CONSTRAINT FK_33401573775D63D FOREIGN KEY (prospects_id) REFERENCES prospects (id)');
        $this->addSql('ALTER TABLE contacts ADD CONSTRAINT FK_33401573CF671859 FOREIGN KEY (opco_id) REFERENCES opco (id)');
        $this->addSql('ALTER TABLE contacts ADD CONSTRAINT FK_33401573422667C5 FOREIGN KEY (payeur_id) REFERENCES payeur (id)');
        $this->addSql('ALTER TABLE contacts ADD CONSTRAINT FK_33401573BA9CD190 FOREIGN KEY (commentaire_id) REFERENCES commentaire (id)');
        $this->addSql('ALTER TABLE conventions ADD CONSTRAINT FK_AC36C555D182060A FOREIGN KEY (prospect_id) REFERENCES prospects (id)');
        $this->addSql('ALTER TABLE conventions ADD CONSTRAINT FK_AC36C55541DEFADA FOREIGN KEY (devis_id) REFERENCES devis (id)');
        $this->addSql('ALTER TABLE conventions_participants ADD CONSTRAINT FK_842B525975019F55 FOREIGN KEY (conventions_id) REFERENCES conventions (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE conventions_participants ADD CONSTRAINT FK_842B5259838709D5 FOREIGN KEY (participants_id) REFERENCES participants (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE devis ADD CONSTRAINT FK_8B27C52B19EB6921 FOREIGN KEY (client_id) REFERENCES prospects (id)');
        $this->addSql('ALTER TABLE devis ADD CONSTRAINT FK_8B27C52BCF671859 FOREIGN KEY (opco_id) REFERENCES opco (id)');
        $this->addSql('ALTER TABLE devis ADD CONSTRAINT FK_8B27C52B3BF5B0C2 FOREIGN KEY (formations_id) REFERENCES formations (id)');
        $this->addSql('ALTER TABLE devis ADD CONSTRAINT FK_8B27C52BB3E9C81 FOREIGN KEY (niveau_id) REFERENCES niveau (id)');
        $this->addSql('ALTER TABLE devis ADD CONSTRAINT FK_8B27C52B422667C5 FOREIGN KEY (payeur_id) REFERENCES payeur (id)');
        $this->addSql('ALTER TABLE devis ADD CONSTRAINT FK_8B27C52BF5F3287F FOREIGN KEY (tarifs_id) REFERENCES tarifs (id)');
        $this->addSql('ALTER TABLE employes ADD CONSTRAINT FK_A94BC0F0E7A1254A FOREIGN KEY (contact_id) REFERENCES contacts (id)');
        $this->addSql('ALTER TABLE employes ADD CONSTRAINT FK_A94BC0F0BF396750 FOREIGN KEY (id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE formateurs ADD CONSTRAINT FK_FD80E574A4AEAFEA FOREIGN KEY (entreprise_id) REFERENCES entreprise (id)');
        $this->addSql('ALTER TABLE formateurs ADD CONSTRAINT FK_FD80E574719FB48E FOREIGN KEY (contacts_id) REFERENCES contacts (id)');
        $this->addSql('ALTER TABLE formateurs ADD CONSTRAINT FK_FD80E574BF396750 FOREIGN KEY (id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE formateurs_formations ADD CONSTRAINT FK_BED037DFFB0881C8 FOREIGN KEY (formateurs_id) REFERENCES formateurs (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE formateurs_formations ADD CONSTRAINT FK_BED037DF3BF5B0C2 FOREIGN KEY (formations_id) REFERENCES formations (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE formations ADD CONSTRAINT FK_409021379F7E4405 FOREIGN KEY (secteur_id) REFERENCES secteur (id)');
        $this->addSql('ALTER TABLE formations ADD CONSTRAINT FK_40902137B3E9C81 FOREIGN KEY (niveau_id) REFERENCES niveau (id)');
        $this->addSql('ALTER TABLE frais_divers ADD CONSTRAINT FK_F059262041DEFADA FOREIGN KEY (devis_id) REFERENCES devis (id)');
        $this->addSql('ALTER TABLE opco ADD CONSTRAINT FK_852D14B4A4AEAFEA FOREIGN KEY (entreprise_id) REFERENCES entreprise (id)');
        $this->addSql('ALTER TABLE opco ADD CONSTRAINT FK_852D14B44DE7DC5C FOREIGN KEY (adresse_id) REFERENCES adresse (id)');
        $this->addSql('ALTER TABLE participants ADD CONSTRAINT FK_71697092E7A1254A FOREIGN KEY (contact_id) REFERENCES contacts (id)');
        $this->addSql('ALTER TABLE participants ADD CONSTRAINT FK_71697092BF396750 FOREIGN KEY (id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE payeur ADD CONSTRAINT FK_AADA89E1A4AEAFEA FOREIGN KEY (entreprise_id) REFERENCES entreprise (id)');
        $this->addSql('ALTER TABLE payeur ADD CONSTRAINT FK_AADA89E14DE7DC5C FOREIGN KEY (adresse_id) REFERENCES adresse (id)');
        $this->addSql('ALTER TABLE prospects ADD CONSTRAINT FK_35730C06A4AEAFEA FOREIGN KEY (entreprise_id) REFERENCES entreprise (id)');
        $this->addSql('ALTER TABLE prospects ADD CONSTRAINT FK_35730C06CF671859 FOREIGN KEY (opco_id) REFERENCES opco (id)');
        $this->addSql('ALTER TABLE prospects ADD CONSTRAINT FK_35730C06422667C5 FOREIGN KEY (payeur_id) REFERENCES payeur (id)');
        $this->addSql('ALTER TABLE prospects ADD CONSTRAINT FK_35730C06BF396750 FOREIGN KEY (id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE session ADD CONSTRAINT FK_D044D5D45200282E FOREIGN KEY (formation_id) REFERENCES formations (id)');
        $this->addSql('ALTER TABLE session ADD CONSTRAINT FK_D044D5D4155D8F51 FOREIGN KEY (formateur_id) REFERENCES formateurs (id)');
        $this->addSql('ALTER TABLE session_days_of_week ADD CONSTRAINT FK_233CEBC4613FECDF FOREIGN KEY (session_id) REFERENCES session (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE session_days_of_week ADD CONSTRAINT FK_233CEBC45D20E35A FOREIGN KEY (days_of_week_id) REFERENCES days_of_week (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE session_niveau ADD CONSTRAINT FK_24B4F932613FECDF FOREIGN KEY (session_id) REFERENCES session (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE session_niveau ADD CONSTRAINT FK_24B4F932B3E9C81 FOREIGN KEY (niveau_id) REFERENCES niveau (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE tarifs ADD CONSTRAINT FK_F9B8C496FB0881C8 FOREIGN KEY (formateurs_id) REFERENCES formateurs (id)');
        $this->addSql('ALTER TABLE tarifs ADD CONSTRAINT FK_F9B8C4963BF5B0C2 FOREIGN KEY (formations_id) REFERENCES formations (id)');
        $this->addSql('ALTER TABLE template_formations ADD CONSTRAINT FK_625B64F85DA0FB8 FOREIGN KEY (template_id) REFERENCES template (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE template_formations ADD CONSTRAINT FK_625B64F83BF5B0C2 FOREIGN KEY (formations_id) REFERENCES formations (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user ADD CONSTRAINT FK_8D93D649BA9CD190 FOREIGN KEY (commentaire_id) REFERENCES commentaire (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE opco DROP FOREIGN KEY FK_852D14B44DE7DC5C');
        $this->addSql('ALTER TABLE payeur DROP FOREIGN KEY FK_AADA89E14DE7DC5C');
        $this->addSql('ALTER TABLE contacts DROP FOREIGN KEY FK_33401573BA9CD190');
        $this->addSql('ALTER TABLE user DROP FOREIGN KEY FK_8D93D649BA9CD190');
        $this->addSql('ALTER TABLE employes DROP FOREIGN KEY FK_A94BC0F0E7A1254A');
        $this->addSql('ALTER TABLE formateurs DROP FOREIGN KEY FK_FD80E574719FB48E');
        $this->addSql('ALTER TABLE participants DROP FOREIGN KEY FK_71697092E7A1254A');
        $this->addSql('ALTER TABLE conventions_participants DROP FOREIGN KEY FK_842B525975019F55');
        $this->addSql('ALTER TABLE session_days_of_week DROP FOREIGN KEY FK_233CEBC45D20E35A');
        $this->addSql('ALTER TABLE commentaire DROP FOREIGN KEY FK_67F068BC611C0C56');
        $this->addSql('ALTER TABLE conventions DROP FOREIGN KEY FK_AC36C55541DEFADA');
        $this->addSql('ALTER TABLE frais_divers DROP FOREIGN KEY FK_F059262041DEFADA');
        $this->addSql('ALTER TABLE contacts DROP FOREIGN KEY FK_33401573F971F91F');
        $this->addSql('ALTER TABLE formateurs DROP FOREIGN KEY FK_FD80E574A4AEAFEA');
        $this->addSql('ALTER TABLE opco DROP FOREIGN KEY FK_852D14B4A4AEAFEA');
        $this->addSql('ALTER TABLE payeur DROP FOREIGN KEY FK_AADA89E1A4AEAFEA');
        $this->addSql('ALTER TABLE prospects DROP FOREIGN KEY FK_35730C06A4AEAFEA');
        $this->addSql('ALTER TABLE adresse DROP FOREIGN KEY FK_C35F0816FB0881C8');
        $this->addSql('ALTER TABLE formateurs_formations DROP FOREIGN KEY FK_BED037DFFB0881C8');
        $this->addSql('ALTER TABLE session DROP FOREIGN KEY FK_D044D5D4155D8F51');
        $this->addSql('ALTER TABLE tarifs DROP FOREIGN KEY FK_F9B8C496FB0881C8');
        $this->addSql('ALTER TABLE devis DROP FOREIGN KEY FK_8B27C52B3BF5B0C2');
        $this->addSql('ALTER TABLE formateurs_formations DROP FOREIGN KEY FK_BED037DF3BF5B0C2');
        $this->addSql('ALTER TABLE session DROP FOREIGN KEY FK_D044D5D45200282E');
        $this->addSql('ALTER TABLE tarifs DROP FOREIGN KEY FK_F9B8C4963BF5B0C2');
        $this->addSql('ALTER TABLE template_formations DROP FOREIGN KEY FK_625B64F83BF5B0C2');
        $this->addSql('ALTER TABLE devis DROP FOREIGN KEY FK_8B27C52BB3E9C81');
        $this->addSql('ALTER TABLE formations DROP FOREIGN KEY FK_40902137B3E9C81');
        $this->addSql('ALTER TABLE session_niveau DROP FOREIGN KEY FK_24B4F932B3E9C81');
        $this->addSql('ALTER TABLE contacts DROP FOREIGN KEY FK_33401573CF671859');
        $this->addSql('ALTER TABLE devis DROP FOREIGN KEY FK_8B27C52BCF671859');
        $this->addSql('ALTER TABLE prospects DROP FOREIGN KEY FK_35730C06CF671859');
        $this->addSql('ALTER TABLE contacts DROP FOREIGN KEY FK_33401573838709D5');
        $this->addSql('ALTER TABLE conventions_participants DROP FOREIGN KEY FK_842B5259838709D5');
        $this->addSql('ALTER TABLE contacts DROP FOREIGN KEY FK_33401573422667C5');
        $this->addSql('ALTER TABLE devis DROP FOREIGN KEY FK_8B27C52B422667C5');
        $this->addSql('ALTER TABLE prospects DROP FOREIGN KEY FK_35730C06422667C5');
        $this->addSql('ALTER TABLE adresse DROP FOREIGN KEY FK_C35F0816775D63D');
        $this->addSql('ALTER TABLE contacts DROP FOREIGN KEY FK_33401573775D63D');
        $this->addSql('ALTER TABLE conventions DROP FOREIGN KEY FK_AC36C555D182060A');
        $this->addSql('ALTER TABLE devis DROP FOREIGN KEY FK_8B27C52B19EB6921');
        $this->addSql('ALTER TABLE formations DROP FOREIGN KEY FK_409021379F7E4405');
        $this->addSql('ALTER TABLE session_days_of_week DROP FOREIGN KEY FK_233CEBC4613FECDF');
        $this->addSql('ALTER TABLE session_niveau DROP FOREIGN KEY FK_24B4F932613FECDF');
        $this->addSql('ALTER TABLE devis DROP FOREIGN KEY FK_8B27C52BF5F3287F');
        $this->addSql('ALTER TABLE template_formations DROP FOREIGN KEY FK_625B64F85DA0FB8');
        $this->addSql('ALTER TABLE employes DROP FOREIGN KEY FK_A94BC0F0BF396750');
        $this->addSql('ALTER TABLE formateurs DROP FOREIGN KEY FK_FD80E574BF396750');
        $this->addSql('ALTER TABLE participants DROP FOREIGN KEY FK_71697092BF396750');
        $this->addSql('ALTER TABLE prospects DROP FOREIGN KEY FK_35730C06BF396750');
        $this->addSql('DROP TABLE adresse');
        $this->addSql('DROP TABLE commentaire');
        $this->addSql('DROP TABLE contacts');
        $this->addSql('DROP TABLE conventions');
        $this->addSql('DROP TABLE conventions_participants');
        $this->addSql('DROP TABLE days_of_week');
        $this->addSql('DROP TABLE devis');
        $this->addSql('DROP TABLE employes');
        $this->addSql('DROP TABLE entreprise');
        $this->addSql('DROP TABLE formateurs');
        $this->addSql('DROP TABLE formateurs_formations');
        $this->addSql('DROP TABLE formations');
        $this->addSql('DROP TABLE frais_divers');
        $this->addSql('DROP TABLE niveau');
        $this->addSql('DROP TABLE opco');
        $this->addSql('DROP TABLE participants');
        $this->addSql('DROP TABLE payeur');
        $this->addSql('DROP TABLE prospects');
        $this->addSql('DROP TABLE secteur');
        $this->addSql('DROP TABLE session');
        $this->addSql('DROP TABLE session_days_of_week');
        $this->addSql('DROP TABLE session_niveau');
        $this->addSql('DROP TABLE tarifs');
        $this->addSql('DROP TABLE template');
        $this->addSql('DROP TABLE template_formations');
        $this->addSql('DROP TABLE user');
    }
}
