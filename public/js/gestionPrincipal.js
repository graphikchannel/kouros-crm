    //maj tous les contacts et adresses en cas de modification du principal
    //mets les autres en secondaire
    const majPrincipal = (name)=>{
        $(`input[name*="${name}"][name*="[principal"]`).on('change',(e)=>{
            //quel élément est coché
            const elementPrincipal = $(e.target).attr('id');
            const elemementName = $(e.target).attr('name');
            //si la valeur est principal
            if($(e.target).val() == 1 ){ 
                //je boucle sur la collection html d'éléments radio
                
                $(`input[name*="${name}"][name*="[principal"]`).each((index, element)=>{ 
                    //si leur id n'est pas le même que l'élément séléctionné
                    //si leur id comprend le name recherché (pour éviter de séléctionner adresse avec contact)
                    //si leur valeur est de 0
                    //alors je les rends 'checked'
                    if($(element).attr('id').search(name) != -1 && $(element).attr('id') != elementPrincipal && $(element).val() == 0 && $(element).attr('name') != elemementName){
                        $(element).prop('checked', true);
                    }
                })
            }
        });
    }