//tde 26/08/21 : recherche entreprise par nom - recup siren /siret 
// source : https://github.com/etalab/sirene_as_api

const apiEntreprise = (inputCible, nom, inputDenom, inputSiret, inputApe, inputActivite, inputAdresse, inputCp, inputVille, inputRegion)=>{
    //efface la liste pour éviter les cumul au rechargement
        //$("#siret").remove();

    //requête ajax à l'api si au moins 3 lettres
    if(nom.length < 2) {
        //fait réapparaitre la loupe et disparaitre le compteur
        $(inputCible).next().children('i').removeClass('d-none');
        $(inputCible).next().children('div').remove();
        //redonne sa couleur normal au label
        $(inputCible).next().removeClass('border-danger text-danger');
        $(inputCible).next().addClass('border-secondary text-primary');
        //supprime la liste de résultats
        $(inputCible).siblings('#listeRecherche').remove()

    } else{
        //supprime l'ancienne liste de résultats
        $(inputCible).siblings('#listeRecherche').remove()

        //précise la requête
        // let numDepartement = (departement == null) ?'' :`&departement=${departement}`;
        // let numPage = (departement == null) ? 1 : page;
        let requete = `https://entreprise.data.gouv.fr/api/sirene/v1/full_text/${nom}?page=1&per_page=50`

        //requête api
        $.ajax({
            url : requete,

        }).done(
            (rep)=>{

                //LABEL - NB RESULTAT
                //changement style label suivant nb résultat
                let nbRep = rep.total_results;
                    if(nbRep >= 50) {
                        $(inputCible).next().removeClass('border-secondary text-primary');
                        $(inputCible).next().addClass('border-danger text-danger');
                    } else{
                        $(inputCible).next().removeClass('border-danger text-danger');
                        $(inputCible).next().addClass('border-secondary text-primary');
                    }

                    //supprime loupe et la remet
                    if($(inputCible).val().length > 0 ){
                        $(inputCible).next().children('i').addClass('d-none');
                    }

                    //insère le nombre de résultat et supprime les doublons
                    $(inputCible).next().children('div').remove();
                    $(inputCible).next().append('<div>' + nbRep + '<div>');

                
                //GESTION RESULTATS
                //insère le ul
                $(inputCible).siblings('label').after('<ul class="card list-group" id="listeRecherche" ></ul>')

                //insère les li dans le ul
                let i = 0;
                rep.etablissement.map(()=>{
                    choix = rep.etablissement[i];

                    //infos entreprise
                    let denomination = choix.nom_raison_sociale ;

                    //infos localisation
                    let ville = choix.libelle_commune ;
                    let dept = choix.departement ;

                    //LISTE DE RESULTATS
                    //crée chaque réponse
                    let liste = '<li class="p-2" ><a class="list-group-item-action" id="entr-' + i + '" >' + denomination + ' (' + ville + ' - ' + dept + ')</a></li>'
                    $('#listeRecherche').append(liste);

                    //passe d'une entreprise à une autre
                    i++;
                })//en rep.etablissement.map(
                    
                    //après execution array map() on lance la détection de click sur la liste
                    //détection click 
                    $('#listeRecherche li a').click((e)=>{
                        //je récup l'id qui correspond à la valeur de i dans les résultats
                        id = $(e.target).attr('id').substr(5);
                        // je récup les données correspondantes
                        let selection = rep.etablissement[id]

                        //insère les données dans les inputs dédiés
                        let denomination = selection.nom_raison_sociale ;
                        $(inputDenom).val(denomination);
                        let siret = selection.siret ;
                        $(inputSiret).val(siret);
                        let ape = selection.activite_principale_entreprise ; 
                        $(inputApe).val(ape);
                        let activite = selection.libelle_activite_principale_entreprise ;
                        $(inputActivite).val(activite);
                        let adresse = selection.l4_normalisee ;
                        $(inputAdresse).val(adresse);
                        let cp = selection.code_postal ;
                        $(inputCp).val(cp);
                        let ville = selection.libelle_commune ;
                        $(inputVille).val(ville);
                        //pas de nom de région pour idf alors je l'insère manuellement
                        let region = (selection.region == "11") ? "île de France" : selection.libelle_region ;
                        $(inputRegion).val(region);

                        //CLEANAGE - POST VALIDATION
                        //vide l'input de recherche
                        $(inputCible).val('')
                        //supprime la liste de résultats
                        $(inputCible).siblings('#listeRecherche').remove()
                        //fait réapparaitre la loupe et disparaitre le compteur
                        $(inputCible).next().children('i').removeClass('d-none');
                        $(inputCible).next().children('div').remove();
                        //redonne sa couleur normal au label
                        $(inputCible).next().removeClass('border-danger text-danger');
                        $(inputCible).next().addClass('border-secondary text-primary');
                    })//end .click((e)=>{

            }// end (rep)=>{
        )//end .done()

        //ERREUR 404
        .fail(
            (fail)=>{
                if(fail.status){
                    $(inputCible).next().children('div').remove();
                    $(inputCible).next().append('<div>0<div>');
                    $(inputCible).next().removeClass('border-secondary text-primary');
                    $(inputCible).next().addClass('border-danger text-danger');
                } 
            }
        )
    }//end else{
}//end apiEntrepise

const Denomination = (inputCible) =>{
    //récup tous les champs ciblés
    let inputDenom = '#prospects_Entreprise_denominationSociale';
    let inputSiret = '#prospects_Entreprise_siret';
    let inputApe = '#prospects_Entreprise_ape';
    let inputActivite = '#prospects_Entreprise_activite';
    let inputAdresse = '#prospects_adresse';
    let inputCp = '#prospects_codePostal';
    let inputVille = '#prospects_ville';
    let inputRegion = '#prospects_region';

    //event champ ville et récup contenu
    $(inputCible).on('keyup', ()=>{
        let recherche = $(inputCible).val();
        apiEntreprise(inputCible, recherche, inputDenom, inputSiret, inputApe, inputActivite, inputAdresse, inputCp, inputVille, inputRegion);
    })
    //utile pour click en dehors
    $(inputCible).on('change', ()=>{
        let recherche = $(inputCible).val();
        apiEntreprise(inputCible, recherche, inputDenom, inputSiret, inputApe, inputActivite, inputAdresse, inputCp, inputVille, inputRegion);
    })

}
Denomination('#searchEntreprise1');


