$(document).on('change', '#devis_formations', function (){
    let $field = $(this)
    let $form = $field.closest('form')
    let data = {}
    data[$field.attr('name')] = $field.val()
    $.post($form.attr("action"), data).then(function (data) {
        
      let $input =  $(data).find('#devis_prix')
     $('#devis_prix').replaceWith($input)
      
    })
});
// block les champs nbrHeure et Jours en fonction du prix selectionné decommenté si necessaire
// pensez a decommenter dans DevisType le 'disabled' des champ NbrJours et dureeH
// pensez a decommenter egalement dans devisTypes le builder addNbrField

// $(document).on('change', '#devis_prix', function (){
 
//  //console.log($("option:selected", this).text().indexOf("horaire"));
//  if ($("option:selected", this).text().indexOf('horaire') !== -1  ) {
//    //console.log("coucou");
//    $("#devis_dureeH").prop("disabled", false);
//  }
//  else if($("option:selected", this).text().indexOf('journalier') !== -1){
//      $("#devis_nbJours").prop("disabled", false);
//      $("#devis_dureeH").prop("disabled", true);
//  }
//  else{
//      $("#devis_dureeH").prop("disabled", true);
//      $("#devis_nbJours").prop("disabled", true);
//  }
// })

$(document).on('change', '.change', function (){
    let $field = $('#devis_prix')
    let data = {}
    let $value = 0
    let frais = 0
    
   

    data[$field.attr('value')] = $field.val();
    $value = parseFloat($field.val())
   $('.annexe').each(function(){
      frais += parseFloat($(this).val())
  })
    
    if(frais == ""){

          if (
            ($("option:selected").text().indexOf("horaire") !== -1 &&
              $(".heures").val() == "") ||
            ($("option:selected").text().indexOf("journalier") !== -1 &&
              $(".jours").val() == "")
          ) {
            $(".tarif").val($value);
          } 
          else if ($("option:selected").text().indexOf("horaire") !== -1 && $(".heures").val() != "")
          {
            $(".tarif").val($value * $(".heures").val());
          } 
          else if ($("option:selected").text().indexOf("journalier") !== -1 && $(".jours").val() != "")
          {
            $(".tarif").val($value *  $(".jours").val()); 
          }
          else 
          { 
            $(".tarif").val($value);
          }

    }else if (frais != "") {
      //console.log(frais, $value);      
          if (
            ($("option:selected").text().indexOf("horaire") !== -1 &&
              $(".heures").val() == "") ||
            ($("option:selected").text().indexOf("journalier") !== -1 &&
              $(".jours").val() == "")
          ) {
            console.log($value, frais);
            $(".tarif").val($value + frais);
          } else if (
            $("option:selected").text().indexOf("horaire") !== -1 &&
            $(".heures").val() != ""
          ) {
            $(".tarif").val($value * $(".heures").val() + frais);
          } else if (
            $("option:selected").text().indexOf("journalier") !== -1 &&
            $(".jours").val() != ""
          ) {
            $(".tarif").val($value * $(".jours").val() + frais);
          } else {
            $(".tarif").val($value + frais);
          }
          
    }
    
})
