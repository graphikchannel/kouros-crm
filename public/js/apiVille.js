//tde 25/08/21 : On va l'utilisé un peu partout : je l'insère dans base.html.twig


const apiGeoVille = (inputCible, saisie)=>{
    //efface la liste pour éviter les cumul au rechargement
        $("#ville").remove();
    //requête ajax à l'api
    $.ajax({
        url : `https://geo.api.gouv.fr/communes?nom=${saisie}`,
    }).done(
        (rep)=>{
            let i = 0;
            if(rep.length <= 30 && rep.length > 0) {
                $(inputCible).after("<select id='ville' size=" + rep.length + " ></select>");

                rep.map(()=>{ 
                    let ville = rep[i].nom;
                    let codeVille = rep[i].code;
                    let departement = rep[i].codeDepartement;
                    let region = rep[i].codeRegion;
                    let cp = rep[i].codesPostaux;
                    
                    let option =  "<option value='" + i + "'>" + ville + " (" + departement + ")</option>";

                    $("#ville").append(option);
                    i++;
                })

                //récup le choix cliqué dans option et le met dans inputCible
                $('#ville').change(()=>{
                    //récup la valeur sélectionnée dans la liste de ville
                    let value = $('#ville option:selected').val();
                    //insère dans l'input ville
                    $('#prospects_ville').val(rep[value].nom)
                    //insère dans l'input cp
                    $('#prospects_codePostal').val(rep[value].codesPostaux )
                    //enregistre code région pour requête
                    let codeRegion = rep[value].codeRegion;

                    $.ajax({
                        url : `https://geo.api.gouv.fr/regions/${codeRegion}`,
                    }).done(
                        (repon)=>{
                            $('#prospects_region').val(repon.nom);
                        }
                    )
                    //efface la liste après insertion
                    $("#ville").remove();
                    
                })
            }
        }
    );
    
}

const ville = (inputCible) =>{
    //event champ ville et récup contenu
    $(inputCible).on('keyup change', ()=>{
        let value = $(inputCible).val();
        (value.length > 2) && apiGeoVille(inputCible, value);
    })
}
ville('#prospects_ville');


