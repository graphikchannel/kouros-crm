//génère les entêtes de commentaire date + identifiant
const commentaire = ()=>{
    //récup l'id des textareas commentaire et la longueur du texte prééxistant au chargement de la page
    let chargement = [];
    const commentAuChargement = ()=>{
        $('textarea').each((index, element)=>{
            if($(element).attr('id').search('_Commentaire')){
                chargement.push({
                    id: $(element).attr('id'),
                    length: $(element).val().length,
                })
            }
        })
        return chargement;
    }
    commentAuChargement();

    //retrouve la zone commentaire concerné dans commentAuChargement() par rapport à la target
    //et récupère la bonne valeur dans chargement
    const commentClicked = (id)=>{
        let test = {};
        chargement.map((textarea)=>{
            if(textarea.id == id){
                test = textarea;
            }
        });
        return test;
    }

    //générélise ces variables
    let id ='';
    let valueWhenClick = '';
    let target = '';

    //écoute click dans une textarea
    $('textarea').on('click',(e)=>{ 
        id = $(e.target).attr('id');
        valueWhenClick = $(e.target).val();
        target = $(e.target);
        

        //vérif si zone commentaire
        //vérif valeur au moment du click == valeur au chargement de la page (donc si rien n'a changé)
        if(id.search('_Commentaire') != -1 && valueWhenClick.length == commentClicked(id).length){
            let contenu = (valueWhenClick.length != 0 ) ? `${valueWhenClick}\r\n` : '';
            let indicatif = $('#contact_commentaire').val();
            let val = `${contenu}${indicatif}`;
            $(target).val(val);
        }

    //supprime l'identifiant à la perte de focus
    $('textarea').on('focusout',(e)=>{
// console.log($(e.target).val().length)
// console.log(commentClicked(id).length + $('#contact_commentaire').val().length )

        //si c'est une zone commentaire
        if(id.search('_Commentaire') != -1){
            //si la valeur au moment de la perte de focus = le commmentaire auto alors supprime tout
            if($(e.target).val().length == $('#contact_commentaire').val().length || $(e.target).val().length == $('#contact_commentaire').val().length - 1){
                $(target).val(valueWhenClick);

            //si il n'y a que l'entête de commentaire en plus 
            //on remet la valeur d'origine
            } else if($(e.target).val().length == commentClicked(id).length + $('#contact_commentaire').val().length +1 ){
                $(e.target).val(valueWhenClick);
            }
        }
        
    })
})
}
