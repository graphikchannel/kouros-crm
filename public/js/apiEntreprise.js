//tde 26/08/21 : recherche entreprise par nom - recup siren /siret 
// source : https://github.com/etalab/sirene_as_api


//gère réinitialisation liste et effacement liste
const reset = (nodes)=>{
    //supprime l'ancienne liste de résultats pour charger la nouvelle
    $(nodes.search).siblings('#listeRecherche').remove()
    //fait réapparaitre la loupe et disparaitre le compteur
    $(nodes.search).next().children('i').removeClass('d-none');
    $(nodes.search).next().children('div').remove();
    //redonne sa couleur normal au label
    $(nodes.search).next().removeClass('border-danger text-danger');
    $(nodes.search).next().addClass('border-secondary text-primary');
}


//changement style label à droite suivant nb résultat (affichage nb résulats ou loupe)
const styleSearch = (nodes,rep)=>{
    const nbRep = rep.total_results;
        if(nbRep >= 50) {
            $(nodes.search).next().removeClass('border-secondary text-primary');
            $(nodes.search).next().addClass('border-danger text-danger');
        } else{
            $(nodes.search).next().removeClass('border-danger text-danger');
            $(nodes.search).next().addClass('border-secondary text-primary');
        }
        //supprime loupe et la remet
        if($(nodes.search).val().length > 0 ){
            $(nodes.search).next().children('i').addClass('d-none');
        }
}

//grille de lecture des tranches nb de salariés
//sera utile pour détermination opco
const trancheEffectif = {
    "NN" : {"min": null, "max":null, "text": "inconnu"},
    "00" : {"min": 0, "max":0, "text": "0"},
    "01" : {"min": 1, "max":2, "text": "1 à 2"},
    "02" : {"min": 3, "max":5, "text": "3 à 5"},   
    "03" : {"min": 6, "max":9, "text": "6 à 9"},   
    "11" : {"min": 10, "max":19, "text": "10 à 19"},   
    "12" : {"min": 20, "max":49, "text": "20 à 49"},   
    "21" : {"min": 50, "max":99, "text": "50 à 99"},   
    "22" : {"min": 100, "max":199, "text": "100 à 199"},   
    "31" : {"min": 200, "max":249, "text": "200 à 249"},   
    "32" : {"min": 250, "max":499, "text": "250 à 499"},   
    "41" : {"min": 500, "max":999, "text": "500 à 999"},   
    "42" : {"min": 1000, "max":1999, "text": "1 000 à 1 999"},   
    "51" : {"min": 2000, "max":4999, "text": "2 000 à 4 999"},   
    "52" : {"min": 5000, "max":9999, "text": "5 000 à 9 999"},   
    "53" : {"min": 10000, "max": 9999999, "text": "supérieur à 10 000"},   
}

//génère le numéro de tva intracommunautaire
const calculNumeroTVA = (siren)=>{
    /* https://www.modele-lettre-gratuit.com/dossiers/entreprise/tout-savoir-sur-la-tva-intracommunautaire.html#:~:text=Le%20num%C3%A9ro%20de%20TVA%20intracommunautaire,'entreprise%20(9%20chiffres).
    Comment calculer le numéro de TVA intracommunautaire à partir du SIREN :
    Calcul de la clé TVA (administration fiscale) = [12 + 3 × (SIREN modulo 97)] modulo 97
    Modulo étant le reste de la division euclidienne par 97.
    Exemple : Clé TVA = [12 + 3 × (123456789 modulo 97)] modulo 97 = [12 + 3 × 39] modulo 97 = 129 modulo 97 = 32
    */ 
    const pays = "FR";
    let cacl1 = siren % 97;
    let cacl2 = 3 * cacl1;
    let cacl3 = 12 + cacl2;
    let cle =  String(cacl3 % 97);

    return pays + cle + String(siren);
}


//récupère les infos de la réponse APi
const recupInfo = (rep,i)=>{
    // je récup les données correspondantes
    let selection = rep.etablissement[i]
    //récup les données
    const infos = {
        denominationSociale : selection.nom_raison_sociale,
        siret : selection.siret,
        ape : selection.activite_principale_entreprise,
        activite : selection.libelle_activite_principale_entreprise,
        salaries : selection.tranche_effectif_salarie_entreprise,
        tva : calculNumeroTVA(selection.siren),
        numero : `${(selection.numero_voie == null) ? '' : selection.numero_voie}`,
        voie : `${(selection.type_voie == null) ? '' :selection.type_voie} ${(selection.libelle_voie == null) ? '' :selection.libelle_voie}`,
        cp : selection.code_postal,
        ville : selection.libelle_commune,
        departement : selection.departement,
        region : (selection.region == "11") ? "île de France" : selection.libelle_region, //pas de nom de région pour idf alors je l'insère manuellement
    }
    return infos;
}

//permet de changer de pages quand beaucoup de résultat
//ou de préciser un departement ou un cp
const PrecisionRecherche = (nodes, rep)=>{
    //récup infos
    const pageActuelle = parseInt(rep.page);
    const nbPage = parseInt(rep.total_pages);

    //chaque crée des index si il a plus d'une page
    if(nbPage != 1){
        
        let calcPrev =  (pageActuelle == 1 ) ? '' : pageActuelle - 1 ;
        let disprev = (pageActuelle == 1) ? "text-secondary" : ''
        let prev =  `<span class="page-item "><a class="page-link ${disprev}" href="${calcPrev}" > << </a></span>` ;

        let activeP1 = (pageActuelle == 1) ? "font-weight-bold" :'';
        let p1 =   `<span class="page-item"><a class="page-link ${activeP1}" href="1" >1</a></span>`;

        let calActuelle = (pageActuelle == 1) ? 2 : pageActuelle;
        let activeAct = (pageActuelle == calActuelle) ? "font-weight-bold" :'';
        let actuelle =   `<span class="page-item"><a class="page-link ${activeAct}" href="${calActuelle}" >${calActuelle}</a></span>`;

        let activeLast = (pageActuelle == nbPage) ? "font-weight-bold" :'';
        let last =  `<span class="page-item"><a class="page-link ${activeLast}" href="${nbPage}" >${nbPage}</a></span>` ;

        let calcNext =  (pageActuelle == nbPage ) ? pageActuelle : pageActuelle + 1 ;
        let disNext = (pageActuelle == nbPage) ? "text-secondary" : ''
        let next =  `<span class="page-item"><a class="page-link ${disNext}" href="${calcNext}" > >> </a></span>` ;
        
        let index = `<span class="pagination ">${prev}${p1}${actuelle}${last}${next}</span>`;

        const inputCp = '<input type="search" class="page-link" placeholder="ex : 75 ou 75007"/>' ;
        const btnCp =  '<button class="page-link" >ok</button>' ;
        const searchCP = `<span id="inputCp" class="pagination "><div class="page-item d-flex" >${inputCp}${btnCp}<div></span>`;

        //insère le tout
        $('#listeRecherche').prepend(`<div id="precision" class="mt-1 d-md-flex justify-content-md-center">${index}${searchCP}</div>`)
    } else{
        $('#precision').remove();
    }
    
}

//lance l'affichage de a page choisie
const pageRecherche = (nodes)=>{
    $('#precision a').on('click', (e)=>{
        e.preventDefault();
        nodes.page = $(e.target).attr('href');
        apiEntreprise(nodes);
    })
}

//lance la recherche du departement ou du code postal
const depCpRecherche = (nodes)=>{
    $('#inputCp button').on('click',(e)=>{
        e.preventDefault();
        let val = $('#inputCp input').val();
        let nb = $('#inputCp input').val().length;
        if(nb >= 2 && nb < 5){
            //prends les 2 premiers caractères pour faire le département
            nodes.dep = val.substring(0,2);
            //réinitialise le cp
            nodes.codePostal = null;
        } else if(nb >= 5){
            //prends les 5 premierscaractères pour faire le code postal
            nodes.codePostal = val.substring(0,5);
            //réinitialise le departement
            nodes.dep = null;
        }
        //réinitialise la page
        nodes.page = 1;
        //lance la recherche
        apiEntreprise(nodes);
    })

}

//génère la liste déroulante d'entreprise
const listeEntreprise = (nodes, rep)=>{
    $('#listeRecherche').remove();
    //insère le nombre de résultat et supprime les doublons
    $(nodes.search).next().children('div').remove();
    $(nodes.search).next().append('<div>' + rep.total_results + '<div>');
    
    //GESTION RESULTATS
    //insère le ul qui recevra la liste
    $(nodes.search).siblings('label').after('<ul class="card list-group" id="listeRecherche" ></ul>')

    //insère les li dans le ul
    let i = 0;
    rep.etablissement.map(()=>{
        choix = rep.etablissement[i];
        //infos entreprise
        let denomination = choix.nom_raison_sociale ;
        //infos localisation
        let ville = choix.libelle_commune ;
        let dept = choix.departement ;
        //LISTE DE RESULTATS - crée chaque occurence de la liste
        let liste = '<li class="p-2" ><a class="list-group-item-action" id="entr-' + i + '" >' + denomination + ' (' + ville + ' - ' + dept + ')</a></li>'
        //insère dans le ul crée plus haut
        $('#listeRecherche').append(liste);

        //change d'occurence entreprise
        i++;
    })//end .map(
}

//fonction requête ajax
const apiEntreprise = (nodes)=>{
    //nom de l'entreprise recherchée
    const nom = $(nodes.search).val();
    //si moins de 3 lettres dans search
    if(nom.length >= 3 ){

        //précise la requête
        const departement = (nodes.dep == null) ? '' :`&departement=${nodes.dep}`;
        const codePostal = (nodes.codePostal == null) ? '' :`&code_postal=${nodes.codePostal}`;
        let requete = `https://entreprise.data.gouv.fr/api/sirene/v1/full_text/${nom}?page=${nodes.page}&per_page=50${departement}${codePostal}`

        //requête api
        $.ajax({
            url : requete,
        }).done(
            (rep)=>{                
                //adapte le style de la loupe au nb de résultats
                styleSearch(nodes,rep);
                //génère la liste déroulante d'entreprise
                listeEntreprise(nodes, rep);
                //pagination des résultats et recherche cp
                PrecisionRecherche(nodes, rep);
                //lance nouvelle recherche suivant la page choisie
                pageRecherche(nodes);
                //lance recherche en fonction departement (2 chiffres saisis) ou cp (5 chiffres saisis)
                depCpRecherche(nodes);

                //détection click choix entreprise
                $('#listeRecherche li a').on('click',(e)=>{
                    //récup l'id qui correspond à la position dans la liste d'entreprise
                    id = $(e.target).attr('id').substr(5);

                    //insère les données dans les inputs dédiés
                    //entreprise
                    $(nodes.denominationSociale).val(recupInfo(rep,id).denominationSociale);
                    $(nodes.siret).val(recupInfo(rep,id).siret);
                    $(nodes.ape).val(recupInfo(rep,id).ape);
                    $(nodes.activite).val(recupInfo(rep,id).activite);
                    $(nodes.tva).val(recupInfo(rep,id).tva);
                    $(nodes.salaries).val(recupInfo(rep,id).salaries);
                    //adresse
                    $(nodes.numero).val(recupInfo(rep,id).numero);
                    $(nodes.voie).val(recupInfo(rep,id).voie);
                    $(nodes.cp).val(recupInfo(rep,id).cp);
                    $(nodes.ville).val(recupInfo(rep,id).ville);
                    $(nodes.region).val(recupInfo(rep,id).region);

                    //CLEANAGE - POST INSERTION
                    reset(nodes);
                    $(nodes.search).val('');

                })//end .click((e)=>{
            }// end (rep)=>{
        )//end .done()

        //ERREUR 404
        .fail(
            (fail)=>{
                if(fail.status){
                    //reboot le style si erreur 404 car 0 résultat
                    $(nodes.search).next().children('div').remove();
                    $(nodes.search).next().append('<div>0<div>');
                    $(nodes.search).next().removeClass('border-secondary text-primary');
                    $(nodes.search).next().addClass('border-danger text-danger');
                } 
            }
        )

    }//fin else = execution requête
}//end apiEntrepise


//lance l'api via écouteurs
const initApiEntreprise = (nodes) =>{
    $(nodes.search).on('keyup', ()=>{
        apiEntreprise(nodes);
        reset(nodes);
        nodes.page = 1;
        nodes.dep = null;
        nodes.codePostal = null;
    })
    //supprime liste et reboot json si pas de valeur et clic en dehors
    $(nodes.search).on('focusout', ()=>{
        if($(nodes.search).val() == ''){
            reset(nodes);
            nodes.page = 1;
            nodes.dep = null;
            nodes.codePostal = null;
        }
    })
}

//json de paramètrage - à faire pour chaque page ou on veut utiliser la fonction
const nodes = {
    search : '#searchEntreprise1',
    page : 1,
    dep : null,
    codePostal : null,
    denominationSociale : '#prospects_Entreprise_denominationSociale',
    siret : '#prospects_Entreprise_siret',
    ape : '#prospects_Entreprise_ape',
    activite : '#prospects_Entreprise_activite',
    salaries : '#prospects_Entreprise_nbsalarie',
    tva : '#prospects_Entreprise_tva',
    numero : '#prospects_adresses_0_numero',
    voie : '#prospects_adresses_0_voie',
    cp : '#prospects_adresses_0_cp',
    ville : '#prospects_adresses_0_ville',
    region : '#prospects_adresses_0_region',
}

//initialise la fonction
initApiEntreprise(nodes);


