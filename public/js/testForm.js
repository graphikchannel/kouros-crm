//tde auto complétion formulair epour tests
const autoName = (nOrP)=>{
        const prenom = [
            "Abby",
            "Emma",
            "Clémence",
            "Thomas",
            "Anthony",
            "Anaïs",
            "Chloé",
            "Antoine",
            "Adam",
            "Lea",
            "Gabriel",
            "Noemie",
            "Raphael",
            "Sarah",
            "Nathan",
            "Aaron",
            "Benjamin",
            "Jean",
            "David",
            "Matthieu",
            "Gabrielle",
            "Salome",
            "Samuel",
            "Simon",
            "Myriam",
            "Noe",
            "Ismael",
            "Johan",
            "Eliott",
            "Arielle",
            "Ben",
            "Anne",
            "Abigael",
            "Ariel",
            "Deborah",
            "Elisabeth",
            "Eve",
            "Daniele",
            "Eliane",
            "Elisee",
            "Emmanuelle",
            "Josue",
            "Emmanuel",
            "Esther",
            "Matthias",
            "Pascal",
            "Rebecca",
            "Jeremie",
            "Joelle",
            "Jessica",
            "Judith",
            "Joel",
            "Joseph",
            "Marie",
            "Michel",
            "Rachel",
            "Raphaelle",
            "Martin",
            "Bernard",
            "Petit",
            "Robert",
            "Richard",
            "Faustin",
            "Elio",
            "Geremy",
            "Jean-Charles",
            "Jean-Joseph",
            "Jean-Michel",
            "Jean-Philippe",
            "Jean-Luc",
            "Jean-Daniel",
            "Jean-Paul",
            "Jean-Marc",
            "Jean-Baptiste",
            "Jean-Pierre",
            "Jean-Marie",
            "Jean-Noel",
            "Jean-Francois",
            "Seraphin",
            "Nathael",
            "Ylan,Daniel"
        ];
        const nom = [
            "Martin",
            "Bernard",
            "Thomas",
            "Petit",
            "Robert",
            "Richard",
            "Durand",
            "Dubois",
            "Moreau",
            "Laurent",
            "Simon",
            "Michel",
            "Lefebvre",
            "Leroy",
            "Roux",
            "David",
            "Bertrand",
            "Morel",
            "Fournier",
            "Girard",
            "Bonnet",
            "Dupont",
            "Lambert",
            "Fontaine",
            "Rousseau",
            "Vincent",
            "Muller",
            "Lefevre",
            "Faure",
            "Andre",
            "Mercier",
            "Blanc",
            "Guerin",
            "Boyer",
            "Garnier",
            "Chevalier",
            "Francois",
            "Legrand",
            "Gauthier",
            "Garcia",
            "Perrin",
            "Robin",
            "Clement",
            "Morin",
            "Nicolas",
            "Henry",
            "Roussel",
            "Mathieu",
            "Gautier",
            "Masson",
            "Marchand",
            "Duval",
            "Denis",
            "Dumont",
            "Marie",
            "Lemaire",
            "Noel",
            "Meyer",
            "Dufour",
            "Meunier",
            "Brun",
            "Blanchard",
            "Giraud",
            "Joly",
            "Riviere",
            "Lucas",
            "Brunet",
            "Gaillard",
            "Barbier",
            "Arnaud",
            "Martinez",
            "Gerard",
            "Roche",
            "Renard",
            "Schmitt",
            "Roy",
            "Leroux",
            "Colin",
            "Vidal",
            "Caron",
            "Picard",
            "Roger",
            "Fabre",
            "Aubert",
            "Lemoine",
            "Renaud",
            "Dumas",
            "Lacroix",
            "Olivier",
            "Philippe",
            "Bourgeois",
            "Pierre",
            "Benoit",
            "Rey",
            "Leclerc",
            "Payet",
            "Rolland",
            "Leclercq",
            "Guillaume",
            "Lecomte",
            "Lopez",
            "Jean",
            "Dupuy",
            "Guillot",
            "Hubert",
            "Berger",
            "Carpentier",
            "Sanchez",
            "Dupuis",
            "Moulin",
            "Louis",
            "Deschamps",
            "Huet",
            "Vasseur",
            "Perez",
            "Boucher",
            "Fleury",
            "Royer",
            "Klein",
            "Jacquet",
            "Adam",
            "Paris",
            "Poirier",
            "Marty",
            "Aubry",
            "Guyot",
            "Carre",
            "Charles",
            "Renault",
            "Charpentier",
            "Menard",
            "Maillard",
            "Baron",
            "Bertin",
            "Bailly",
            "Herve",
            "Schneider",
            "Fernandez",
            "Le Gall",
            "Collet",
            "Leger",
            "Bouvier",
            "Julien",
            "Prevost",
            "Millet",
            "Perrot",
            "Daniel",
            "Le Roux",
            "Cousin",
            "Germain",
            "Breton",
            "Besson",
            "Langlois",
            "Remy",
            "Le Goff",
            "Pelletier",
            "Leveque",
            "Perrier",
            "Leblanc",
            "Barre",
            "Lebrun",
            "Marchal",
            "Weber",
            "Mallet",
            "Hamon",
            "Boulanger",
            "Jacob",
            "Monnier",
            "Michaud",
            "Rodriguez",
            "Guichard",
            "Gillet",
            "Etienne",
            "Grondin",
            "Poulain",
            "Tessier",
            "Chevallier",
            "Collin",
            "Chauvin",
            "Da Silva",
            "Bouchet",
            "Gay",
            "Lemaitre",
            "Benard",
            "Marechal",
            "Humbert",
            "Reynaud",
            "Antoine",
            "Hoarau",
            "Perret",
            "Barthelemy",
            "Cordier",
            "Pichon",
            "Lejeune",
            "Gilbert",
            "Lamy",
            "Delaunay",
            "Pasquier",
            "Carlier",
            "Laporte"
        ];
        //si on demande un prénom = 'p'sinon un nom
        (nOrP == 'p') ? list = prenom : list = nom ;
        //fait un choix random
        randChoix = list[Math.floor(Math.random() * list.length)]
        //retourne le choix
        return randChoix
    }

const city = [
    { name : 'Paris', cp : `750${Math.floor(Math.random()*20)}` , region : 'île de France'},
    { name : 'Reims', cp : '51100' , region : 'Champagne-Ardenne'},
    { name : 'Marseille', cp : `750${Math.floor(Math.random()*16)}` , region : "Provence-Alpes-Côte d'Azur"},
]

//crée un nombre random du bon nombre de chiffre
const numRandom = (nbChiffre)=>{
    let num = 0;
    for (let i = 0; i < nbChiffre; i++) {
        num += String(Math.floor(Math.random()* 9))
    }
    return num;
}

const lorem = ()=>{
    const text = [
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
        "Sed bibendum eros risus, eget finibus ligula aliquet quis.",
        "Praesent at dictum neque. Nam eget nunc leo.",
        "Integer pharetra lacus in tellus congue lobortis.",
        "Vestibulum nisl urna, tincidunt id tellus a, lobortis pretium augue.",
        "Integer purus quam, volutpat id accumsan et, cursus id dui. Praesent eu ultrices velit.",
        "Morbi consectetur lacus in nisi cursus gravida.",
        "Donec consequat ullamcorper magna at placerat.",
        "Nunc nec metus sed neque volutpat interdum.",
        "Quisque vitae justo pretium, sagittis felis ac, ornare dolor.",
        "Aenean at maximus libero. Nullam fringilla neque ut nisi blandit, in iaculis leo efficitur.",
        "Nullam consectetur fringilla nulla, vel dignissim eros. Etiam molestie semper metus a dictum.",
        "Nunc lacus nunc, cursus quis porttitor ac, bibendum nec magna."
    ];
    const nbLigne = Math.floor(Math.random()* 2) + 1;
    let content = '' ;
    for (let index = 0; index < nbLigne; index++) {
        content = content.concat(text[Math.floor(Math.random()* (text.length - 1))], ' ', content) ;
    }
    return content;
}

//vrai random binaire avec 50/50 de chance
const randBinaire = ()=>{
    let number = 2;
    while (number != 0 && number != 1) {
        number = Math.floor(Math.random() *9 % 3)
    }
    return number;
}

const autoComplete = ()=>{
    let prenom = autoName('p');
    let nom = autoName('n');
    //permet de conserver le bon prénom et nom dans l'email quelque soit l'ordre des champs
    const email = (prenom,nom)=>{
        return `${prenom}.${nom}${numRandom(3)}@gmail.com`;
    }
    let mail = email(prenom, nom);
    let choiceCity = Math.floor(Math.random()*2);
    let ville = city[choiceCity].name ;
    let cp = city[choiceCity].cp ;
    let region = city[choiceCity].region;
    //crée le numéro adhérent tva
    let siret = numRandom(14);
    const tva = (siret)=>{
        return `FR${siret.substr(0,9)}${numRandom(2)}`
    }
    const test = `test${numRandom(3)}`;


    // les inputs
    $('input').each((index, element)=>{
        //si champs non rempli
        if($(element).attr('id') /* && $(element).val() == '' */){
            const max = parseInt($(element).attr('maxlength'), 10);
            const disabled = ($(element).attr('disabled')) ? true : false ;
            const dnone = ($(element).is(':visible'));
            const name = $(element).attr('id');

            if(disabled == false && dnone == true ){
                if($(element).attr('type') == "email"){
                    $(element).val(mail);
                    mail = email(prenom, nom);

                }else if($(element).attr('type') == "text"){
                    if(name.search('_prenom') != -1){ 
                        $(element).val(prenom);
                        prenom = autoName('p');
        
                    }else if(name.search('_nom') != -1){
                        $(element).val(nom);
                        nom = autoName('n');
        
                    }else if(name.search('_tel') != -1){
                        $(element).val(`01${numRandom(7)}`);
        
                    }else if(name.search('_mobile') != -1){
                        $(element).val(`06${numRandom(7)}`);

                    }else if(name.search('_denomination') != -1){
                        $(element).val(`Entreprise ${autoName('n')} ${numRandom(4)}`);

                    } else if(name.search('_siret') != -1){
                        $(element).val(siret);

                    } else if(name.search('_ape') != -1){
                        $(element).val(numRandom(4)+"N");
                    
                    } else if(name.search('_tva') != -1){
                        $(element).val(tva(siret));

                    }else if(name.search('_adresse') != -1){
                        if(name.search('_numero') != -1){
                            $(element).val(numRandom(3));

                        } else if(name.search('_voie') != -1){
                            let type =''
                            switch (randBinaire()) {
                                case 0:
                                    type ='rue';
                                    break;
                                case 1:
                                    type ='avenue';
                                    break;
                            }
                            $(element).val(`${type} ${autoName('p')} ${autoName('n')}`);

                        } else if(name.search('_cp') != -1){
                            $(element).val(cp);

                        } else if(name.search('_ville') != -1){
                            $(element).val(ville);

                        } else if(name.search('_region') != -1){
                            $(element).val(region);
                        } 
                    }else{
                        //valeur par défaut
                        $(element).val(test);
                    }
                }
            }
        }
    })
    //textarea
    $('textarea').each((index, element)=>{
        const disabled = ($(element).attr('disabled')) ? true : false ;
        const name = $(element).attr('id');
        const dnone = ($(element).is(':visible'));

        if(name.search('_Commentaire') != -1 && dnone == true ){ 
            let entete = $('#contact_commentaire').val();
            let val = `${entete} ${lorem()}`;
            $(element).val(val);
        }
    })
}

//récup le click droit si on est dans un input
const testForm = ()=>{
    $.event.special.rightclick = {
        bindType: "contextmenu",
        delegateType: "contextmenu",
    };
    
    $(document).on("rightclick", "input", ()=>{
        autoComplete();
        return false;
    });
}
testForm();
