-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le : jeu. 05 août 2021 à 15:51
-- Version du serveur :  10.4.19-MariaDB
-- Version de PHP : 7.4.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `kourosv1`
--

-- --------------------------------------------------------

--
-- Structure de la table `conventions`
--

CREATE TABLE `conventions` (
  `id` int(11) NOT NULL,
  `prospect_id` int(11) NOT NULL,
  `devis_id` int(11) NOT NULL,
  `lieu_formation` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `commentaire` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `conventions`
--

INSERT INTO `conventions` (`id`, `prospect_id`, `devis_id`, `lieu_formation`, `commentaire`) VALUES
(1, 5, 2, 'Clermont ferrand', NULL),
(3, 2, 1, 'Paris', NULL),
(5, 1, 3, 'Toulouse', 'ras'),
(6, 1, 5, 'Toulouse', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `conventions_participants`
--

CREATE TABLE `conventions_participants` (
  `conventions_id` int(11) NOT NULL,
  `participants_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `conventions_participants`
--

INSERT INTO `conventions_participants` (`conventions_id`, `participants_id`) VALUES
(5, 3),
(6, 4);

-- --------------------------------------------------------

--
-- Structure de la table `devis`
--

CREATE TABLE `devis` (
  `id` int(11) NOT NULL,
  `client_id` int(11) DEFAULT NULL,
  `opco_id` int(11) DEFAULT NULL,
  `tva` double DEFAULT NULL,
  `date_creation` datetime NOT NULL,
  `date_debut` datetime NOT NULL,
  `date_fin` datetime NOT NULL,
  `duree_h` int(11) NOT NULL,
  `numero_devis` int(11) NOT NULL,
  `methode` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `frais_annexes` double DEFAULT NULL,
  `statut` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nbr_participants` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `devis`
--

INSERT INTO `devis` (`id`, `client_id`, `opco_id`, `tva`, `date_creation`, `date_debut`, `date_fin`, `duree_h`, `numero_devis`, `methode`, `frais_annexes`, `statut`, `nbr_participants`) VALUES
(1, 1, NULL, 0, '2021-07-02 00:00:00', '2021-07-05 00:00:00', '2021-09-30 00:00:00', 21, 3484, '1', 540, '0', 0),
(2, 4, NULL, 0, '2021-07-06 00:00:00', '2021-10-10 00:00:00', '2021-12-12 00:00:00', 32, 3489, '2', 210, '1', 0),
(3, 5, NULL, 0, '2021-07-02 00:00:00', '2021-07-28 00:00:00', '2021-07-30 00:00:00', 5, 112, '1', NULL, '2', 0),
(4, 10, 1, 0, '2021-07-09 00:00:00', '2021-07-09 00:00:00', '2021-07-31 00:00:00', 42, 213, '1', 100, '0', 0),
(5, 9, 1, 3, '2021-07-15 00:00:00', '2021-07-16 00:00:00', '2021-07-22 00:00:00', 32, 113, '1', 520, '0', 3);

-- --------------------------------------------------------

--
-- Structure de la table `devis_formations`
--

CREATE TABLE `devis_formations` (
  `devis_id` int(11) NOT NULL,
  `formations_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `devis_formations`
--

INSERT INTO `devis_formations` (`devis_id`, `formations_id`) VALUES
(1, 1),
(2, 1),
(3, 2),
(4, 1),
(5, 2);

-- --------------------------------------------------------

--
-- Structure de la table `devis_prospects`
--

CREATE TABLE `devis_prospects` (
  `devis_id` int(11) NOT NULL,
  `prospects_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `devis_prospects`
--

INSERT INTO `devis_prospects` (`devis_id`, `prospects_id`) VALUES
(5, 7);

-- --------------------------------------------------------

--
-- Structure de la table `doctrine_migration_versions`
--

CREATE TABLE `doctrine_migration_versions` (
  `version` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `executed_at` datetime DEFAULT NULL,
  `execution_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `doctrine_migration_versions`
--

INSERT INTO `doctrine_migration_versions` (`version`, `executed_at`, `execution_time`) VALUES
('DoctrineMigrations\\Version20210727084749', '2021-07-27 10:47:55', 305),
('DoctrineMigrations\\Version20210727085206', '2021-07-27 10:52:10', 137),
('DoctrineMigrations\\Version20210804094233', '2021-08-04 11:42:41', 1425),
('DoctrineMigrations\\Version20210804094912', '2021-08-04 11:49:18', 192);

-- --------------------------------------------------------

--
-- Structure de la table `entreprise`
--

CREATE TABLE `entreprise` (
  `id` int(11) NOT NULL,
  `denomination_sociale` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `siren` int(11) DEFAULT NULL,
  `nom` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `prenom` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nom_facultatif` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `prenom_facultatif` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mail` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mail_facultatif` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tel` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tel_facultatif` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `activite` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `commentaire` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `entreprise`
--

INSERT INTO `entreprise` (`id`, `denomination_sociale`, `siren`, `nom`, `prenom`, `nom_facultatif`, `prenom_facultatif`, `mail`, `mail_facultatif`, `tel`, `tel_facultatif`, `activite`, `commentaire`) VALUES
(1, 'Domo', 123456789, 'Momo', 'Maurice', NULL, NULL, 'momo@domo.fr', NULL, '0100000000', NULL, 'domotique', NULL),
(2, 'Stop', 712345612, 'Toto', 'Test', 'Simone', 'Sisi', 'toto@toto.fr', 'sisi@sisi.fr', '0121003300', '0200000000', 'Sécurité', 'Appeler MMe Simone en priorité'),
(3, 'Test2', 1234566, 'Aure', 'Aurelie', NULL, NULL, 'aure@aure.fr', NULL, '0321769823', NULL, 'Recherche emploi', NULL),
(4, 'EntrepriseProspectType', 4, 'Toto', 'Test', 'Toto', 'Test', 'aure@aure.fr', NULL, '0321769823', NULL, 'Recherche emploi', NULL),
(5, 'Le Gaulois', 2147483647, 'Ast', 'Eric', 'Obelix', 'Idefix', 'ast@eric.fr', 'obelix@idefix.com', '0700000000', NULL, 'Alimentaire', 'RAS'),
(6, 'Sotf', 213123123, 'lara', 'croft', NULL, NULL, 'lara@croft.com', NULL, '0622222222', NULL, 'Numérique', NULL),
(7, 'Soleil', 1231231231, 'tar', 'tampion', NULL, NULL, 'tar@tampion.fr', NULL, '0900000000', NULL, 'Luminaire', NULL),
(8, 'Vaporette', 234561234, 'Star', 'Joe', NULL, NULL, 'star@joe.com', NULL, '0900000000', NULL, 'Substitut', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `formateurs`
--

CREATE TABLE `formateurs` (
  `id` int(11) NOT NULL,
  `denomination_sociale` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `siret` int(11) DEFAULT NULL,
  `nom` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `prenom` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mail` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tel` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `adresse` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `code_postal` varchar(5) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ville` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tva` double DEFAULT NULL,
  `methode` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `formateurs`
--

INSERT INTO `formateurs` (`id`, `denomination_sociale`, `siret`, `nom`, `prenom`, `mail`, `tel`, `adresse`, `code_postal`, `ville`, `tva`, `methode`) VALUES
(1, NULL, NULL, 'Testeur', 'Test', 'tes@testeur.fr', '0600000000', '27 rue du soleil', '12345', 'Soleil', NULL, '1'),
(2, NULL, NULL, 'Jean', 'Jeannot', 'jan@jen.fr', '0600000000', 'rue de la cerise', '12345', 'Salade', NULL, '3');

-- --------------------------------------------------------

--
-- Structure de la table `formateurs_formations`
--

CREATE TABLE `formateurs_formations` (
  `formateurs_id` int(11) NOT NULL,
  `formations_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `formateurs_formations`
--

INSERT INTO `formateurs_formations` (`formateurs_id`, `formations_id`) VALUES
(1, 1),
(2, 1),
(2, 2);

-- --------------------------------------------------------

--
-- Structure de la table `formations`
--

CREATE TABLE `formations` (
  `id` int(11) NOT NULL,
  `titre` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `prix_jour` double NOT NULL,
  `programme` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `niveau` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rubrique` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `formations`
--

INSERT INTO `formations` (`id`, `titre`, `prix_jour`, `programme`, `niveau`, `rubrique`) VALUES
(1, 'Android', 590, 'Android', '1', '12'),
(2, '3DS Max', 590, '3DS Max', '3', '16'),
(3, 'Adobe Muse', 470, '<p>Adobe Muse</p>', '1', '17');

-- --------------------------------------------------------

--
-- Structure de la table `opco`
--

CREATE TABLE `opco` (
  `id` int(11) NOT NULL,
  `nom` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nom_contact` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `prenom_contact` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mail` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tel` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adresse` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ville` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code_postal` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tva` double DEFAULT NULL,
  `site` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ape` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `opco`
--

INSERT INTO `opco` (`id`, `nom`, `nom_contact`, `prenom_contact`, `mail`, `tel`, `adresse`, `ville`, `code_postal`, `tva`, `site`, `ape`) VALUES
(1, 'opco1', 'contactopco', 'contact', 'contact@opco.com', '0780954672', '12 rue opco', 'arbre', '12345', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `participants`
--

CREATE TABLE `participants` (
  `id` int(11) NOT NULL,
  `nom` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `prenom` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `participants`
--

INSERT INTO `participants` (`id`, `nom`, `prenom`) VALUES
(3, 'toto', 'tarte'),
(4, 'Testeur', 'Test');

-- --------------------------------------------------------

--
-- Structure de la table `prix`
--

CREATE TABLE `prix` (
  `id` int(11) NOT NULL,
  `prix_ht` double NOT NULL,
  `prix_ttc` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `prix_devis`
--

CREATE TABLE `prix_devis` (
  `prix_id` int(11) NOT NULL,
  `devis_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `prix_formations`
--

CREATE TABLE `prix_formations` (
  `prix_id` int(11) NOT NULL,
  `formations_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `prospects`
--

CREATE TABLE `prospects` (
  `id` int(11) NOT NULL,
  `entreprise_id` int(11) DEFAULT NULL,
  `statut` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nom` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `prenom` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mail` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tel` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adresse` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `code_postal` varchar(5) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ville` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `site` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `commentaire` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `prospects`
--

INSERT INTO `prospects` (`id`, `entreprise_id`, `statut`, `nom`, `prenom`, `mail`, `tel`, `adresse`, `code_postal`, `ville`, `site`, `commentaire`, `role`) VALUES
(1, 1, '1', 'Taupe', 'René', 'rené@lataupe.fr', '0799225511', 'rue de la cerise', '12345', 'Salade', NULL, NULL, 1),
(2, NULL, '1', 'Souris', 'Lili', 'lili@souris.fr', '0321769823', NULL, NULL, 'Lille', NULL, NULL, 1),
(3, 2, '1', 'Jean', 'Jeannot', 'jean@jeannot.fr', '0321769823', 'rue de la cerise', '12345', 'Salades', NULL, NULL, 1),
(4, 2, '1', 'Aure', 'Aurelie', 'aure@aure.fr', '0600000000', '23 cité de la pensée', '23456', 'Saint rosé', 'www.rosedusud.com', NULL, 1),
(5, 1, '1', 'Chipper', 'Dora', 'dora@exploration.com', '0321000000', '1 rue de l\'étoile', '01234', 'Ciel', NULL, NULL, 1),
(6, 2, '1', 'Marco', 'Marc', 'marc@marc.fr', '0344223344', 'rue de la cerise', '12345', 'Salade', NULL, NULL, 0),
(7, NULL, '1', 'Lucie', 'Lucette', 'lucie@lucette.fr', '0900000000', '23 cité de la pensée', '23456', 'Saint rosé', NULL, NULL, 1),
(8, 2, '1', 'Aure', 'Aurelie', 'aure@aure.fr', '0321769823', '23 cité de la pensée', '23456', 'Saint rosé', 'www.aure.com', NULL, 0),
(9, NULL, '1', 'Lulu', 'Lucie', 'lulu@cie.com', '0799225511', 'rue de la cerise', '12345', 'Salade', 'www.lulu.com', NULL, 0),
(10, 3, '1', 'Test2', 'Test2', 'Test2@test.fr', '0433333333', '23 cité de la pensée', '23456', 'Saint rosé', NULL, NULL, 0),
(11, 4, '1', 'Test', 'NouveauForm', 'form@entrepriseProspect.fr', '0321769823', 'rue de la cerise', '12345', 'Salade', NULL, 'test ajout formulaire entreprise Prospect', 0),
(12, 8, '1', 'Star', 'Joe', 'star@joe.com', '0800000000', '1rue de la tour', '12345', 'Monaco', NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Structure de la table `template`
--

CREATE TABLE `template` (
  `id` int(11) NOT NULL,
  `texte` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tag` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `filename` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `template_formations`
--

CREATE TABLE `template_formations` (
  `template_id` int(11) NOT NULL,
  `formations_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `email` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `roles` longtext COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:json)',
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fullname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`id`, `email`, `roles`, `password`, `image`, `fullname`) VALUES
(1, 'aurebes@gmail.com', '[]', NULL, 'https://lh3.googleusercontent.com/a/AATXAJx4OTb1SZaKY1pL28JfVc73iASLR9VrKWiv81LY=s96-c', 'aurélie besnard');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `conventions`
--
ALTER TABLE `conventions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_AC36C55541DEFADA` (`devis_id`),
  ADD KEY `IDX_AC36C555D182060A` (`prospect_id`);

--
-- Index pour la table `conventions_participants`
--
ALTER TABLE `conventions_participants`
  ADD PRIMARY KEY (`conventions_id`,`participants_id`),
  ADD KEY `IDX_842B525975019F55` (`conventions_id`),
  ADD KEY `IDX_842B5259838709D5` (`participants_id`);

--
-- Index pour la table `devis`
--
ALTER TABLE `devis`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_8B27C52B19EB6921` (`client_id`),
  ADD KEY `IDX_8B27C52BCF671859` (`opco_id`);

--
-- Index pour la table `devis_formations`
--
ALTER TABLE `devis_formations`
  ADD PRIMARY KEY (`devis_id`,`formations_id`),
  ADD KEY `IDX_F0316ADF41DEFADA` (`devis_id`),
  ADD KEY `IDX_F0316ADF3BF5B0C2` (`formations_id`);

--
-- Index pour la table `devis_prospects`
--
ALTER TABLE `devis_prospects`
  ADD PRIMARY KEY (`devis_id`,`prospects_id`),
  ADD KEY `IDX_44A0C67C41DEFADA` (`devis_id`),
  ADD KEY `IDX_44A0C67C775D63D` (`prospects_id`);

--
-- Index pour la table `doctrine_migration_versions`
--
ALTER TABLE `doctrine_migration_versions`
  ADD PRIMARY KEY (`version`);

--
-- Index pour la table `entreprise`
--
ALTER TABLE `entreprise`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `formateurs`
--
ALTER TABLE `formateurs`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `formateurs_formations`
--
ALTER TABLE `formateurs_formations`
  ADD PRIMARY KEY (`formateurs_id`,`formations_id`),
  ADD KEY `IDX_BED037DFFB0881C8` (`formateurs_id`),
  ADD KEY `IDX_BED037DF3BF5B0C2` (`formations_id`);

--
-- Index pour la table `formations`
--
ALTER TABLE `formations`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `opco`
--
ALTER TABLE `opco`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `participants`
--
ALTER TABLE `participants`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `prix`
--
ALTER TABLE `prix`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `prix_devis`
--
ALTER TABLE `prix_devis`
  ADD PRIMARY KEY (`prix_id`,`devis_id`),
  ADD KEY `IDX_6B68AB75944722F2` (`prix_id`),
  ADD KEY `IDX_6B68AB7541DEFADA` (`devis_id`);

--
-- Index pour la table `prix_formations`
--
ALTER TABLE `prix_formations`
  ADD PRIMARY KEY (`prix_id`,`formations_id`),
  ADD KEY `IDX_39F2E9BC944722F2` (`prix_id`),
  ADD KEY `IDX_39F2E9BC3BF5B0C2` (`formations_id`);

--
-- Index pour la table `prospects`
--
ALTER TABLE `prospects`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_35730C06A4AEAFEA` (`entreprise_id`);

--
-- Index pour la table `template`
--
ALTER TABLE `template`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `template_formations`
--
ALTER TABLE `template_formations`
  ADD PRIMARY KEY (`template_id`,`formations_id`),
  ADD KEY `IDX_625B64F85DA0FB8` (`template_id`),
  ADD KEY `IDX_625B64F83BF5B0C2` (`formations_id`);

--
-- Index pour la table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_8D93D649E7927C74` (`email`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `conventions`
--
ALTER TABLE `conventions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT pour la table `devis`
--
ALTER TABLE `devis`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT pour la table `entreprise`
--
ALTER TABLE `entreprise`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT pour la table `formateurs`
--
ALTER TABLE `formateurs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `formations`
--
ALTER TABLE `formations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table `opco`
--
ALTER TABLE `opco`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `participants`
--
ALTER TABLE `participants`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `prix`
--
ALTER TABLE `prix`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `prospects`
--
ALTER TABLE `prospects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT pour la table `template`
--
ALTER TABLE `template`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `conventions`
--
ALTER TABLE `conventions`
  ADD CONSTRAINT `FK_AC36C55541DEFADA` FOREIGN KEY (`devis_id`) REFERENCES `devis` (`id`),
  ADD CONSTRAINT `FK_AC36C555D182060A` FOREIGN KEY (`prospect_id`) REFERENCES `prospects` (`id`);

--
-- Contraintes pour la table `conventions_participants`
--
ALTER TABLE `conventions_participants`
  ADD CONSTRAINT `FK_842B525975019F55` FOREIGN KEY (`conventions_id`) REFERENCES `conventions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_842B5259838709D5` FOREIGN KEY (`participants_id`) REFERENCES `participants` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `devis`
--
ALTER TABLE `devis`
  ADD CONSTRAINT `FK_8B27C52B19EB6921` FOREIGN KEY (`client_id`) REFERENCES `prospects` (`id`),
  ADD CONSTRAINT `FK_8B27C52BCF671859` FOREIGN KEY (`opco_id`) REFERENCES `opco` (`id`);

--
-- Contraintes pour la table `devis_formations`
--
ALTER TABLE `devis_formations`
  ADD CONSTRAINT `FK_F0316ADF3BF5B0C2` FOREIGN KEY (`formations_id`) REFERENCES `formations` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_F0316ADF41DEFADA` FOREIGN KEY (`devis_id`) REFERENCES `devis` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `devis_prospects`
--
ALTER TABLE `devis_prospects`
  ADD CONSTRAINT `FK_44A0C67C41DEFADA` FOREIGN KEY (`devis_id`) REFERENCES `devis` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_44A0C67C775D63D` FOREIGN KEY (`prospects_id`) REFERENCES `prospects` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `formateurs_formations`
--
ALTER TABLE `formateurs_formations`
  ADD CONSTRAINT `FK_BED037DF3BF5B0C2` FOREIGN KEY (`formations_id`) REFERENCES `formations` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_BED037DFFB0881C8` FOREIGN KEY (`formateurs_id`) REFERENCES `formateurs` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `prix_devis`
--
ALTER TABLE `prix_devis`
  ADD CONSTRAINT `FK_6B68AB7541DEFADA` FOREIGN KEY (`devis_id`) REFERENCES `devis` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_6B68AB75944722F2` FOREIGN KEY (`prix_id`) REFERENCES `prix` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `prix_formations`
--
ALTER TABLE `prix_formations`
  ADD CONSTRAINT `FK_39F2E9BC3BF5B0C2` FOREIGN KEY (`formations_id`) REFERENCES `formations` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_39F2E9BC944722F2` FOREIGN KEY (`prix_id`) REFERENCES `prix` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `prospects`
--
ALTER TABLE `prospects`
  ADD CONSTRAINT `FK_35730C06A4AEAFEA` FOREIGN KEY (`entreprise_id`) REFERENCES `entreprise` (`id`);

--
-- Contraintes pour la table `template_formations`
--
ALTER TABLE `template_formations`
  ADD CONSTRAINT `FK_625B64F83BF5B0C2` FOREIGN KEY (`formations_id`) REFERENCES `formations` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_625B64F85DA0FB8` FOREIGN KEY (`template_id`) REFERENCES `template` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
